-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jul 29, 2020 at 05:43 PM
-- Server version: 10.3.22-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `maamariam_dadon`
--

-- --------------------------------------------------------

--
-- Table structure for table `costs`
--

CREATE TABLE `costs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `date` date NOT NULL,
  `basket` int(11) NOT NULL DEFAULT 0,
  `commission` int(11) NOT NULL DEFAULT 0,
  `details` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `costs`
--

INSERT INTO `costs` (`id`, `date`, `basket`, `commission`, `details`, `created_at`, `updated_at`) VALUES
(1, '2020-07-20', 4825, 3650, 'robel', '2020-07-24 04:23:57', '2020-07-24 04:23:57'),
(2, '2020-07-25', 6100, 8750, 'tukri+lebar,commison bake sarkar50%selim10%', '2020-07-25 01:33:24', '2020-07-25 01:33:24'),
(5, '2020-07-26', 37407, 20600, 'extera cash 18857', '2020-07-29 00:13:02', '2020-07-29 00:13:02'),
(6, '2020-07-27', 6550, 6800, 'Robel', '2020-07-29 00:14:32', '2020-07-29 00:14:32'),
(7, '2020-07-28', 10075, 8250, 'Robel', '2020-07-29 00:16:48', '2020-07-29 00:16:48'),
(8, '2020-07-29', 9940, 7700, 'Robel', '2020-07-29 00:18:08', '2020-07-29 00:18:08');

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `father` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mother` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permanent_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `loan` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `name`, `date_of_birth`, `company`, `father`, `mother`, `email`, `phone`, `address`, `permanent_address`, `loan`, `created_at`, `updated_at`) VALUES
(4, 'জালাল আহম্মদ/রুবেল', '1989-02-27', 'এফ.বি:মা মরিয়ম ১', 'আব্দুল গনি', 'আমেনা বেগম', 'emrulkaies14@gmail.com', '01814125885', 'চট্রগ্রাম', 'চট্রগ্রাম', 70000, '2020-07-23 07:06:45', '2020-07-27 07:13:01'),
(5, 'নুর মোহাম্মদ/ইউসুপ আলী মাঝী', '1984-01-07', 'এফ.বি:মা মরিয়ম ৩', 'আব্দুল গনি', 'আমেনা বেগম', 'admin@gmail.com', '01819801580', 'চট্রগ্রাম', 'চট্রগ্রাম', 1075000, '2020-07-23 07:21:20', '2020-07-24 03:36:06'),
(6, 'ছালেহ আহম্মদ/বাবুল', '1998-06-23', 'এফ.বি:ফাতেমা', 'আব্দুল গনি', 'আমেনা বেগম', 'Admin1@gmail.com', '01818449496', 'চট্রগ্রাম', 'চট্রগ্রাম', 0, '2020-07-23 07:28:44', '2020-07-23 07:28:44'),
(7, 'ছৈয়দ নুর/মুক্তার মাঝী', '1987-05-23', 'এফ.বি:ফাতেমা ৫', 'আব্দুল গনি', 'আমেনা বেগম', 'Admin2@gmail.com', '01890865563', 'চট্রগ্রাম', 'চট্রগ্রাম', 0, '2020-07-23 07:35:18', '2020-07-23 07:35:18'),
(8, 'নাছির মাঝী', '1974-07-23', 'এফ.বি আল্লাহর দান', 'ইয়াকুব আলী', 'হোছনেরা বেগম', 'Admin4@gmail.com', '01823166642', 'কক্সবাজার', 'কক্সবাজার', 1300000, '2020-07-23 07:43:45', '2020-07-24 03:18:44'),
(9, 'আব্দু শুক্কুর/রবিউল হোছেন', '1992-02-10', 'এফ.বি:হক হাজেরা ১', 'করত আলী', 'শামশুনাহার', 'admin5@gmail.com', '01820106414', 'ধলঘাট', 'ধলঘাট', 1500000, '2020-07-23 07:51:32', '2020-07-24 03:18:06'),
(10, 'মোহাম্মদ মোর্শেদ কোং', '1975-04-08', 'এফ.বি:মা-বাবার দোয়া', 'বদর উদ্দীন', 'রহিমা খাতুন', 'admin6@gmail.com', '01813996914', 'কক্সবাজার', 'কক্সবাজার', 1000000, '2020-07-23 08:01:10', '2020-07-24 03:19:18'),
(11, 'কতুব উদ্দীন', '1977-08-07', 'এফ.বি:আবুল কায়েস', 'আবুল আহম্মেদ', 'আমেনা খাতুন', 'admin7@gmail.com', '01716141991', 'কক্সবাজার', 'কক্সবাজার', 800000, '2020-07-23 08:09:11', '2020-07-24 03:24:40'),
(12, 'মাস্টার আবু শামা', '1967-07-23', 'এফ.বি:কে আসমা', 'N/a', 'N/a', 'Admin8@gmail.com', '01829919928', 'চকরিয়া', 'চকরিয়া', 1500000, '2020-07-23 08:19:25', '2020-07-24 03:21:59'),
(13, 'ওসমান গনি(AD)', '1984-07-23', 'এফ.বি:আল্লাহর দান', 'জাবের সওঃ', 'N/A', 'admin9@gmail.com', '01624340451', 'চকরিয়া', 'চকরিয়া', 1500000, '2020-07-23 08:27:50', '2020-07-24 03:22:35'),
(14, 'ওসমান গনি', '1984-07-23', 'এফ.বি:এজাহার মামইনুা', 'জাবের সওঃ', 'N/A', 'Admin10@gmail.com', '01866831379', 'চকরিয়া', 'চকরিয়া', 1500000, '2020-07-23 08:35:05', '2020-07-24 03:24:14'),
(15, 'ফজল করিম', '1988-07-23', 'এফ.বি:শাহ্ আমনত', 'নাই', 'নাই', 'Admin11@gmail.com', '01824587490', 'মহেশখালী', 'মহেশখলী', 1200000, '2020-07-23 09:15:29', '2020-07-24 03:27:04'),
(16, 'নুরুল আবছার', '1984-12-23', 'এফ.বি:সিরাজ মরিয়ম', 'নাই', 'নাই', 'Admin12@gmail.com', '01825183742', 'চকরিয়া', 'চকরিয়া', 1500000, '2020-07-23 09:19:30', '2020-07-24 03:27:54'),
(17, 'সাজু মাঝী', '1981-07-23', 'এফ.বি:নুর ভেলুযারা', 'নুরুল আমিন', 'নাই', 'Admin13@gmail.com', '01878210731', 'ধলঘাট', 'ধলঘাট', 1500000, '2020-07-23 09:24:41', '2020-07-24 03:28:47'),
(18, 'মোহাম্মদ ইদ্রিছ', '1987-03-22', 'এফ.বি:মা আয়েশা', 'শাহ আলম', 'নুরু নাহার', 'Admin14@gmail.com', '01812828696', 'বদরখালী', 'বদরখালী', 1500000, '2020-07-23 09:32:40', '2020-07-24 03:31:07'),
(19, 'হাবিব/আব্দুল আজিজ', '1984-07-23', 'এফ.বি:সোনার মদিনা ১', 'নাই', 'নাই', 'Admin15@gmail.com', '01818921153', 'বাঁশখালী', 'বাঁশখালী', 1400000, '2020-07-23 09:41:53', '2020-07-24 03:32:03'),
(20, 'হাবিব/আব্দুল আজিজ(K.A)', '1975-07-23', 'এফ.বি:খাজা আজমির', 'নাই', 'নাই', 'admin16@gmail.com', '01817709986', 'বাঁশখালী', 'বাঁশখালী', 1400000, '2020-07-23 09:45:44', '2020-07-24 03:33:12'),
(21, 'ফরেষ্টর বাবুল', '1979-07-23', 'এফ.বি:মায়ের দোয়া', 'নাই', 'নাই', 'admin17@gmail.com', '01818357670', 'কক্সবাজা', 'কক্সবাজা', 1000000, '2020-07-23 09:49:34', '2020-07-24 03:34:14'),
(22, 'শাহাদত মাঝী', '1988-07-23', 'এফ.বি:মাইজ ভান্ডার', 'শাহ আলম', 'নুরুন্নাহার', 'Admin18@gmail.com', '01820987650', 'বদরখালী', 'বদরখালী', 1400000, '2020-07-23 10:10:51', '2020-07-25 00:58:13'),
(23, 'মাহাবুল আলম/আব্দুল হক', '1977-07-23', 'এফ.বি:আল্লাহর দান', 'নাই', 'নাই', 'Admin19@gmail.com', '01755750758', 'মহেশখালী', 'মহেশখালী', 1500000, '2020-07-23 10:15:24', '2020-07-24 03:35:38'),
(24, 'ফারুক মেম্বার', '1973-07-23', 'এফ.বি:নবী মোহম্মদ', 'নাই', 'নাই', 'Admin20@gmail.com', '01620257906', 'কতুবদিয়া', 'কতুবদিয়া', 400000, '2020-07-23 10:19:45', '2020-07-24 03:17:36'),
(25, 'রওশন আলী', '1986-07-23', 'এফ.বি:মা মেহেরুনে্নছা', 'নাই', 'নাই', 'Admin21@gmail.com', '01826529788', 'চট্রগ্রাম', 'চট্রগ্রাম', 1000000, '2020-07-23 10:33:56', '2020-07-24 03:29:32'),
(26, 'জালাল আহম্মদ', '1968-04-13', 'এফ.বি:ইরফান', 'ছবির আহম্মদ', 'লোকমান খাতুন', 'Admin22@gmail.com', '01819065890', 'ধলঘাট', 'ধলঘাট', 1000000, '2020-07-23 10:40:40', '2020-07-25 01:09:19'),
(27, 'হারুণ/ফারুক মেম্বার(ভাই)', '1986-01-01', 'এফ.বি:', 'মোহাম্মদ কালু', 'মমতাজ বেগম', 'Admin23@gmail.com', '01835035573', 'কতুবদিয়া', 'কতুবদিয়া', 350000, '2020-07-23 10:51:05', '2020-07-24 03:38:27'),
(28, 'আহম্মদ করিম', '1984-07-23', 'এফ.বি:', 'নাই', 'নাই', 'Admin24@gmail.com', '01813997877', 'চকরিয়া', 'চকরিয়া', 800000, '2020-07-23 10:53:39', '2020-07-24 03:40:15'),
(29, 'আবুল কালাম(মনু)', '1991-07-23', 'লাশের ধার', 'নাই', 'নাই', 'admin26@gmail.com', '01875996714', 'বাঁশখালী', 'বাঁশখালী', 370000, '2020-07-23 11:01:05', '2020-07-24 03:30:18'),
(30, 'আমির হোছেন', '1979-07-23', 'লাশের ধার', 'নাই', 'নাই', 'Admin27@gmail.com', '01871470351', 'বাঁশখালী', 'বাঁশখালী', 200000, '2020-07-23 11:05:50', '2020-07-24 03:37:44'),
(31, 'আব্দু জলীল', '1983-07-23', 'লাশের ধার', 'নাই', 'নাই', 'Admin28@gmail.com', '01747663708', 'বাঁশখালী', 'বাঁশখালী', 200000, '2020-07-23 11:09:41', '2020-07-24 03:38:59'),
(32, 'আব্দুল আজীজ', '1986-07-23', 'লাশের ধার', 'নাই', 'নাই', 'Admin29@gmail.com', '01633911082', 'বাঁশখালী', 'বাঁশখালী', 250000, '2020-07-23 11:11:57', '2020-07-24 03:39:32'),
(33, 'হাবীব(দুলা ভাই)', '1984-07-23', 'লাশের ধার', 'নাই', 'নাই', 'Admin30@gmail.com', '01789765404', 'বাঁশখালী', 'বাঁশখালী', 200000, '2020-07-23 11:17:25', '2020-07-24 03:41:43'),
(34, 'জমির সওদাগর', '1978-07-23', 'মৎস্য ব্যাবসায়ী', 'নাই', 'নাই', 'Admin31@gmail.com', '01829045257', 'বাঁশখালী', 'বাঁশখালী', 300000, '2020-07-23 11:20:21', '2020-07-24 04:18:49'),
(35, 'মুজিবুল হক(মেম্বার)', '1986-07-24', 'এফ.বি:অলী আহম্মদ', 'অলী আহম্মদ', 'নাই', 'Admin32@gmail.com', '01834733998', 'মহেশ খালী', 'মহেশখালী', 0, '2020-07-24 02:48:46', '2020-07-24 02:48:46'),
(36, 'অন্যান্য মাছ বিক্রি', '1995-07-24', 'এফ.বি:', 'নাই', 'নাই', 'admin33@gmail.com', '01811105104', 'কতুবদিয়া', 'কতুবদীয়া', 0, '2020-07-24 02:52:44', '2020-07-25 01:56:27'),
(37, 'শামশু দোহা(বাবুল বহদ্দার)', '1980-02-27', 'এফ.বি হাজী শাহ আলম', 'হাজী শাহ আলম', 'নাই', 'admin34@gmail.com', '01814469113', 'চট্রগ্রাম', 'চট্রগ্রাম', 0, '2020-07-27 04:14:11', '2020-07-29 00:02:20');

-- --------------------------------------------------------

--
-- Table structure for table `deposits`
--

CREATE TABLE `deposits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `deposit_uid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `deposit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `deposits`
--

INSERT INTO `deposits` (`id`, `deposit_uid`, `customer_id`, `deposit`, `date`, `note`, `created_at`, `updated_at`) VALUES
(1, '5f1ab216454b8', 36, '322781', '2020-07-20', 'robel', '2020-07-24 04:04:06', '2020-07-24 04:04:06'),
(2, '5f1ab26dc1b9e', 26, '200000', '2020-07-20', 'robel', '2020-07-24 04:05:33', '2020-07-24 04:05:33'),
(3, '5f1ab2b427513', 26, '166312', '2020-07-21', 'robel', '2020-07-24 04:06:44', '2020-07-24 04:07:40'),
(5, '5f1ab5ebb27d7', 34, '82174', '2020-07-22', 'robel', '2020-07-24 04:20:27', '2020-07-24 04:20:27'),
(6, '5f1ab9c47ba41', 36, '8000', '2020-07-24', 'COMISSION 2%BEAK.do', '2020-07-24 04:36:52', '2020-07-24 04:36:52'),
(7, '5f1be815e6aa3', 26, '511074', '2020-07-25', 'robel', '2020-07-25 02:06:45', '2020-07-25 02:06:45'),
(8, '5f1be83fc9f6b', 22, '652832', '2020-07-25', 'robel', '2020-07-25 02:07:27', '2020-07-25 02:07:27'),
(10, '5f1be89fe731a', 36, '36536', '2020-07-25', 'robel', '2020-07-25 02:09:03', '2020-07-25 02:09:03'),
(12, '5f1edba7f38f7', 4, '202167', '2020-07-25', 'robel', '2020-07-27 07:50:31', '2020-07-27 07:50:31');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `installments`
--

CREATE TABLE `installments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `installment_uid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `taka` int(11) NOT NULL,
  `date` date NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `installments`
--

INSERT INTO `installments` (`id`, `installment_uid`, `customer_id`, `taka`, `date`, `note`, `added_by`, `created_at`, `updated_at`) VALUES
(1, '5f1aae7ed6cb1', 36, 0, '2020-07-20', 'COMISSION 2%BEAK.do', 1, '2020-07-24 03:48:46', '2020-07-24 03:48:46'),
(2, '5f1aafa75f555', 26, 0, '2020-07-20', 'robel', 1, '2020-07-24 03:53:43', '2020-07-24 03:53:43'),
(3, '5f1ab0f2943e4', 26, 0, '2020-07-21', 'ice/salt/', 1, '2020-07-24 03:59:14', '2020-07-24 03:59:14'),
(5, '5f1ab1ad84260', 34, 0, '2020-07-22', 'robel', 1, '2020-07-24 04:02:21', '2020-07-24 04:02:21'),
(6, '5f1bd805424e7', 22, 100000, '2020-07-25', 'robel', 1, '2020-07-25 00:58:13', '2020-07-25 00:58:13'),
(7, '5f1bd96390b89', 4, 0, '2020-07-25', 'robel', 1, '2020-07-25 01:04:03', '2020-07-25 01:04:03'),
(8, '5f1bda9f77b84', 26, 0, '2020-07-25', 'robel', 1, '2020-07-25 01:09:19', '2020-07-25 01:09:19'),
(9, '5f1be5abbc46e', 36, 0, '2020-07-25', 'nur mohama.dk', 1, '2020-07-25 01:56:27', '2020-07-25 01:56:27');

-- --------------------------------------------------------

--
-- Table structure for table `loans`
--

CREATE TABLE `loans` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `loan_uid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `taka` int(11) NOT NULL,
  `date` date NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `loans`
--

INSERT INTO `loans` (`id`, `loan_uid`, `customer_id`, `taka`, `date`, `note`, `added_by`, `created_at`, `updated_at`) VALUES
(1, '5f1aa6f05fd8e', 34, 300000, '2020-07-24', 'robel', 1, '2020-07-24 03:16:32', '2020-07-24 03:16:32'),
(2, '5f1aa7305d07e', 24, 400000, '2020-07-24', 'robel', 1, '2020-07-24 03:17:36', '2020-07-24 03:17:36'),
(3, '5f1aa74ef0b44', 9, 1500000, '2020-07-24', 'robel', 1, '2020-07-24 03:18:06', '2020-07-24 03:18:06'),
(4, '5f1aa77469df0', 8, 1300000, '2020-07-24', 'robel', 1, '2020-07-24 03:18:44', '2020-07-24 03:18:44'),
(5, '5f1aa7962d954', 10, 1000000, '2020-07-24', 'robel', 1, '2020-07-24 03:19:18', '2020-07-24 03:19:18'),
(6, '5f1aa83725e37', 12, 1500000, '2020-07-24', 'robel', 1, '2020-07-24 03:21:59', '2020-07-24 03:21:59'),
(7, '5f1aa85bb1f6b', 13, 1500000, '2020-07-24', 'robel', 1, '2020-07-24 03:22:35', '2020-07-24 03:22:35'),
(8, '5f1aa8be134ce', 14, 1500000, '2020-07-24', 'robel', 1, '2020-07-24 03:24:14', '2020-07-24 03:24:14'),
(9, '5f1aa8d8d02db', 11, 800000, '2020-07-24', 'robel', 1, '2020-07-24 03:24:40', '2020-07-24 03:24:40'),
(10, '5f1aa96858fbc', 15, 1200000, '2020-07-24', 'robel', 1, '2020-07-24 03:27:04', '2020-07-24 03:27:04'),
(11, '5f1aa99a3477e', 16, 1500000, '2020-07-24', 'robel', 1, '2020-07-24 03:27:54', '2020-07-24 03:27:54'),
(12, '5f1aa9cfdfb8f', 17, 1500000, '2020-07-24', 'robel', 1, '2020-07-24 03:28:47', '2020-07-24 03:28:47'),
(13, '5f1aa9fc25d47', 25, 1000000, '2020-07-24', 'robel', 1, '2020-07-24 03:29:32', '2020-07-24 03:29:32'),
(14, '5f1aaa2a69c04', 29, 370000, '2020-07-24', 'robel', 1, '2020-07-24 03:30:18', '2020-07-24 03:30:18'),
(15, '5f1aaa5b87f1f', 18, 1500000, '2020-07-24', 'robel', 1, '2020-07-24 03:31:07', '2020-07-24 03:31:07'),
(16, '5f1aaa931d046', 19, 1400000, '2020-07-24', 'robel', 1, '2020-07-24 03:32:03', '2020-07-24 03:32:03'),
(17, '5f1aaad821910', 20, 1400000, '2020-07-24', 'robel', 1, '2020-07-24 03:33:12', '2020-07-24 03:33:12'),
(18, '5f1aab163eee6', 21, 1000000, '2020-07-24', 'robel', 1, '2020-07-24 03:34:14', '2020-07-24 03:34:14'),
(19, '5f1aab4edf2aa', 22, 1500000, '2020-07-24', 'robel', 1, '2020-07-24 03:35:10', '2020-07-24 03:35:10'),
(20, '5f1aab69f4010', 23, 1500000, '2020-07-24', 'robel', 1, '2020-07-24 03:35:37', '2020-07-24 03:35:37'),
(21, '5f1aab8680b39', 5, 1000000, '2020-07-24', 'robel', 1, '2020-07-24 03:36:06', '2020-07-24 03:36:06'),
(22, '5f1aaba2f298d', 26, 1000000, '2020-07-24', 'robel', 1, '2020-07-24 03:36:34', '2020-07-24 03:36:34'),
(23, '5f1aabe8a67a0', 30, 200000, '2020-07-24', 'robel', 1, '2020-07-24 03:37:44', '2020-07-24 03:37:44'),
(24, '5f1aac1348559', 27, 350000, '2020-07-24', 'robel', 1, '2020-07-24 03:38:27', '2020-07-24 03:38:27'),
(25, '5f1aac335ae6c', 31, 200000, '2020-07-24', 'robel', 1, '2020-07-24 03:38:59', '2020-07-24 03:38:59'),
(26, '5f1aac54c7237', 32, 250000, '2020-07-24', 'robel', 1, '2020-07-24 03:39:32', '2020-07-24 03:39:32'),
(27, '5f1aac7fe7f4c', 28, 800000, '2020-07-24', 'robel', 1, '2020-07-24 03:40:15', '2020-07-24 03:40:15'),
(28, '5f1aacd7589d8', 33, 200000, '2020-07-24', 'robel', 1, '2020-07-24 03:41:43', '2020-07-24 03:41:43'),
(29, '5f1d780148689', 4, 0, '2020-07-26', 'robel', 1, '2020-07-26 06:33:05', '2020-07-26 06:33:05');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `collection_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` bigint(20) UNSIGNED NOT NULL,
  `manipulations` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `custom_properties` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `responsive_images` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `model_type`, `model_id`, `collection_name`, `name`, `file_name`, `mime_type`, `disk`, `size`, `manipulations`, `custom_properties`, `responsive_images`, `order_column`, `created_at`, `updated_at`) VALUES
(9, 'App\\Models\\Customer', 4, 'profile', '74E98EF0-D25E-4E99-B65F-D6FAF0B6CA4E', '74E98EF0-D25E-4E99-B65F-D6FAF0B6CA4E.jpeg', 'image/jpeg', 'public', 791142, '[]', '[]', '[]', 1, '2020-07-23 07:06:45', '2020-07-23 07:06:45'),
(10, 'App\\Models\\Customer', 4, 'nid_card', '8CD04044-72A0-43F6-95C9-BD37EF6EA20D', '8CD04044-72A0-43F6-95C9-BD37EF6EA20D.jpeg', 'image/jpeg', 'public', 877193, '[]', '[]', '[]', 2, '2020-07-23 07:06:45', '2020-07-23 07:06:45'),
(11, 'App\\Models\\Customer', 4, 'check', '1E5E2C44-B9F4-4BFF-B847-B88483CEC33E', '1E5E2C44-B9F4-4BFF-B847-B88483CEC33E.jpeg', 'image/jpeg', 'public', 791142, '[]', '[]', '[]', 3, '2020-07-23 07:06:45', '2020-07-23 07:06:45'),
(12, 'App\\Models\\Customer', 5, 'profile', '48F5D13E-DE28-4D39-8F5B-E8B733C7D96E', '48F5D13E-DE28-4D39-8F5B-E8B733C7D96E.jpeg', 'image/jpeg', 'public', 141189, '[]', '[]', '[]', 4, '2020-07-23 07:21:20', '2020-07-23 07:21:20'),
(13, 'App\\Models\\Customer', 5, 'nid_card', 'image', 'image.jpg', 'image/jpeg', 'public', 1956885, '[]', '[]', '[]', 5, '2020-07-23 07:21:20', '2020-07-23 07:21:20'),
(14, 'App\\Models\\Customer', 5, 'check', '5ACAE93E-08AA-4A07-8642-E393EF4144E6', '5ACAE93E-08AA-4A07-8642-E393EF4144E6.png', 'image/png', 'public', 410451, '[]', '[]', '[]', 6, '2020-07-23 07:21:21', '2020-07-23 07:21:21'),
(15, 'App\\Models\\Customer', 6, 'profile', '7ED7F796-9AF8-48E8-AAF0-BA7883DC28D2', '7ED7F796-9AF8-48E8-AAF0-BA7883DC28D2.jpeg', 'image/jpeg', 'public', 274974, '[]', '[]', '[]', 7, '2020-07-23 07:28:44', '2020-07-23 07:28:44'),
(16, 'App\\Models\\Customer', 6, 'nid_card', 'BCB55439-5016-498D-90C4-7E1D2E99B6C5', 'BCB55439-5016-498D-90C4-7E1D2E99B6C5.jpeg', 'image/jpeg', 'public', 274974, '[]', '[]', '[]', 8, '2020-07-23 07:28:44', '2020-07-23 07:28:44'),
(17, 'App\\Models\\Customer', 6, 'check', '37D6CC36-CE75-45B0-A845-14D6A0FB51CF', '37D6CC36-CE75-45B0-A845-14D6A0FB51CF.jpeg', 'image/jpeg', 'public', 274974, '[]', '[]', '[]', 9, '2020-07-23 07:28:44', '2020-07-23 07:28:44'),
(18, 'App\\Models\\Customer', 7, 'profile', '3B2314D5-53BF-4553-84CF-47A75E0D8D03', '3B2314D5-53BF-4553-84CF-47A75E0D8D03.jpeg', 'image/jpeg', 'public', 721235, '[]', '[]', '[]', 10, '2020-07-23 07:35:18', '2020-07-23 07:35:18'),
(19, 'App\\Models\\Customer', 7, 'nid_card', 'A76C9050-C6B6-427A-AB22-9CEBC4EEDD77', 'A76C9050-C6B6-427A-AB22-9CEBC4EEDD77.jpeg', 'image/jpeg', 'public', 59905, '[]', '[]', '[]', 11, '2020-07-23 07:35:19', '2020-07-23 07:35:19'),
(20, 'App\\Models\\Customer', 7, 'check', '8622432C-02A5-4E44-BB5D-DA81D9102B79', '8622432C-02A5-4E44-BB5D-DA81D9102B79.jpeg', 'image/jpeg', 'public', 65401, '[]', '[]', '[]', 12, '2020-07-23 07:35:19', '2020-07-23 07:35:19'),
(22, 'App\\Models\\Customer', 8, 'nid_card', '5BD2A196-3333-4395-9223-C2425437313A', '5BD2A196-3333-4395-9223-C2425437313A.jpeg', 'image/jpeg', 'public', 408132, '[]', '[]', '[]', 14, '2020-07-23 07:43:45', '2020-07-23 07:43:45'),
(23, 'App\\Models\\Customer', 8, 'check', '9C743F85-A3F5-43A9-8096-0A7D867A0739', '9C743F85-A3F5-43A9-8096-0A7D867A0739.jpeg', 'image/jpeg', 'public', 240152, '[]', '[]', '[]', 15, '2020-07-23 07:43:45', '2020-07-23 07:43:45'),
(24, 'App\\Models\\Customer', 9, 'profile', '8C36EC38-48B9-4CA0-904A-A1240A80F643', '8C36EC38-48B9-4CA0-904A-A1240A80F643.jpeg', 'image/jpeg', 'public', 124036, '[]', '[]', '[]', 16, '2020-07-23 07:51:32', '2020-07-23 07:51:32'),
(25, 'App\\Models\\Customer', 9, 'nid_card', 'D8C80ECD-06BE-478A-B7DD-D889122016FF', 'D8C80ECD-06BE-478A-B7DD-D889122016FF.jpeg', 'image/jpeg', 'public', 196777, '[]', '[]', '[]', 17, '2020-07-23 07:51:32', '2020-07-23 07:51:32'),
(26, 'App\\Models\\Customer', 9, 'check', '98B92197-4BDE-4A05-ADC1-26E163955473', '98B92197-4BDE-4A05-ADC1-26E163955473.jpeg', 'image/jpeg', 'public', 301424, '[]', '[]', '[]', 18, '2020-07-23 07:51:32', '2020-07-23 07:51:32'),
(27, 'App\\Models\\Customer', 10, 'profile', 'E07F58E7-3319-408A-A9BD-8707B79AF821', 'E07F58E7-3319-408A-A9BD-8707B79AF821.jpeg', 'image/jpeg', 'public', 39527, '[]', '[]', '[]', 19, '2020-07-23 08:01:10', '2020-07-23 08:01:10'),
(28, 'App\\Models\\Customer', 10, 'nid_card', '1ABF9990-BD3B-4A98-A350-68F75D12C1E1', '1ABF9990-BD3B-4A98-A350-68F75D12C1E1.jpeg', 'image/jpeg', 'public', 744080, '[]', '[]', '[]', 20, '2020-07-23 08:01:10', '2020-07-23 08:01:10'),
(29, 'App\\Models\\Customer', 10, 'check', 'BD6E0AE8-DAB8-406E-8D22-3D166E3E9722', 'BD6E0AE8-DAB8-406E-8D22-3D166E3E9722.jpeg', 'image/jpeg', 'public', 361600, '[]', '[]', '[]', 21, '2020-07-23 08:01:10', '2020-07-23 08:01:10'),
(30, 'App\\Models\\Customer', 11, 'profile', 'ABC3041F-9CAB-487F-96A4-9D7B6962EC4A', 'ABC3041F-9CAB-487F-96A4-9D7B6962EC4A.jpeg', 'image/jpeg', 'public', 37646, '[]', '[]', '[]', 22, '2020-07-23 08:09:11', '2020-07-23 08:09:11'),
(31, 'App\\Models\\Customer', 11, 'nid_card', 'E3D402B8-1BC6-4B3C-8E17-798DEEABFB79', 'E3D402B8-1BC6-4B3C-8E17-798DEEABFB79.jpeg', 'image/jpeg', 'public', 326986, '[]', '[]', '[]', 23, '2020-07-23 08:09:11', '2020-07-23 08:09:11'),
(32, 'App\\Models\\Customer', 11, 'check', 'ACF41778-0009-48E7-8EBF-C45A2547B25C', 'ACF41778-0009-48E7-8EBF-C45A2547B25C.jpeg', 'image/jpeg', 'public', 266104, '[]', '[]', '[]', 24, '2020-07-23 08:09:11', '2020-07-23 08:09:11'),
(33, 'App\\Models\\Customer', 12, 'profile', '1AF87C6C-80F9-4A84-B710-7E69FEA04279', '1AF87C6C-80F9-4A84-B710-7E69FEA04279.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 25, '2020-07-23 08:19:25', '2020-07-23 08:19:25'),
(34, 'App\\Models\\Customer', 12, 'nid_card', '47F992D3-FB84-458E-84D2-8CA572C92D53', '47F992D3-FB84-458E-84D2-8CA572C92D53.jpeg', 'image/jpeg', 'public', 471320, '[]', '[]', '[]', 26, '2020-07-23 08:19:25', '2020-07-23 08:19:25'),
(35, 'App\\Models\\Customer', 12, 'check', '008AA629-96DB-4373-9C19-C89E9A2C41F3', '008AA629-96DB-4373-9C19-C89E9A2C41F3.jpeg', 'image/jpeg', 'public', 471320, '[]', '[]', '[]', 27, '2020-07-23 08:19:25', '2020-07-23 08:19:25'),
(36, 'App\\Models\\Customer', 13, 'profile', '341388D6-645D-43CA-9F29-35A4629E40E2', '341388D6-645D-43CA-9F29-35A4629E40E2.jpeg', 'image/jpeg', 'public', 541300, '[]', '[]', '[]', 28, '2020-07-23 08:27:50', '2020-07-23 08:27:50'),
(37, 'App\\Models\\Customer', 13, 'nid_card', '7D2B1658-B21F-44EB-B2D8-242ECA0F6816', '7D2B1658-B21F-44EB-B2D8-242ECA0F6816.jpeg', 'image/jpeg', 'public', 393599, '[]', '[]', '[]', 29, '2020-07-23 08:27:50', '2020-07-23 08:27:50'),
(38, 'App\\Models\\Customer', 13, 'check', '78BB4C6B-D19E-4DD0-A1EB-7920F2D95F34', '78BB4C6B-D19E-4DD0-A1EB-7920F2D95F34.jpeg', 'image/jpeg', 'public', 384445, '[]', '[]', '[]', 30, '2020-07-23 08:27:50', '2020-07-23 08:27:50'),
(39, 'App\\Models\\Customer', 14, 'profile', '72968AAE-1176-40BA-8176-787518AFCA62', '72968AAE-1176-40BA-8176-787518AFCA62.jpeg', 'image/jpeg', 'public', 541300, '[]', '[]', '[]', 31, '2020-07-23 08:35:05', '2020-07-23 08:35:05'),
(40, 'App\\Models\\Customer', 14, 'nid_card', '224C407F-D5EB-4A6E-AFA0-F87F1E16E524', '224C407F-D5EB-4A6E-AFA0-F87F1E16E524.jpeg', 'image/jpeg', 'public', 384445, '[]', '[]', '[]', 32, '2020-07-23 08:35:05', '2020-07-23 08:35:05'),
(41, 'App\\Models\\Customer', 14, 'check', 'CA26B8FB-657F-42D6-892C-1A58699F4FA7', 'CA26B8FB-657F-42D6-892C-1A58699F4FA7.jpeg', 'image/jpeg', 'public', 384445, '[]', '[]', '[]', 33, '2020-07-23 08:35:05', '2020-07-23 08:35:05'),
(42, 'App\\Models\\Customer', 15, 'profile', '7956A5FC-BED8-45C9-B9FC-DFC29623A356', '7956A5FC-BED8-45C9-B9FC-DFC29623A356.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 34, '2020-07-23 09:15:29', '2020-07-23 09:15:29'),
(43, 'App\\Models\\Customer', 15, 'nid_card', 'B830EB20-242E-4250-88F8-5ADDF30E66B6', 'B830EB20-242E-4250-88F8-5ADDF30E66B6.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 35, '2020-07-23 09:15:29', '2020-07-23 09:15:29'),
(44, 'App\\Models\\Customer', 15, 'check', '8069751E-238C-4F7E-A186-012339E7A8D8', '8069751E-238C-4F7E-A186-012339E7A8D8.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 36, '2020-07-23 09:15:29', '2020-07-23 09:15:29'),
(45, 'App\\Models\\Customer', 16, 'profile', 'CE05B314-8CE2-4942-B5ED-8A6FC32AFD3C', 'CE05B314-8CE2-4942-B5ED-8A6FC32AFD3C.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 37, '2020-07-23 09:19:30', '2020-07-23 09:19:30'),
(46, 'App\\Models\\Customer', 16, 'nid_card', '6676944D-E0C9-440A-9CB5-BCCC0E84ADE7', '6676944D-E0C9-440A-9CB5-BCCC0E84ADE7.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 38, '2020-07-23 09:19:30', '2020-07-23 09:19:30'),
(47, 'App\\Models\\Customer', 16, 'check', '1E1F531B-9C55-4BE3-BF9D-E22055ED2F9B', '1E1F531B-9C55-4BE3-BF9D-E22055ED2F9B.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 39, '2020-07-23 09:19:30', '2020-07-23 09:19:30'),
(48, 'App\\Models\\Customer', 17, 'profile', '9F61B553-3DD3-4DE7-AF6F-44C3345863BF', '9F61B553-3DD3-4DE7-AF6F-44C3345863BF.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 40, '2020-07-23 09:24:41', '2020-07-23 09:24:41'),
(49, 'App\\Models\\Customer', 17, 'nid_card', '451F72A4-DC34-4134-B480-B7D5F86D23B2', '451F72A4-DC34-4134-B480-B7D5F86D23B2.jpeg', 'image/jpeg', 'public', 283114, '[]', '[]', '[]', 41, '2020-07-23 09:24:41', '2020-07-23 09:24:41'),
(50, 'App\\Models\\Customer', 17, 'check', '3EF1407B-185E-490A-8526-240F0DB58EE9', '3EF1407B-185E-490A-8526-240F0DB58EE9.jpeg', 'image/jpeg', 'public', 283114, '[]', '[]', '[]', 42, '2020-07-23 09:24:41', '2020-07-23 09:24:41'),
(51, 'App\\Models\\Customer', 18, 'profile', '87AD51DC-649E-42A9-B926-DA4530EB36E9', '87AD51DC-649E-42A9-B926-DA4530EB36E9.jpeg', 'image/jpeg', 'public', 48049, '[]', '[]', '[]', 43, '2020-07-23 09:32:40', '2020-07-23 09:32:40'),
(52, 'App\\Models\\Customer', 18, 'nid_card', 'F4DC4DEA-2269-4F69-B862-2F2E91476416', 'F4DC4DEA-2269-4F69-B862-2F2E91476416.jpeg', 'image/jpeg', 'public', 302367, '[]', '[]', '[]', 44, '2020-07-23 09:32:40', '2020-07-23 09:32:40'),
(53, 'App\\Models\\Customer', 18, 'check', '099586D7-B86A-48E8-9E8B-85CA2EDBF263', '099586D7-B86A-48E8-9E8B-85CA2EDBF263.jpeg', 'image/jpeg', 'public', 423182, '[]', '[]', '[]', 45, '2020-07-23 09:32:40', '2020-07-23 09:32:40'),
(54, 'App\\Models\\Customer', 19, 'profile', 'EAB5ED09-326A-4308-A268-5FE36C8A2B36', 'EAB5ED09-326A-4308-A268-5FE36C8A2B36.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 46, '2020-07-23 09:41:53', '2020-07-23 09:41:53'),
(55, 'App\\Models\\Customer', 19, 'nid_card', '7C8E79AB-21CF-46DE-AB21-A33D0CF4BF68', '7C8E79AB-21CF-46DE-AB21-A33D0CF4BF68.jpeg', 'image/jpeg', 'public', 408247, '[]', '[]', '[]', 47, '2020-07-23 09:41:53', '2020-07-23 09:41:53'),
(56, 'App\\Models\\Customer', 19, 'check', '2C50B899-026F-4F23-9DD4-4C05D20D1A20', '2C50B899-026F-4F23-9DD4-4C05D20D1A20.jpeg', 'image/jpeg', 'public', 408247, '[]', '[]', '[]', 48, '2020-07-23 09:41:53', '2020-07-23 09:41:53'),
(57, 'App\\Models\\Customer', 20, 'profile', '07ECC009-A349-4198-A79E-E99DD27055B8', '07ECC009-A349-4198-A79E-E99DD27055B8.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 49, '2020-07-23 09:45:44', '2020-07-23 09:45:44'),
(58, 'App\\Models\\Customer', 20, 'nid_card', 'C5A1C472-95E6-4CE8-BDBA-FC80913D8FDA', 'C5A1C472-95E6-4CE8-BDBA-FC80913D8FDA.jpeg', 'image/jpeg', 'public', 408247, '[]', '[]', '[]', 50, '2020-07-23 09:45:44', '2020-07-23 09:45:44'),
(59, 'App\\Models\\Customer', 20, 'check', '4E281C50-26A2-49DB-8095-670A5CA01A09', '4E281C50-26A2-49DB-8095-670A5CA01A09.jpeg', 'image/jpeg', 'public', 408247, '[]', '[]', '[]', 51, '2020-07-23 09:45:44', '2020-07-23 09:45:44'),
(60, 'App\\Models\\Customer', 21, 'profile', '9B88D6ED-74E1-431A-A31C-80163A8A5818', '9B88D6ED-74E1-431A-A31C-80163A8A5818.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 52, '2020-07-23 09:49:34', '2020-07-23 09:49:34'),
(61, 'App\\Models\\Customer', 21, 'nid_card', 'D7F78754-87DF-4DC6-9796-D6CA17C0EB12', 'D7F78754-87DF-4DC6-9796-D6CA17C0EB12.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 53, '2020-07-23 09:49:34', '2020-07-23 09:49:34'),
(62, 'App\\Models\\Customer', 21, 'check', '62DB67A1-C9FC-4E1A-B5AD-A6194FDFF6FC', '62DB67A1-C9FC-4E1A-B5AD-A6194FDFF6FC.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 54, '2020-07-23 09:49:34', '2020-07-23 09:49:34'),
(63, 'App\\Models\\Customer', 22, 'profile', '4D751EBA-0A18-4D2D-992C-25F24471CFD0', '4D751EBA-0A18-4D2D-992C-25F24471CFD0.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 55, '2020-07-23 10:10:52', '2020-07-23 10:10:52'),
(64, 'App\\Models\\Customer', 22, 'nid_card', '004F1F16-2B18-44B0-81EC-94D7B1085EC3', '004F1F16-2B18-44B0-81EC-94D7B1085EC3.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 56, '2020-07-23 10:10:52', '2020-07-23 10:10:52'),
(65, 'App\\Models\\Customer', 22, 'check', 'CA244E0C-444D-426D-8F1D-6F26D46AE7FF', 'CA244E0C-444D-426D-8F1D-6F26D46AE7FF.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 57, '2020-07-23 10:10:52', '2020-07-23 10:10:52'),
(66, 'App\\Models\\Customer', 23, 'profile', '2C7B4596-57A5-426E-ACAB-1FB586E1DE93', '2C7B4596-57A5-426E-ACAB-1FB586E1DE93.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 58, '2020-07-23 10:15:24', '2020-07-23 10:15:24'),
(67, 'App\\Models\\Customer', 23, 'nid_card', 'F0C61B3D-D253-4586-A0EB-B9D80B113FEB', 'F0C61B3D-D253-4586-A0EB-B9D80B113FEB.jpeg', 'image/jpeg', 'public', 522888, '[]', '[]', '[]', 59, '2020-07-23 10:15:24', '2020-07-23 10:15:24'),
(68, 'App\\Models\\Customer', 23, 'check', '37EE1CFC-14CE-4EEA-9B68-2D76F69232CB', '37EE1CFC-14CE-4EEA-9B68-2D76F69232CB.jpeg', 'image/jpeg', 'public', 522888, '[]', '[]', '[]', 60, '2020-07-23 10:15:24', '2020-07-23 10:15:24'),
(69, 'App\\Models\\Customer', 24, 'profile', '6157FD95-B5D8-4C29-9155-E4E06279E1DC', '6157FD95-B5D8-4C29-9155-E4E06279E1DC.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 61, '2020-07-23 10:19:45', '2020-07-23 10:19:45'),
(70, 'App\\Models\\Customer', 24, 'nid_card', '478C4C9F-7EEE-4C72-A7DE-617A4405A3A0', '478C4C9F-7EEE-4C72-A7DE-617A4405A3A0.jpeg', 'image/jpeg', 'public', 474393, '[]', '[]', '[]', 62, '2020-07-23 10:19:45', '2020-07-23 10:19:45'),
(71, 'App\\Models\\Customer', 24, 'check', '233FCF7B-CEAA-4759-ABE2-9F6DE5A15672', '233FCF7B-CEAA-4759-ABE2-9F6DE5A15672.jpeg', 'image/jpeg', 'public', 474393, '[]', '[]', '[]', 63, '2020-07-23 10:19:45', '2020-07-23 10:19:45'),
(73, 'App\\Models\\Customer', 25, 'nid_card', 'B8353020-DF73-46E3-8E90-4E21692DE41B', 'B8353020-DF73-46E3-8E90-4E21692DE41B.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 65, '2020-07-23 10:33:56', '2020-07-23 10:33:56'),
(74, 'App\\Models\\Customer', 25, 'check', 'D905263D-2FB7-4284-8D0A-D15C83E67D86', 'D905263D-2FB7-4284-8D0A-D15C83E67D86.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 66, '2020-07-23 10:33:56', '2020-07-23 10:33:56'),
(75, 'App\\Models\\Customer', 26, 'profile', '83835B9B-F56A-4C30-AA33-6F52BCC7782E', '83835B9B-F56A-4C30-AA33-6F52BCC7782E.jpeg', 'image/jpeg', 'public', 56742, '[]', '[]', '[]', 67, '2020-07-23 10:40:40', '2020-07-23 10:40:40'),
(76, 'App\\Models\\Customer', 26, 'nid_card', 'FF257FDB-98ED-4E5D-B2DB-5E666B532F91', 'FF257FDB-98ED-4E5D-B2DB-5E666B532F91.jpeg', 'image/jpeg', 'public', 592557, '[]', '[]', '[]', 68, '2020-07-23 10:40:40', '2020-07-23 10:40:40'),
(77, 'App\\Models\\Customer', 26, 'check', '92C23DF3-5132-4B87-BD43-712FAA434CEF', '92C23DF3-5132-4B87-BD43-712FAA434CEF.jpeg', 'image/jpeg', 'public', 378076, '[]', '[]', '[]', 69, '2020-07-23 10:40:40', '2020-07-23 10:40:40'),
(78, 'App\\Models\\Customer', 27, 'profile', '658A6B5B-ED67-46FC-8213-FE4A1DDF801B', '658A6B5B-ED67-46FC-8213-FE4A1DDF801B.jpeg', 'image/jpeg', 'public', 411563, '[]', '[]', '[]', 70, '2020-07-23 10:51:05', '2020-07-23 10:51:05'),
(79, 'App\\Models\\Customer', 27, 'nid_card', 'E23420DC-BABD-42CD-87B4-7A6944DB8471', 'E23420DC-BABD-42CD-87B4-7A6944DB8471.jpeg', 'image/jpeg', 'public', 523620, '[]', '[]', '[]', 71, '2020-07-23 10:51:05', '2020-07-23 10:51:05'),
(80, 'App\\Models\\Customer', 27, 'check', '85A9D5D5-E0A1-4EF0-AFC9-D576E7DD2924', '85A9D5D5-E0A1-4EF0-AFC9-D576E7DD2924.jpeg', 'image/jpeg', 'public', 257755, '[]', '[]', '[]', 72, '2020-07-23 10:51:05', '2020-07-23 10:51:05'),
(81, 'App\\Models\\Customer', 28, 'profile', '0877DE25-1288-43A9-A5C5-33FA554F8737', '0877DE25-1288-43A9-A5C5-33FA554F8737.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 73, '2020-07-23 10:53:39', '2020-07-23 10:53:39'),
(82, 'App\\Models\\Customer', 28, 'nid_card', 'B10DE2AA-CE29-4626-9F89-BE8124D73C5E', 'B10DE2AA-CE29-4626-9F89-BE8124D73C5E.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 74, '2020-07-23 10:53:39', '2020-07-23 10:53:39'),
(83, 'App\\Models\\Customer', 28, 'check', '78133252-E234-4F4F-884C-19C1F78C24F2', '78133252-E234-4F4F-884C-19C1F78C24F2.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 75, '2020-07-23 10:53:39', '2020-07-23 10:53:39'),
(84, 'App\\Models\\Customer', 29, 'profile', '8CD3B824-821B-427E-B8D8-82A8A1CCB544', '8CD3B824-821B-427E-B8D8-82A8A1CCB544.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 76, '2020-07-23 11:01:05', '2020-07-23 11:01:05'),
(85, 'App\\Models\\Customer', 29, 'nid_card', 'B4DBE294-2F6E-4CF1-9C4C-01EDE975C64F', 'B4DBE294-2F6E-4CF1-9C4C-01EDE975C64F.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 77, '2020-07-23 11:01:05', '2020-07-23 11:01:05'),
(86, 'App\\Models\\Customer', 29, 'check', '1B06C3D0-8F0A-40BE-BF6E-1CEC8B1C7FE1', '1B06C3D0-8F0A-40BE-BF6E-1CEC8B1C7FE1.jpeg', 'image/jpeg', 'public', 266540, '[]', '[]', '[]', 78, '2020-07-23 11:01:05', '2020-07-23 11:01:05'),
(87, 'App\\Models\\Customer', 30, 'profile', 'EE98F8E9-D6AA-4463-902B-6D960BE01D15', 'EE98F8E9-D6AA-4463-902B-6D960BE01D15.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 79, '2020-07-23 11:05:50', '2020-07-23 11:05:50'),
(88, 'App\\Models\\Customer', 30, 'nid_card', '9037AA4E-32F6-4A73-A674-3420787925E2', '9037AA4E-32F6-4A73-A674-3420787925E2.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 80, '2020-07-23 11:05:50', '2020-07-23 11:05:50'),
(89, 'App\\Models\\Customer', 30, 'check', 'BB82FE54-77FD-4C20-B646-76378BDC1415', 'BB82FE54-77FD-4C20-B646-76378BDC1415.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 81, '2020-07-23 11:05:50', '2020-07-23 11:05:50'),
(90, 'App\\Models\\Customer', 31, 'profile', '34F69078-F2A2-473D-BDF1-1B02F6C4D71B', '34F69078-F2A2-473D-BDF1-1B02F6C4D71B.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 82, '2020-07-23 11:09:41', '2020-07-23 11:09:41'),
(91, 'App\\Models\\Customer', 31, 'nid_card', '123F4884-1A3C-45AB-A7A6-A89EC3E3A10F', '123F4884-1A3C-45AB-A7A6-A89EC3E3A10F.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 83, '2020-07-23 11:09:41', '2020-07-23 11:09:41'),
(92, 'App\\Models\\Customer', 31, 'check', '87EFC6B5-0481-4A17-A1F7-EB36B35199CA', '87EFC6B5-0481-4A17-A1F7-EB36B35199CA.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 84, '2020-07-23 11:09:41', '2020-07-23 11:09:41'),
(93, 'App\\Models\\Customer', 32, 'profile', 'BD6D08F5-6A0C-433B-BB30-BDCAAC9277DB', 'BD6D08F5-6A0C-433B-BB30-BDCAAC9277DB.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 85, '2020-07-23 11:11:57', '2020-07-23 11:11:57'),
(94, 'App\\Models\\Customer', 32, 'nid_card', '88E2DEF4-55F0-4610-AD0B-819674021032', '88E2DEF4-55F0-4610-AD0B-819674021032.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 86, '2020-07-23 11:11:57', '2020-07-23 11:11:57'),
(95, 'App\\Models\\Customer', 32, 'check', '8073E1BD-3D49-4372-B0F9-CF7E54A72C9F', '8073E1BD-3D49-4372-B0F9-CF7E54A72C9F.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 87, '2020-07-23 11:11:57', '2020-07-23 11:11:57'),
(96, 'App\\Models\\Customer', 33, 'profile', 'FA2D65FF-458E-4AC9-A822-C9CDB9F14E2E', 'FA2D65FF-458E-4AC9-A822-C9CDB9F14E2E.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 88, '2020-07-23 11:17:26', '2020-07-23 11:17:26'),
(97, 'App\\Models\\Customer', 33, 'nid_card', '1F7ED403-2C47-4900-9236-38CEC1420DFE', '1F7ED403-2C47-4900-9236-38CEC1420DFE.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 89, '2020-07-23 11:17:26', '2020-07-23 11:17:26'),
(98, 'App\\Models\\Customer', 33, 'check', 'A3FF688C-2023-4BE4-8D7C-94FB2FA19FA3', 'A3FF688C-2023-4BE4-8D7C-94FB2FA19FA3.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 90, '2020-07-23 11:17:26', '2020-07-23 11:17:26'),
(99, 'App\\Models\\Customer', 34, 'profile', 'CAA2931F-1979-4995-9E2B-94A351F35696', 'CAA2931F-1979-4995-9E2B-94A351F35696.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 91, '2020-07-23 11:20:21', '2020-07-23 11:20:21'),
(100, 'App\\Models\\Customer', 34, 'nid_card', 'C92289C2-42E9-4166-A083-820CA75781B4', 'C92289C2-42E9-4166-A083-820CA75781B4.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 92, '2020-07-23 11:20:21', '2020-07-23 11:20:21'),
(101, 'App\\Models\\Customer', 34, 'check', 'D8C33C3E-B9E9-4F7F-88AA-86B972435F9C', 'D8C33C3E-B9E9-4F7F-88AA-86B972435F9C.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 93, '2020-07-23 11:20:21', '2020-07-23 11:20:21'),
(102, 'App\\Models\\Customer', 8, 'profile', 'AAEC0BD0-104A-47B1-9AA3-B77B39F85D73', 'AAEC0BD0-104A-47B1-9AA3-B77B39F85D73.jpeg', 'image/jpeg', 'public', 53530, '[]', '[]', '[]', 94, '2020-07-23 20:28:32', '2020-07-23 20:28:32'),
(103, 'App\\Models\\Customer', 25, 'profile', '12799098_159073994479388_5636269438175539958_n (2)', '12799098_159073994479388_5636269438175539958_n-(2).jpg', 'image/jpeg', 'public', 24264, '[]', '[]', '[]', 95, '2020-07-23 22:17:16', '2020-07-23 22:17:16'),
(104, 'App\\Models\\Customer', 35, 'profile', '1D4C1C98-3152-416D-ADFE-C8934C077B92', '1D4C1C98-3152-416D-ADFE-C8934C077B92.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 96, '2020-07-24 02:48:46', '2020-07-24 02:48:46'),
(105, 'App\\Models\\Customer', 35, 'nid_card', '78698C7C-5910-4DA5-B21C-56700DA7A57A', '78698C7C-5910-4DA5-B21C-56700DA7A57A.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 97, '2020-07-24 02:48:46', '2020-07-24 02:48:46'),
(106, 'App\\Models\\Customer', 35, 'check', 'E4ADAC2E-10D0-4221-ABAC-C6FE2AC22F1D', 'E4ADAC2E-10D0-4221-ABAC-C6FE2AC22F1D.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 98, '2020-07-24 02:48:46', '2020-07-24 02:48:46'),
(107, 'App\\Models\\Customer', 36, 'profile', 'F4F89655-DA79-4888-B017-992BFC973F1F', 'F4F89655-DA79-4888-B017-992BFC973F1F.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 99, '2020-07-24 02:52:44', '2020-07-24 02:52:44'),
(108, 'App\\Models\\Customer', 36, 'nid_card', 'EB6D08C1-55ED-4933-9B1A-B4A923CB0F76', 'EB6D08C1-55ED-4933-9B1A-B4A923CB0F76.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 100, '2020-07-24 02:52:44', '2020-07-24 02:52:44'),
(109, 'App\\Models\\Customer', 36, 'check', 'F60885C7-5C5C-4C2B-A867-FF2E5CF6D814', 'F60885C7-5C5C-4C2B-A867-FF2E5CF6D814.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 101, '2020-07-24 02:52:44', '2020-07-24 02:52:44'),
(110, 'App\\Models\\Customer', 37, 'profile', '8909DF8C-BD35-4C5F-BDB0-419C25DD6E33', '8909DF8C-BD35-4C5F-BDB0-419C25DD6E33.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 102, '2020-07-27 04:14:11', '2020-07-27 04:14:11'),
(111, 'App\\Models\\Customer', 37, 'nid_card', 'D601E6AA-81AD-4072-8B64-1CFD8ECCD5F9', 'D601E6AA-81AD-4072-8B64-1CFD8ECCD5F9.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 103, '2020-07-27 04:14:11', '2020-07-27 04:14:11'),
(112, 'App\\Models\\Customer', 37, 'check', '8F415D21-39D2-4441-9B2D-5905A7581590', '8F415D21-39D2-4441-9B2D-5905A7581590.jpeg', 'image/jpeg', 'public', 12383, '[]', '[]', '[]', 104, '2020-07-27 04:14:11', '2020-07-27 04:14:11'),
(113, 'App\\User', 1, 'profile', '41086296_1714391832017154_9138702816812990464_o (2)', '41086296_1714391832017154_9138702816812990464_o-(2).jpg', 'image/jpeg', 'public', 105193, '[]', '[]', '[]', 105, '2020-07-28 05:38:59', '2020-07-28 05:38:59');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(9, '2014_10_12_000000_create_users_table', 1),
(10, '2014_10_12_100000_create_password_resets_table', 1),
(11, '2019_08_19_000000_create_failed_jobs_table', 1),
(12, '2020_07_15_050901_create_customers_table', 1),
(13, '2020_07_15_091133_create_media_table', 1),
(15, '2020_07_15_152350_create_loans_table', 2),
(16, '2020_07_16_055834_create_installments_table', 3),
(20, '2020_07_16_095038_create_transactions_table', 4),
(21, '2020_07_19_180646_create_sales_table', 5),
(22, '2020_07_20_134759_create_deposits_table', 6);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sales`
--

CREATE TABLE `sales` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `sale_uid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `sell` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `commission` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tea_spend` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `other_spend` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `added_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sales`
--

INSERT INTO `sales` (`id`, `sale_uid`, `customer_id`, `date`, `sell`, `commission`, `tea_spend`, `other_spend`, `note`, `added_by`, `created_at`, `updated_at`) VALUES
(1, '5f1aae7ed6cb1', 36, '2020-07-20', '354281', '17714.05', '10500', '5785', 'COMISSION 2%BEAK.do', 1, '2020-07-24 03:48:46', '2020-07-24 04:34:58'),
(2, '5f1aafa75f555', 26, '2020-07-20', '348968', '17448.4', '2000', '800', 'robel', 1, '2020-07-24 03:53:43', '2020-07-24 03:56:40'),
(3, '5f1ab0f2943e4', 26, '2020-07-21', '39182', '1959.1', '0', '1630', 'ice/salt/', 1, '2020-07-24 03:59:14', '2020-07-24 03:59:14'),
(5, '5f1ab1ad84260', 34, '2020-07-22', '86826', '4341.3', '800', '310', 'robel', 1, '2020-07-24 04:02:21', '2020-07-24 04:18:49'),
(6, '5f1bd805424e7', 22, '2020-07-25', '798010', '39900.5', '0', '5277', 'robel', 1, '2020-07-25 00:58:13', '2020-07-25 00:58:13'),
(7, '5f1bd96390b89', 4, '2020-07-25', '220903', '11045.15', '00', '7690', 'robel', 1, '2020-07-25 01:04:03', '2020-07-27 07:13:01'),
(8, '5f1bda9f77b84', 26, '2020-07-25', '542102', '27105.1', '2500', '3923', 'robel', 1, '2020-07-25 01:09:19', '2020-07-25 01:09:19'),
(9, '5f1be5abbc46e', 36, '2020-07-25', '38458', '1922.9', '363', '0', 'nur mohama.dk', 1, '2020-07-25 01:56:27', '2020-07-25 01:56:27');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `trans_id` int(11) NOT NULL,
  `trans_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `trans_calculation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `trans_id`, `trans_type`, `trans_calculation`, `created_at`, `updated_at`) VALUES
(1, 1, 'App\\Models\\Loan', '300000', '2020-07-24 03:16:32', '2020-07-24 03:16:32'),
(2, 2, 'App\\Models\\Loan', '400000', '2020-07-24 03:17:36', '2020-07-24 03:17:36'),
(3, 3, 'App\\Models\\Loan', '1500000', '2020-07-24 03:18:06', '2020-07-24 03:18:06'),
(4, 4, 'App\\Models\\Loan', '1300000', '2020-07-24 03:18:44', '2020-07-24 03:18:44'),
(5, 5, 'App\\Models\\Loan', '1000000', '2020-07-24 03:19:18', '2020-07-24 03:19:18'),
(6, 6, 'App\\Models\\Loan', '1500000', '2020-07-24 03:21:59', '2020-07-24 03:21:59'),
(7, 7, 'App\\Models\\Loan', '1500000', '2020-07-24 03:22:35', '2020-07-24 03:22:35'),
(8, 8, 'App\\Models\\Loan', '1500000', '2020-07-24 03:24:14', '2020-07-24 03:24:14'),
(9, 9, 'App\\Models\\Loan', '800000', '2020-07-24 03:24:40', '2020-07-24 03:24:40'),
(10, 10, 'App\\Models\\Loan', '1200000', '2020-07-24 03:27:04', '2020-07-24 03:27:04'),
(11, 11, 'App\\Models\\Loan', '1500000', '2020-07-24 03:27:54', '2020-07-24 03:27:54'),
(12, 12, 'App\\Models\\Loan', '1500000', '2020-07-24 03:28:47', '2020-07-24 03:28:47'),
(13, 13, 'App\\Models\\Loan', '1000000', '2020-07-24 03:29:32', '2020-07-24 03:29:32'),
(14, 14, 'App\\Models\\Loan', '370000', '2020-07-24 03:30:18', '2020-07-24 03:30:18'),
(15, 15, 'App\\Models\\Loan', '1500000', '2020-07-24 03:31:07', '2020-07-24 03:31:07'),
(16, 16, 'App\\Models\\Loan', '1400000', '2020-07-24 03:32:03', '2020-07-24 03:32:03'),
(17, 17, 'App\\Models\\Loan', '1400000', '2020-07-24 03:33:12', '2020-07-24 03:33:12'),
(18, 18, 'App\\Models\\Loan', '1000000', '2020-07-24 03:34:14', '2020-07-24 03:34:14'),
(19, 19, 'App\\Models\\Loan', '1500000', '2020-07-24 03:35:10', '2020-07-24 03:35:10'),
(20, 20, 'App\\Models\\Loan', '1500000', '2020-07-24 03:35:38', '2020-07-24 03:35:38'),
(21, 21, 'App\\Models\\Loan', '1075000', '2020-07-24 03:36:06', '2020-07-24 03:36:06'),
(22, 22, 'App\\Models\\Loan', '1000000', '2020-07-24 03:36:34', '2020-07-24 03:36:34'),
(23, 23, 'App\\Models\\Loan', '200000', '2020-07-24 03:37:44', '2020-07-24 03:37:44'),
(24, 24, 'App\\Models\\Loan', '350000', '2020-07-24 03:38:27', '2020-07-24 03:38:27'),
(25, 25, 'App\\Models\\Loan', '200000', '2020-07-24 03:38:59', '2020-07-24 03:38:59'),
(26, 26, 'App\\Models\\Loan', '250000', '2020-07-24 03:39:32', '2020-07-24 03:39:32'),
(27, 27, 'App\\Models\\Loan', '800000', '2020-07-24 03:40:15', '2020-07-24 03:40:15'),
(28, 28, 'App\\Models\\Loan', '200000', '2020-07-24 03:41:43', '2020-07-24 03:41:43'),
(29, 1, 'App\\Models\\Installment', '0', '2020-07-24 03:48:46', '2020-07-24 04:34:58'),
(30, 2, 'App\\Models\\Installment', '1000000', '2020-07-24 03:53:43', '2020-07-24 03:57:07'),
(31, 3, 'App\\Models\\Installment', '1000000', '2020-07-24 03:59:14', '2020-07-24 03:59:14'),
(33, 5, 'App\\Models\\Installment', '300000', '2020-07-24 04:02:21', '2020-07-24 04:18:49'),
(34, 6, 'App\\Models\\Installment', '1400000', '2020-07-25 00:58:13', '2020-07-25 00:58:13'),
(35, 7, 'App\\Models\\Installment', '70000', '2020-07-25 01:04:03', '2020-07-27 07:13:01'),
(36, 8, 'App\\Models\\Installment', '1000000', '2020-07-25 01:09:19', '2020-07-25 01:09:19'),
(37, 9, 'App\\Models\\Installment', '0', '2020-07-25 01:56:27', '2020-07-25 01:56:27'),
(38, 29, 'App\\Models\\Loan', '70000', '2020-07-26 06:33:05', '2020-07-26 06:33:05');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_of_birth` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `date_of_birth`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ek Robel', 'emrulkaies14@gmail.com', '01731984851', '1989-02-27', NULL, '$2y$10$K7D0MRwCu4z1Wm9KVOC7ieMJ8Kf0Ovw20zBikTYJ6rAsnskDijAv2', NULL, '2020-07-15 10:14:50', '2020-07-28 05:41:10'),
(2, 'Test', 'test@gmail.com', '01740963472', NULL, NULL, '$2y$10$LgH4FDF4ILmeHne5DwP/T.QeWgGcQcMhq/gvFDo4.CtsSSqfYdEDu', NULL, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `costs`
--
ALTER TABLE `costs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deposits`
--
ALTER TABLE `deposits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `installments`
--
ALTER TABLE `installments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loans`
--
ALTER TABLE `loans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`),
  ADD KEY `media_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `sales`
--
ALTER TABLE `sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `costs`
--
ALTER TABLE `costs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `deposits`
--
ALTER TABLE `deposits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `installments`
--
ALTER TABLE `installments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `loans`
--
ALTER TABLE `loans`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `sales`
--
ALTER TABLE `sales`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
