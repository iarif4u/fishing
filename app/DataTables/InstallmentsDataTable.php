<?php

namespace App\DataTables;

use App\Models\Customer;
use App\Models\Installment;
use App\Models\Loan;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class InstallmentsDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('paid',function($installment){
                $taka=  Installment::where('id','<=',$installment->id)->where('customer_id','=',$installment->customer_id)->sum('taka');
                return en2bnNumber($taka);
            })->editColumn('paid',function($installment){
                return en2bnNumber($installment->paid);
            })->editColumn('taka',function($installment){
                return en2bnNumber($installment->taka);
            })->addColumn('trans',function($installment){
                return en2bnNumber($installment->transaction->trans_calculation);
            })->addColumn('customer',function($installment){
                return Customer::where('id','=',$installment->customer_id)->first()->name;
            })->filterColumn('customer', function($query, $keyword) {
                $query->first()->customer->where('name', 'like', "%{$keyword}%")->first();
            })->addColumn('action',function ($installment){
              return view('installment.datatable.actions',['installment'=>$installment]);
            })->addColumn("profile",function($installment){
                return view('customer.datatable.profile_img',['customer'=>$installment->customer]);
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param Installment $model
     * @return Builder
     */
    public function query(Installment $model)
    {
        return $model->newQuery()->with(['transaction'])->where('taka' ,'>',0);
    }

    public function dbTableDom()
    {
        return '<"row"<"col-md-12"<"row"<"col-md-6"B><"col-md-6"f> > ><"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >';
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->addTableClass("table table-hover non-hover")
            ->setTableId('installments-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->processing(false)
            ->languagePaginatePrevious('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>')
            ->languagePaginateNext('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>')
            ->languageInfo("Showing page _PAGE_ of _PAGES_")
            ->languageSearch('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>')
            ->languageSearchPlaceholder("Search...")
            ->languageLengthMenu("Results :  _MENU_")
            ->lengthMenu([7, 10, 20, 50])
            ->pageLength(10)
            ->orderBy(1);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('profile')->title("ছবি")->orderable(false),
            Column::make('customer')->title("ব্যক্তি নাম"),
            Column::make('date')->title("তারিখ"),
            Column::make('taka')->title("টাকার পরিমাপ"),
            Column::make('paid')->title("মোট আমানত/কিস্তি পরিশোধ"),
            Column::make('trans')->title("মোট দাদন বাকি আছে")->name('id'),
            Column::make('action')->title("আরও তথ্য জানান")->searchable(false)->orderable(false),
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Loans_' . date('YmdHis');
    }
}
