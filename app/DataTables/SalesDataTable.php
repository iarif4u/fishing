<?php

namespace App\DataTables;

use App\Models\Customer;
use App\Models\Sale;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Yajra\DataTables\DataTableAbstract;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class SalesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->editColumn('date',function($sale){
                return ($sale->date) ? Carbon::parse($sale->date)->format('d-m-Y') : '';
            })->filterColumn('date', function ($query, $keyword) {
                $query->whereRaw("DATE_FORMAT(date,'%d-%m-%Y') like ?", ["%$keyword%"]);
            })
            ->addColumn("action",function($sale){
                return view('sale.datatable.actions',['sale'=>$sale]);
            })->editColumn('sell',function ($sale){
                return en2bnNumber($sale->sell);
            })->editColumn('ghat',function($sale){
                return en2bnNumber($sale->ghat);
            })->editColumn('tax',function($sale){
                return en2bnNumber($sale->tax);
            })->editColumn('mosque',function($sale){
                return en2bnNumber($sale->mosque);
            })->editColumn('association',function($sale){
                return en2bnNumber($sale->association);
            })->editColumn('commission',function ($sale){
                return en2bnNumber($sale->commission);
            })->editColumn('other_spend',function ($sale){
                return en2bnNumber($sale->other_spend);
            })->editColumn('tea_spend',function ($sale){
                return en2bnNumber($sale->tea_spend);
            });
    }

    /**
     * Get query source of dataTable.
     *
     * @param Sale $model
     * @return Builder
     */
    public function query(Sale $model)
    {
        return $model->newQuery()->with('customer');
    }

    public function dbTableDom()
    {
        return '<"row"<"col-md-12"<"row"<"col-md-6"B><"col-md-6"f> > ><"col-md-12"rt> <"col-md-12"<"row"<"col-md-5"i><"col-md-7"p>>> >';
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->addTableClass("table table-hover non-hover")
            ->setTableId('sales-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->processing(false)
            ->languagePaginatePrevious('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>')
            ->languagePaginateNext('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>')
            ->languageInfo("Showing page _PAGE_ of _PAGES_")
            ->languageSearch('<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>')
            ->languageSearchPlaceholder("Search...")
            ->languageLengthMenu("Results :  _MENU_")
            ->lengthMenu([7, 10, 20, 50])
            ->pageLength(10)
            ->orderBy(1);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            Column::make('customer.name')->title("ব্যক্তি নাম"),
            Column::make('date')->title('তারিখ'),
            Column::make('sell')->title('মোট বিক্রি'),
            Column::make('commission')->title('বিক্রির কমিশন'),
            Column::make('ghat')->title('ঠেলা/ঘাট'),
            Column::make('tax')->title('খাজনা'),
            Column::make('association')->title('বিহিন্দি জাল সমিতি'),
            Column::make('mosque')->title('মসজিদ'),
            Column::make('tea_spend')->title('চা-নাস্তা খরচ'),
            Column::make('other_spend')->title('অন্যান্য খরচ'),
            Column::make('action')->title('আরও তথ্য জানান')->orderable(false)->searchable(false),
        ];
    }


    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Sales_' . date('YmdHis');
    }
}
