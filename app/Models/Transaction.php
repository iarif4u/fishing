<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = ["trans_id","trans_type", "trans_calculation"];

    /**
     * Get the owning commentable model.
     */
    public function transable()
    {
        return $this->morphTo();
    }
}
