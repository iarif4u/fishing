<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Deposit extends Model
{
    protected $fillable = ["deposit_uid","customer_id","deposit","date","note"];

    public function customer(){
        return $this->hasOne(Customer::class,'id','customer_id');
    }
}
