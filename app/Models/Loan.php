<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    protected $fillable = ["loan_uid","customer_id","taka","date","note","added_by"];

    public function customer(){
        return $this->hasOne(Customer::class,'id','customer_id');
    }
    /**
     * Get all of the transactions.
     */
    public function transaction()
    {
        return $this->morphOne(Transaction::class, 'transable',"trans_type","trans_id");
    }
}
