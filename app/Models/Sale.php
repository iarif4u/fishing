<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    protected $fillable = ["sale_uid","customer_id","date","sell","commission","tea_spend","ghat","tax","association","mosque","other_spend","note","added_by"];

    public function customer(){
        return $this->hasOne(Customer::class,'id','customer_id');
    }
    public function installment(){
        return $this->hasOne(Installment::class,'installment_uid','sale_uid')->with('transaction');
    }
}
