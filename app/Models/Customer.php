<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
class Customer extends Model implements HasMedia
{
    use HasMediaTrait;
    protected $fillable = ["name", "date_of_birth", "company", "father", "mother", "email", "phone", "address", "permanent_address","loan"];
}
