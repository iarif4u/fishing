<?php

namespace App\Http\Controllers;

use App\DataTables\CostsDataTable;
use App\Http\Requests\NewCostRequest;
use App\Http\Requests\ProfileUpdateRequest;
use App\Models\Cost;
use App\Models\Deposit;
use App\Models\Installment;
use App\Models\Loan;
use App\Models\Sale;
use App\User;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return Renderable
     */
    public function index()
    {
        $deposit = Deposit::sum('deposit');
        $loan = Loan::sum('taka');
        $installment = Installment::sum('taka');
        $sale = Sale::sum('sell');
        $today_sale = Sale::whereDate('date',today())->sum('sell');
        $today_ghat = Sale::whereDate('date',today())->sum('ghat');
        $today_tax = Sale::whereDate('date',today())->sum('tax');
        $today_association = Sale::whereDate('date',today())->sum('association');
        $today_mosque = Sale::whereDate('date',today())->sum('mosque');
        $today_installment = Installment::whereDate('date',today())->sum('taka');
        $today_commission = Sale::whereDate('date',today())->sum('commission');
        $today_spend = Sale::whereDate('date',today())->sum('other_spend');
        $commission = Sale::sum('commission');
        $other_spend = Sale::sum('other_spend');
        $tea_spend = Sale::sum('tea_spend');
        $commission_back = Cost::sum('commission');
        $basket = Cost::sum('basket');
        $today_fourth = $today_sale - $today_commission - $today_spend - $today_installment-$today_ghat-$today_tax-$today_association-$today_mosque;
        return view('dashboard', ['today_fourth'=>$today_fourth,'today_mosque'=>$today_mosque,'today_association'=>$today_association,'today_tax'=>$today_tax,'today_ghat'=>$today_ghat,'today_installment'=>$today_installment,'today_commission'=>$today_commission,'today_sale'=>$today_sale,'commission_back'=>$commission_back,'basket'=>$basket,'tea_spend' => $tea_spend, 'other_spend' => $other_spend, 'commission' => $commission, 'sale' => $sale, 'installment' => $installment, 'loan' => $loan, 'deposit' => $deposit]);
    }

    public function cost(CostsDataTable $dataTable)
    {
        return $dataTable->render('cost.cost');
    }

    public function make_new_cost(NewCostRequest $request)
    {
        if ($request->input('basket', 0) + $request->input('commission', 0) > 0) {
            Cost::create(["date" => $request->input('date'), "basket" => $request->input('basket', 0), "commission" => $request->input('commission', 0), "details" => $request->input('details')]);
            return redirect()->back()->with('message', "Cost create success");
        } else {
            return redirect()->back()->withErrors(['Cost create failed']);
        }
    }

    public function profile()
    {
        return view('profile');
    }

    public function update_profile(ProfileUpdateRequest $request)
    {
        $user = User::where(['id' => auth()->id()])->first();
        DB::beginTransaction();
        try {
            if ($request->hasFile('photo')) {
                sync_media_file($user, $request->file('photo'), config('const.media.profile'));
            }
            if ($request->input('password')) {
                $user->password = bcrypt($request->input('password'));
            }
            $user->name = $request->input('name');
            $user->email = $request->input('email');
            $user->phone = $request->input('phone');
            $user->date_of_birth = $request->input('date_of_birth');
            $user->save();
            DB::commit();
            return redirect()->back()->with('message', "Profile update successfully done");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors(["Profile update failed"]);
        }
    }

    public function cost_update_view($cost_id)
    {
        $cost = Cost::where(['id' => $cost_id])->first();
        return view('cost.cost_update', ['cost' => $cost]);
    }

    public function update_cost(NewCostRequest $request, $cost_id)
    {
        if ($request->input('basket', 0) + $request->input('commission', 0) > 0) {
            Cost::where(['id' => $cost_id])->update(["date" => $request->input('date'), "basket" => $request->input('basket', 0), "commission" => $request->input('commission', 0), "details" => $request->input('details')]);
            return redirect()->back()->with('message', "Cost update success");
        } else {
            return redirect()->back()->withErrors(['Cost update failed']);
        }
    }
    public function delete_cost($cost_id){
        Cost::where(['id' => $cost_id])->delete();
        return redirect()->back()->with('message', "Cost delete success");
    }

}
