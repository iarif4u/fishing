<?php

namespace App\Http\Controllers;

use App\DataTables\DepositsDataTable;
use App\Http\Requests\NewDepositRequest;
use App\Models\Deposit;
use App\Models\Installment;
use App\Models\Sale;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class DepositController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function new_deposit_view()
    {
        return view('deposit.new_deposit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NewDepositRequest $request
     * @return RedirectResponse
     */
    public function make_new_deposit(NewDepositRequest $request)
    {
        try {
            Deposit::create([
                "deposit_uid" => uniqid(),
                "customer_id" => $request->input('customer_id'),
                "deposit" => $request->input('deposit'),
                "date" => $request->input('date'),
                "note" => $request->input('note')
            ]);
            return redirect()->back()->with("message", "Deposit create successfully done");
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors("Deposit create failed");
        }
    }

    /**
     * Display the specified resource.
     * @param DepositsDataTable $dataTable
     * @return mixed
     */
    public function get_deposit_list(DepositsDataTable $dataTable)
    {
        return $dataTable->render('deposit.deposits');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $deposit_id
     * @return View
     */
    public function get_deposit_update_view($deposit_id)
    {
        $deposit = Deposit::with('customer')->where(['deposit_uid'=>$deposit_id])->first();

        $sell_amount = Sale::where(['customer_id' => $deposit->customer->id])->sum('sell');
        $installment_amount = Installment::where(['customer_id' => $deposit->customer->id])->sum('taka');
        $commission_amount = Sale::where(['customer_id' => $deposit->customer->id])->sum('commission');
        $spend_amount = Sale::where(['customer_id' => $deposit->customer->id])->sum('other_spend');
        $fourth = $sell_amount - $commission_amount - $spend_amount-$installment_amount;
        $previous_payment = Deposit::where(["customer_id" =>$deposit->customer->id])->sum('deposit');
        return view('deposit.edit_deposit',['deposit'=>$deposit,'fourth'=>$fourth,'previous_payment'=>$previous_payment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param NewDepositRequest $request
     * @param $deposit_id
     * @return RedirectResponse
     */
    public function update_deposit(NewDepositRequest $request,$deposit_id)
    {
        $deposit = Deposit::where(['id'=>$deposit_id])->first();
        try {
            $deposit->update([
                "customer_id" => $request->input('customer_id'),
                "deposit" => $request->input('deposit'),
                "date" => $request->input('date'),
                "note" => $request->input('note')
            ]);
            return redirect()->back()->with("message", "Deposit update successfully done");
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors("Deposit update failed");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $deposit_id
     * @return RedirectResponse
     */
    public function delete_deposit($deposit_id)
    {
        $deposit = Deposit::where(['deposit_uid'=>$deposit_id])->first();
        $deposit->delete();
        return redirect()->back()->with("message", "Deposit delete success");
    }

    public function print_deposit($deposit_id){
        $deposit = Deposit::with('customer')->where(['deposit_uid'=>$deposit_id])->first();
        return view('deposit.print',['deposit'=>$deposit]);
    }
}
