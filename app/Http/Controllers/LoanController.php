<?php

namespace App\Http\Controllers;

use App\DataTables\LoansDataTable;
use App\Http\Requests\NewLoanRequest;
use App\Http\Requests\UpdateLoanRequest;
use App\Models\Customer;
use App\Models\Loan;
use App\Models\Transaction;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class LoanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function new_loan_view()
    {
        return view('loan.new_loan');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param NewLoanRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function make_new_loan(NewLoanRequest $request)
    {
        DB::beginTransaction();
        try {
            $taka = $request->input('taka');
            $loan = Loan::create([
                "loan_uid" => uniqid(),
                "customer_id" => $request->input('customer_id'),
                "taka" => $taka,
                "date" => $request->input('date'),
                "note" => $request->input('note'),
                "added_by" => auth()->id()
            ]);
            $customer = Customer::where(['id' => $request->input('customer_id')])->first();
            $customer->increment('loan', $request->input('taka'));
            $transaction = new Transaction(['trans_calculation' => $customer->loan]);
            $loan->transaction()->save($transaction);
            DB::commit();
            return redirect()->back()->with("message", "New loan create success");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors("New loan create failed");
        }
    }

    public function get_loan_list(LoansDataTable $dataTable)
    {
        return $dataTable->render('loan.loans');
    }

    public function get_loan_update_view($loan_uid)
    {
        $loan = Loan::with('customer')->where(['loan_uid' => $loan_uid])->first();
        return view('loan.edit_loan', ['loan' => $loan]);
    }

    public function update_loan(UpdateLoanRequest $request, $loan_id)
    {
        try {
            $loan = Loan::where(['id' => $loan_id])->first();
            $prev_loan = $loan->taka;
            $diff = $request->input('taka') - $prev_loan;
            $customer_id= $request->input('customer_id');
            $loan->update([
                "customer_id" => $customer_id,
                "taka" => $request->input('taka'),
                "date" => $request->input('date'),
                "note" => $request->input('note'),
                "added_by" => auth()->id()
            ]);
            Customer::where(['id' => $request->input('customer_id')])->increment('loan', $diff);
            $loan->transaction()->increment('trans_calculation', $diff);
            $loans=Loan::with('transaction')
                ->where(['customer_id'=>$customer_id])
                ->where('id','>' ,$loan_id)
                ->get();
            foreach ($loans as $cs_loan){
                $cs_loan->transaction()->increment('trans_calculation', $diff);
            }
            DB::commit();
            return redirect()->back()->with("message", "Loan update success");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors("Loan update failed");
        }

    }

    public function delete_loan($loan_uid)
    {
        try {
            $loan = Loan::where(['loan_uid' => $loan_uid])->firstOrFail();
            Customer::where(['id' => $loan->customer_id])->decrement('loan', $loan->taka);
            $loans=Loan::with('transaction')
                ->where(['customer_id'=>$loan->customer_id])
                ->where('id','>' ,$loan->id)
                ->get();
            foreach ($loans as $cs_loan){
                $cs_loan->transaction()->decrement('trans_calculation', $loan->taka);
            }
            $loan->delete();
            DB::commit();
            return redirect()->back()->with("message", "Loan delete success");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors("Loan delete failed");
        }
    }
    public function print_loan($loan_uid){
        $loan = Loan::with('customer')->where(['loan_uid' => $loan_uid])->firstOrFail();
        return view('loan.print',['loan'=>$loan]);
    }
}
