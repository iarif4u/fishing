<?php

namespace App\Http\Controllers;

use App\DataTables\CustomersDataTable;
use App\Http\Requests\CustomerUpdateRequest;
use App\Http\Requests\NewCustomerRequest;
use App\Models\Customer;
use App\Models\Deposit;
use App\Models\Installment;
use App\Models\Loan;
use App\Models\Sale;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class CustomersController extends Controller
{
    /**
     * Display a user/customer list .
     *
     * @param CustomersDataTable $dataTable
     * @return Application|Factory|View
     */
    public function get_user_list(CustomersDataTable $dataTable)
    {
        return $dataTable->render("customer.customers");
    }

    /**
     * Display a new customer form view .
     *
     * @return Application|Factory|View
     */
    public function new_customer_view()
    {
        return view("customer.new_customer");
    }

    /**
     * Make new customer.
     *
     * @param NewCustomerRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function make_new_customer(NewCustomerRequest $request)
    {
        DB::beginTransaction();
        try {
            $customer = Customer::create([
                "name" => $request->input("name"),
                "date_of_birth" => $request->input("dob"),
                "company" => $request->input("company"),
                "father" => $request->input("father"),
                "mother" => $request->input("mother"),
                "email" => $request->input("email"),
                "phone" => $request->input("phone"),
                "address" => $request->input("address"),
                "permanent_address" => $request->input("permanent_address")
            ]);
            add_media_file($customer, $request->file("profile"), config("const.media.profile"));
            add_media_file($customer, $request->file("nid_card"), config("const.media.nid_card"));
            add_media_file($customer, $request->file("check"), config("const.media.check"));
            DB::commit();
            return redirect()->back()->with("message", "User add successfully done");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors("Users added failed");
        }
    }

    /**
     * Get customer profile
     *
     * @param $customer_id
     * @return Application|Factory|View
     */
    public function get_user_profile($customer_id)
    {
        $customer = Customer::where(['id' => $customer_id])->first();
        if ($customer){
        $deposit = Deposit::where(['customer_id' => $customer_id])->sum('deposit');
        $deposits = Deposit::where(['customer_id' => $customer_id])->get();
        $loan = Loan::where(['customer_id' => $customer_id])->sum('taka');
        $loans = Loan::where(['customer_id' => $customer_id])->get();
        $installment = Installment::where(['customer_id' => $customer_id])->sum('taka');
        $installments = Installment::with('transaction')->where(['customer_id' => $customer_id])->get();
        $sale = Sale::where(['customer_id' => $customer_id])->sum('sell');
        $commission = Sale::where(['customer_id' => $customer_id])->sum('commission');
        $other_spend = Sale::where(['customer_id' => $customer_id])->sum('other_spend');
        $tea_spend = Sale::where(['customer_id' => $customer_id])->sum('tea_spend');
        $tax = Sale::where(['customer_id' => $customer_id])->sum('tax');
        $ghat = Sale::where(['customer_id' => $customer_id])->sum('ghat');
        $association = Sale::where(['customer_id' => $customer_id])->sum('association');
        $mosque = Sale::where(['customer_id' => $customer_id])->sum('mosque');
        $fourth = $sale-$commission-$other_spend-$installment-$ghat-$association-$mosque-$tax;
        return view('customer.profile',['mosque'=>$mosque,'association'=>$association,'ghat'=>$ghat,'tax'=>$tax,'deposits'=>$deposits,'installments'=>$installments,'loans'=>$loans,'tea_spend'=>$tea_spend,'other_spend'=>$other_spend,'commission'=>$commission,'sale'=>$sale,'installment'=>$installment,'loan'=>$loan,'deposit'=>$deposit,'customer'=>$customer,'fourth'=>$fourth]);
        }else{
            return redirect()->route('users.list')->withErrors(["Customer not found"]);
        }
    }

    /**
     * Get customer profile update view
     *
     * @param $user_id
     * @return Application|Factory|View
     */
    public function get_user_update($user_id)
    {
        $customer = Customer::where(['id' => $user_id])->first();
        return view('customer.edit_customer', ['customer' => $customer]);
    }

    /**
     * Get customer profile update view
     *
     * @param $user_id
     * @param CustomerUpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update_user($user_id, CustomerUpdateRequest $request)
    {
        if ($user_id === $request->input('user_id')):
            DB::beginTransaction();
            try {
                $customer = Customer::where(['id' => $user_id])->first();
                $customer->update([
                    "name" => $request->input("name"),
                    "date_of_birth" => $request->input("dob"),
                    "company" => $request->input("company"),
                    "father" => $request->input("father"),
                    "mother" => $request->input("mother"),
                    "email" => $request->input("email"),
                    "phone" => $request->input("phone"),
                    "address" => $request->input("address"),
                    "permanent_address" => $request->input("permanent_address")
                ]);
                if ($request->hasFile('profile')) {
                    sync_media_file($customer, $request->file("profile"), config("const.media.profile"));
                }
                if ($request->hasFile('nid_card')) {
                    sync_media_file($customer, $request->file("nid_card"), config("const.media.nid_card"));
                }
                if ($request->hasFile('check')) {
                    sync_media_file($customer, $request->file("check"), config("const.media.check"));
                }
                DB::commit();
                return redirect()->back()->with("message", "User update successfully done");
            } catch (\Exception $exception) {
                DB::rollBack();
                return redirect()->back()->withErrors("Users update failed");
            }
        else:
            return redirect()->back()->withErrors("Users update failed");
        endif;
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Customer $customers
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete_user($customer_id)
    {
        try {
            Customer::where(['id' => $customer_id])->first()->delete();
            return redirect()->back()->with("message", "User delete successfully done");
        } catch (\Exception $exception) {
            return redirect()->back()->withErrors("Users delete failed");
        }

    }

    public function search_customer(Request $request)
    {
        try {
            $string = $request->input('search', '');
            $response = Customer::where('name', 'like', "%$string%")
                ->orWhere('company', 'like', "%$string%")
                ->orWhere('phone', 'like', "%$string%")
                ->orWhere('email', 'like', "%$string%")
                ->orWhere('address', 'like', "%$string%")
                ->orWhere('permanent_address', 'like', "%$string%")
                ->get();
            $customers = [];
            foreach ($response as $customer) {
                $customers[] = [
                    'id' => $customer->id,
                    'name' => $customer->name . " (" . $customer->phone . ")",
                ];
            }
            return response()->json(['total' => $response->count(), 'data' => $customers]);
        } catch (\Exception $exception) {
            return response()->json(['message' => $exception->getMessage()], 500);
        }
    }

    public function get_customer_fourth(Request $request)
    {
        $customer_id = $request->input('customer_id');
        $installment_amount = Installment::where(['customer_id' => $customer_id])->sum('taka');
        $sell_amount = Sale::where(['customer_id' => $customer_id])->sum('sell');
        $commission_amount = Sale::where(['customer_id' => $customer_id])->sum('commission');
        $spend_amount = Sale::where(['customer_id' => $customer_id])->sum('other_spend');
        $fourth = $sell_amount - $commission_amount - $spend_amount-$installment_amount;
        $previous_payment = Deposit::where(["customer_id" => $request->input('customer_id')])->sum('deposit');
        return response()->json(['fourth' => $fourth,'previous_payment'=>$previous_payment]);
    }
}
