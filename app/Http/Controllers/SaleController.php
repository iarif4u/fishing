<?php

namespace App\Http\Controllers;

use App\DataTables\SalesDataTable;
use App\Http\Requests\NewSaleRequest;
use App\Models\Customer;
use App\Models\Installment;
use App\Models\Sale;
use App\Models\Transaction;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class SaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function new_sell_view()
    {
        return view('sale.new_sale');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    /**
     * Store a newly created resource in storage.
     *
     * @param NewSaleRequest $request
     * @return RedirectResponse
     */
    public function make_new_sell(NewSaleRequest $request)
    {
        $sell = $request->input('sell');
        $commission = calculate_percentage($sell, env('COMMISION', 5));
        $tea_spend =  $request->input('tea_spend');
        DB::beginTransaction();
        try {
            $unique_id  = uniqid();
            Sale::create([
                "sale_uid" =>$unique_id ,
                "customer_id" => $request->input('customer_id'),
                "date" => $request->input('date'),
                "sell" => $sell,
                "commission" => $commission,
                "tea_spend" => $tea_spend,
                "ghat" => $request->input('ghat'),
                "tax" => $request->input('tax'),
                "association" => $request->input('association'),
                "mosque" => $request->input('mosque'),
                "other_spend" => $request->input('other_spend'),
                "note" => $request->input('note'),
                "added_by" => auth()->id()
            ]);
            $installment=Installment::create([
                "installment_uid"=>$unique_id,
                "customer_id"=>$request->input('customer_id'),
                "taka"=>$request->input('installment'),
                "date" => $request->input('date'),
                "note" => $request->input('note'),
                "added_by"=>auth()->id()
            ]);
            $customer = Customer::where(['id' => $request->input('customer_id')])->first();
            $customer->decrement('loan', $request->input('installment'));
            $transaction = new Transaction(['trans_calculation' => $customer->loan]);
            $installment->transaction()->save($transaction);
            DB::commit();
            return redirect()->back()->with("message", "Sale create success");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors("Sale create failed");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param SalesDataTable $dataTable
     * @return Response
     */
    public function get_sale_list(SalesDataTable $dataTable)
    {
        return $dataTable->render('sale.sales');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return View
     */
    public function get_sale_update_view($sale_id)
    {

        $sale = Sale::with('customer')->where(['id' => $sale_id])->first();
        $installment = Installment::where(['installment_uid' => $sale->sale_uid])->first();
        return view('sale.edit_sale', ['sale' => $sale,'installment'=>$installment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param NewSaleRequest $request
     * @param $sale_id
     * @return RedirectResponse
     */
    public function update_sale(NewSaleRequest $request, $sale_id)
    {
        DB::beginTransaction();
        try {
            $customer_id =$request->input('customer_id');
            $sale = Sale::with(['customer','installment'])->where(['id' => $sale_id])->first();
            $installment = $sale->installment;
            $prev_installment = $installment->taka;
            $diff = $prev_installment - $request->input('installment');
            $installment->update([
                "customer_id" => $customer_id,
                "taka" => $request->input('installment'),
                "date" => $request->input('date'),
                "note" => $request->input('note'),
                "added_by" => auth()->id()
            ]);

            Customer::where(['id' => $customer_id])->increment('loan', $diff);
            $installment->transaction()->increment('trans_calculation', $diff);
            $Installments = Installment::with('transaction')
                ->where(['customer_id' => $customer_id])
                ->where('id', '>', $installment->id)
                ->get();
            foreach ($Installments as $cs_installment) {
                $cs_installment->transaction()->increment('trans_calculation', $diff);
            }
            $sell = $request->input('sell');
            $commission = calculate_percentage($sell, env('COMMISION', 5));
            $tea_spend =  $request->input('tea_spend');
            $sale->update([
                "customer_id" => $request->input('customer_id'),
                "date" => $request->input('date'),
                "sell" => $sell,
                "commission" => $commission,
                "tea_spend" => $tea_spend,
                "ghat" => $request->input('ghat'),
                "tax" => $request->input('tax'),
                "association" => $request->input('association'),
                "mosque" => $request->input('mosque'),
                "other_spend" => $request->input('other_spend'),
                "note" => $request->input('note'),
                "added_by" => auth()->id()
            ]);
            DB::commit();
            return redirect()->back()->with("message", "Sale update success");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors("Sale update failed");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $sale_id
     * @return RedirectResponse
     */
    public function delete_sale($sale_id)
    {
        $sale = Sale::where(['id'=>$sale_id])->first();
        $installment = $sale->installment;
        Customer::where(['id' => $sale->customer_id])->increment('loan', $installment->taka);
        $installments = Installment::with('transaction')
            ->where(['customer_id' => $sale->customer_id])
            ->where('id', '>', $installment->id)
            ->get();
        foreach ($installments as $cs_installment) {
            $cs_installment->transaction()->increment('trans_calculation', $installment->taka);
        }
        $installment->transaction->delete();
        $installment->delete();
        $sale->delete();
        return redirect()->back()->with("message", "Sale delete success");
    }

    public function get_sale_print_view($sale_id){
        $sale = Sale::with('customer')->where(['id'=>$sale_id])->first();
        $total_spend = $sale->commission+$sale->ghat+$sale->tax+$sale->other_spend+$sale->mosque+$sale->association;
        return view('sale.print',['sale'=>$sale,'total_spend'=>$total_spend]);
    }
}


