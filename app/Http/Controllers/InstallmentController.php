<?php

namespace App\Http\Controllers;

use App\DataTables\InstallmentsDataTable;
use App\Http\Requests\NewInstallmentRequest;
use App\Models\Customer;
use App\Models\Installment;
use App\Models\Loan;
use App\Models\Transaction;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class InstallmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function new_installment_view()
    {
        return view('installment.new_installment');
    }


    /**
     * Store a newly created Installment in storage.
     *
     * @param NewInstallmentRequest $request
     * @return RedirectResponse
     */
    public function make_new_installment(NewInstallmentRequest $request)
    {
        DB::beginTransaction();
        try {
            $installment = Installment::create([
                "installment_uid" => uniqid(),
                "customer_id" => $request->input('customer_id'),
                "taka" => $request->input('taka'),
                "date" => $request->input('date'),
                "note" => $request->input('note'),
                "added_by" => auth()->id()
            ]);
            $customer = Customer::where(['id' => $request->input('customer_id')])->first();
            $customer->decrement('loan', $request->input('taka'));
            $transaction = new Transaction(['trans_calculation' => $customer->loan]);
            $installment->transaction()->save($transaction);
            DB::commit();
            return redirect()->back()->with("message", "New installment create success");
        } catch (\Exception $exception) {
            DB::rollBack();
            dd($exception->getMessage());
            return redirect()->back()->withErrors("New installment create failed");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param InstallmentsDataTable $dataTable
     * @return Response
     */
    public function get_installment_list(InstallmentsDataTable $dataTable)
    {
        return $dataTable->render('installment.installments');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $installment_uid
     * @return View
     */
    public function get_installment_update_view($installment_uid)
    {
        $installment = Installment::with('customer')->where(['installment_uid' => $installment_uid])->first();
        return view('installment.edit_installment', ['installment' => $installment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $installment_uid
     * @return RedirectResponse
     */
    public function update_installment(NewInstallmentRequest $request, $installment_uid)
    {
        DB::beginTransaction();
        try {
            $installment = Installment::with('transaction')->where(['installment_uid' => $installment_uid])->first();
            $prev_installment = $installment->taka;
            $customer_id = $request->input('customer_id');
            $diff = $prev_installment - $request->input('taka');
            $installment->update([
                "customer_id" => $customer_id,
                "taka" => $request->input('taka'),
                "date" => $request->input('date'),
                "note" => $request->input('note'),
                "added_by" => auth()->id()
            ]);
            Customer::where(['id' => $request->input('customer_id')])->increment('loan', $diff);
            $installment->transaction()->increment('trans_calculation', $diff);
            $Installments = Installment::with('transaction')
                ->where(['customer_id' => $customer_id])
                ->where('id', '>', $installment->id)
                ->get();
            foreach ($Installments as $cs_installment) {
                $cs_installment->transaction()->increment('trans_calculation', $diff);
            }
            DB::commit();
            return redirect()->back()->with("message", "Installment update success");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors("Installment update failed");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $installment_uid
     * @return RedirectResponse
     */
    public function delete_installment($installment_uid)
    {
        DB::beginTransaction();
        try {
            $installment = Installment::with('transaction')->where(['installment_uid' => $installment_uid])->first();
            Customer::where(['id' => $installment->customer_id])->increment('loan', $installment->taka);
            $installments = Installment::with('transaction')
                ->where(['customer_id' => $installment->customer_id])
                ->where('id', '>', $installment->id)
                ->get();
            foreach ($installments as $cs_installment) {
                $cs_installment->transaction()->increment('trans_calculation', $installment->taka);
            }
            $installment->transaction->delete();
            $installment->delete();
            DB::commit();
            return redirect()->back()->with("message", "Installment delete success");
        } catch (\Exception $exception) {
            DB::rollBack();
            return redirect()->back()->withErrors("Installment delete failed");
        }
    }
    public function print_installment($installment_uid){
        $installment = Installment::with(['transaction','customer'])->where(['installment_uid' => $installment_uid])->first();
        return view("installment.print",['installment'=>$installment]);
    }
}
