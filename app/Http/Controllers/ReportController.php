<?php

namespace App\Http\Controllers;

use App\Http\Requests\OtherCostsReportRequest;
use App\Http\Requests\UserLoanReportRequest;
use App\Models\Cost;
use App\Models\Customer;
use App\Models\Deposit;
use App\Models\Installment;
use App\Models\Loan;
use App\Models\Sale;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function get_all_user_report()
    {
        $customers = Customer::all();
        return view('report.all_users', ['customers' => $customers]);
    }

    public function print_all_user_report()
    {
        $customers = Customer::all();
        return view('report.all_users_print', ['customers' => $customers]);
    }

    public function get_user_loan_report_view()
    {
        return view('report.user_loan');
    }

    public function get_user_loan_report(UserLoanReportRequest $request)
    {
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        $customer_id = $request->input('customer_id');
        $customer = Customer::where(['id' => $customer_id])->first();
        $loans = Loan::with('customer')->whereBetween('date', [$start_date, $end_date])->where(['customer_id' => $customer_id])->get();
        return view('report.user_loan', ['loans' => $loans, 'customer' => $customer]);
    }

    public function print_user_loan_report(UserLoanReportRequest $request)
    {
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        $customer_id = $request->input('customer_id');
        $customer = Customer::where(['id' => $customer_id])->first();
        $loans = Loan::with('customer')->whereBetween('date', [$start_date, $end_date])->where(['customer_id' => $customer_id])->get();
        return view('report.user_loan_print', ['loans' => $loans, 'customer' => $customer]);
    }

    public function get_user_sale_report_view()
    {
        return view('report.individual_sales');
    }

    public function print_user_sale_report(UserLoanReportRequest $request)
    {

        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        $customer_id = $request->input('customer_id');
        $sales = Sale::with('customer')->whereBetween('date', [$start_date, $end_date])->where(['customer_id' => $customer_id])->get();
        $installments = Installment::with('customer')
            ->whereBetween('date', [$start_date, $end_date])
            ->where(['customer_id' => $customer_id])
            ->where('taka', '>', 0)
            ->get();
        $customer = Customer::where(['id' => $customer_id])->first();
        $results = [];
        foreach ($sales as $sale) {
            $results[$sale->date]['customer'] = $sale->customer->name;
            $results[$sale->date]['date'] = $sale->date;
            if (array_key_exists('sell', $results[$sale->date])) {
                $results[$sale->date]['sell'] += $sale->sell;
            } else {
                $results[$sale->date]['sell'] = $sale->sell;
            }
            if (array_key_exists('commission', $results[$sale->date])) {
                $results[$sale->date]['commission'] += $sale->commission;
            } else {
                $results[$sale->date]['commission'] = $sale->commission;
            }
            if (array_key_exists('ghat', $results[$sale->date])) {
                $results[$sale->date]['ghat'] += $sale->ghat;
            } else {
                $results[$sale->date]['ghat'] = $sale->ghat;
            }
            if (array_key_exists('tax', $results[$sale->date])) {
                $results[$sale->date]['tax'] += $sale->tax;
            } else {
                $results[$sale->date]['tax'] = $sale->tax;
            }
            if (array_key_exists('association', $results[$sale->date])) {
                $results[$sale->date]['association'] += $sale->association;
            } else {
                $results[$sale->date]['association'] = $sale->association;
            }
            if (array_key_exists('mosque', $results[$sale->date])) {
                $results[$sale->date]['mosque'] += $sale->mosque;
            } else {
                $results[$sale->date]['mosque'] = $sale->mosque;
            }
            if (array_key_exists('other_spend', $results[$sale->date])) {
                $results[$sale->date]['other_spend'] += $sale->other_spend;
            } else {
                $results[$sale->date]['other_spend'] = $sale->other_spend;
            }
            if (array_key_exists('tea_spend', $results[$sale->date])) {
                $results[$sale->date]['tea_spend'] += $sale->tea_spend;
            } else {
                $results[$sale->date]['tea_spend'] = $sale->tea_spend;
            }
        }
        foreach ($installments as $installment) {

            $results[$installment->date]['customer'] = $installment->customer->name;
            $results[$installment->date]['date'] = $installment->date;
            if (array_key_exists('taka', $results[$installment->date])) {
                $results[$installment->date]['taka'] += $installment->taka;
            } else {
                $results[$installment->date]['taka'] = $installment->taka;
            }
        }
        return view('report.user_sales_print', ['customer' => $customer, 'results' => $results, 'sales' => $sales, 'installments' => $installments]);
    }

    public function get_user_sale_report(UserLoanReportRequest $request)
    {
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        $customer_id = $request->input('customer_id');
        $sales = Sale::with('customer')->whereBetween('date', [$start_date, $end_date])->where(['customer_id' => $customer_id])->get();
        $installments = Installment::with('customer')
            ->whereBetween('date', [$start_date, $end_date])
            ->where(['customer_id' => $customer_id])
            ->where('taka', '>', 0)
            ->get();
        $customer = Customer::where(['id' => $customer_id])->first();
        $results = [];
        foreach ($sales as $sale) {
            $results[$sale->date]['company'] = $sale->customer->company;
            $results[$sale->date]['customer'] = $sale->customer->name;
            $results[$sale->date]['date'] = $sale->date;
            if (array_key_exists('sell', $results[$sale->date])) {
                $results[$sale->date]['sell'] += $sale->sell;
            } else {
                $results[$sale->date]['sell'] = $sale->sell;
            }
            if (array_key_exists('commission', $results[$sale->date])) {
                $results[$sale->date]['commission'] += $sale->commission;
            } else {
                $results[$sale->date]['commission'] = $sale->commission;
            }
            if (array_key_exists('ghat', $results[$sale->date])) {
                $results[$sale->date]['ghat'] += $sale->ghat;
            } else {
                $results[$sale->date]['ghat'] = $sale->ghat;
            }
            if (array_key_exists('tax', $results[$sale->date])) {
                $results[$sale->date]['tax'] += $sale->tax;
            } else {
                $results[$sale->date]['tax'] = $sale->tax;
            }
            if (array_key_exists('association', $results[$sale->date])) {
                $results[$sale->date]['association'] += $sale->association;
            } else {
                $results[$sale->date]['association'] = $sale->association;
            }
            if (array_key_exists('mosque', $results[$sale->date])) {
                $results[$sale->date]['mosque'] += $sale->mosque;
            } else {
                $results[$sale->date]['mosque'] = $sale->mosque;
            }
            if (array_key_exists('other_spend', $results[$sale->date])) {
                $results[$sale->date]['other_spend'] += $sale->other_spend;
            } else {
                $results[$sale->date]['other_spend'] = $sale->other_spend;
            }
            if (array_key_exists('tea_spend', $results[$sale->date])) {
                $results[$sale->date]['tea_spend'] += $sale->tea_spend;
            } else {
                $results[$sale->date]['tea_spend'] = $sale->tea_spend;
            }
        }
        foreach ($installments as $installment) {

            $results[$installment->date]['customer'] = $installment->customer->name;
            $results[$installment->date]['company'] = $installment->customer->company;
            $results[$installment->date]['date'] = $installment->date;
            if (array_key_exists('taka', $results[$installment->date])) {
                $results[$installment->date]['taka'] += $installment->taka;
            } else {
                $results[$installment->date]['taka'] = $installment->taka;
            }
        }

        return view('report.individual_sales', ['customer' => $customer, 'results' => $results, 'sales' => $sales, 'installments' => $installments]);
    }


    public function get_user_deposit_report_view()
    {
        return view('report.individual_deposit');
    }

    public function get_user_deposit_report(UserLoanReportRequest $request)
    {
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        $customer_id = $request->input('customer_id');
        $deposits = Deposit::with('customer')->whereBetween('date', [$start_date, $end_date])->where(['customer_id' => $customer_id])->get();
        $fourth = get_customer_fourth_by_date($customer_id, $start_date);
        $customer = Customer::where(['id' => $customer_id])->first();
        return view('report.individual_deposit', ['customer' => $customer, 'deposits' => $deposits, 'fourth' => $fourth]);
    }

    public function print_user_deposit_report(UserLoanReportRequest $request)
    {
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        $customer_id = $request->input('customer_id');
        $deposits = Deposit::with('customer')->whereBetween('date', [$start_date, $end_date])->where(['customer_id' => $customer_id])->get();
        $fourth = get_customer_fourth_by_date($customer_id, $start_date);
        $customer = Customer::where(['id' => $customer_id])->first();
        return view('report.user_deposit_print', ['customer' => $customer, 'deposits' => $deposits, 'fourth' => $fourth]);
    }

    public function get_user_individual_transaction_report_view()
    {
        return view('report.individual_transaction');
    }

    public function get_user_individual_transaction_report(UserLoanReportRequest $request)
    {
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        $customer_id = $request->input('customer_id');
        $deposit = Deposit::where(['customer_id' => $customer_id])->whereBetween('date', [$start_date, $end_date])->sum('deposit');
        $loan = Loan::where(['customer_id' => $customer_id])->whereBetween('date', [$start_date, $end_date])->sum('taka');
        $installment = Installment::where(['customer_id' => $customer_id])->whereBetween('date', [$start_date, $end_date])->sum('taka');
        $sale = Sale::where(['customer_id' => $customer_id])->whereBetween('date', [$start_date, $end_date])->sum('sell');
        $commission = Sale::where(['customer_id' => $customer_id])->whereBetween('date', [$start_date, $end_date])->sum('commission');
        $other_spend = Sale::where(['customer_id' => $customer_id])->whereBetween('date', [$start_date, $end_date])->sum('other_spend');
        $fourth = $sale - $commission - $other_spend - $installment;
        $customer = Customer::where(['id' => $customer_id])->first();
        return view('report.individual_transaction', ['other_spend' => $other_spend, 'commission' => $commission, 'sale' => $sale, 'installment' => $installment, 'loan' => $loan, 'deposit' => $deposit, 'customer' => $customer, 'fourth' => $fourth]);
    }

    public function print_user_individual_transaction_report(UserLoanReportRequest $request)
    {
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        $customer_id = $request->input('customer_id');
        $deposit = Deposit::where(['customer_id' => $customer_id])->whereBetween('date', [$start_date, $end_date])->sum('deposit');
        $loan = Loan::where(['customer_id' => $customer_id])->whereBetween('date', [$start_date, $end_date])->sum('taka');
        $installment = Installment::where(['customer_id' => $customer_id])->whereBetween('date', [$start_date, $end_date])->sum('taka');
        $sale = Sale::where(['customer_id' => $customer_id])->whereBetween('date', [$start_date, $end_date])->sum('sell');
        $commission = Sale::where(['customer_id' => $customer_id])->whereBetween('date', [$start_date, $end_date])->sum('commission');
        $other_spend = Sale::where(['customer_id' => $customer_id])->whereBetween('date', [$start_date, $end_date])->sum('other_spend');
        $fourth = $sale - $commission - $other_spend - $installment;
        $customer = Customer::where(['id' => $customer_id])->first();
        return view('report.user_transactions_print', ['other_spend' => $other_spend, 'commission' => $commission, 'sale' => $sale, 'installment' => $installment, 'loan' => $loan, 'deposit' => $deposit, 'customer' => $customer, 'fourth' => $fourth]);
    }

    public function get_others_cost_report_view()
    {
        return view('report.others_cost');
    }

    public function get_others_cost_report(OtherCostsReportRequest $request)
    {
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        //$deposit = Deposit::where(['customer_id' => $customer_id])->whereBetween('date', [$start_date, $end_date])->sum('deposit');
        $costs = Cost::whereBetween('date', [$start_date, $end_date])->get();
        return view('report.others_cost', ['costs' => $costs]);
    }

    public function print_others_cost_report(OtherCostsReportRequest $request)
    {
        $start_date = $request->input('start_date');
        $end_date = $request->input('end_date');
        //$deposit = Deposit::where(['customer_id' => $customer_id])->whereBetween('date', [$start_date, $end_date])->sum('deposit');
        $costs = Cost::whereBetween('date', [$start_date, $end_date])->get();
        return view('report.other_cost_print', ['costs' => $costs]);
    }
}
