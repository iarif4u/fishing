<?php

namespace App\Http\Requests;

class NewSaleRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_id' => 'required|exists:customers,id',
            'installment' => 'required',
            'commission' => 'required',
            'sell' => 'required',
            'tea_spend' => 'required',
            'ghat' => 'nullable|numeric',
            'tax' => 'nullable|numeric',
            'association' => 'nullable|numeric',
            'mosque' => 'nullable|numeric',
            'other_spend' => 'required',
            'date' => 'required|date|date_format:Y-m-d|before_or_equal:today',
            'note' => 'nullable'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'customer_id.required' => 'Customer is required',
            'customer_id.exists' => 'Customer is invalid',
            'tea_spend.required' => 'Tea spend amount is required',
            'other_spend.required' => 'Others spend amount is required',
            'installment.required' => 'Installment is required',
            'sell.required' => 'Sell amount is required',
            'commission.required' => 'Commission is required',
            'date.required' => 'Date is required',
            'date.date' => 'Date is invalid',
            'date.date_format' => 'Date has invalid format',
            'date.before_or_equal' => 'Date must be before today',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'customer_id' => 'trim|escape|strip_tags',
            'installment' => 'trim|escape|strip_tags',
            'sell' => 'trim|escape|strip_tags',
            'commission' => 'trim|escape|strip_tags',
            'tea_spend' => 'trim|escape|strip_tags',
            'other_spend' => 'trim|escape|strip_tags',
            'note' => 'trim|escape|strip_tags',
            'date' => 'trim|escape|strip_tags'
        ];
    }
}
