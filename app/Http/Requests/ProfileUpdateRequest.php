<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileUpdateRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'password' => 'nullable|min:6',
            'email' => 'required|email',
            'phone' => 'required|phone:AUTO,BD',
            'date_of_birth' => 'required|date|date_format:Y-m-d|before_or_equal:today',
            'photo' => 'nullable|file|image',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Name is required',
            'password.min' => 'Password is invalid required',
            'email.required' => 'Email address is required',
            'email.email' => 'Email address is invalid',
            'phone.required' => 'Phone number is required',
            'phone.phone' => 'Phone number is invalid',
            'date_of_birth.required' => 'Date of birth is required',
            'date_of_birth.date' => 'Date of birth is invalid',
            'date_of_birth.date_format' => 'Date of birth has invalid format',
            'date_of_birth.before_or_equal' => 'Date of birth must be before today',
            'photo.file' => 'Profile image must be a file',
            'photo.image' => 'Profile image is invalid',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'name' => 'trim|escape|strip_tags',
            'date_of_birth' => 'trim|escape|strip_tags',
            'phone' => 'trim|escape|strip_tags',
            'email' => 'trim|escape|strip_tags',
            'password' => 'trim|escape|strip_tags',
        ];
    }
}
