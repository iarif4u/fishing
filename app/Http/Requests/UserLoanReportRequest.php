<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserLoanReportRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_id' => 'required|exists:customers,id',
            'start_date' => 'required|date|date_format:Y-m-d|before_or_equal:today',
            'end_date' => 'required|date|date_format:Y-m-d|before_or_equal:today'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'customer_id.required' => 'Customer is required',
            'customer_id.exists' => 'Customer is invalid',
            'start_date.required' => 'Start date is required',
            'start_date.date' => 'Start date is invalid',
            'start_date.date_format' => 'Start date has invalid format',
            'start_date.before_or_equal' => 'Start date must be before today',
            'end_date.required' => 'End date is required',
            'end_date.date' => 'End date is invalid',
            'end_date.date_format' => 'End date has invalid format',
            'end_date.before_or_equal' => 'End date must be before today',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'customer_id' => 'trim|escape|strip_tags',
            'start_date' => 'trim|escape|strip_tags',
            'end_date' => 'trim|escape|strip_tags',
        ];
    }
}
