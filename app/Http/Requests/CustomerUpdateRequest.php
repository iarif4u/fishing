<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CustomerUpdateRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_id = request()->segment(3);
        return [
            'name' => 'required',
            'user_id' => 'required|exists:customers,id',
            'father' => 'required',
            'mother' => 'required',
            'address' => 'required',
            'permanent_address' => 'required',
            'email' => 'required|email|unique:customers,email,'.$user_id,
            'company' => 'required|string|max:50',
            'phone' => 'required|phone:AUTO,BD|unique:customers,phone,'.$user_id,
            'dob' => 'required|date|date_format:Y-m-d|before_or_equal:today',
            'profile' => 'nullable|file|image',
            'nid_card' => 'nullable|file|image',
            'check' => 'nullable|file|image',
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'user_id.required' => 'User is required',
            'user_id.exists' => 'User is invalid',
            'name.required' => 'Name is required',
            'father.required' => "Father's name is required",
            'mother.required' => "Mother's name is required",
            'address.required' => 'Address is required',
            'permanent_address.required' => 'Permanent address required',
            'email.required' => 'Email address is required',
            'email.email' => 'Email address is invalid',
            'email.unique' => 'The email address has already been taken',
            'company.required' => 'Company name is required',
            'phone.required' => 'Phone number is required',
            'phone.phone' => 'Phone number is invalid',
            'phone.unique' => 'The phone number has already been taken',
            'dob.required' => 'Date of birth is required',
            'dob.date' => 'Date of birth is invalid',
            'dob.date_format' => 'Date of birth has invalid format',
            'dob.before_or_equal' => 'Date of birth must be before today',
            'profile.required' => 'Profile image is required',
            'profile.file' => 'Profile image must be a file',
            'profile.image' => 'Profile image is invalid',
            'nid_card.required' => 'NID card is required',
            'nid_card.file' => 'NID card image is invalid',
            'nid_card.image' => 'NID card image is invalid',
            'check.required' => 'Check is required',
            'check.file' => 'Check image is invalid',
            'check.image' => 'Check image is invalid',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'name' => 'trim|escape|strip_tags',
            'father' => 'trim|escape|strip_tags',
            'mother' => 'trim|escape|strip_tags',
            'address' => 'trim|escape|strip_tags',
            'phone' => 'trim|escape|strip_tags',
            'permanent_address' => 'trim|escape|strip_tags',
            'email' => 'trim|escape|strip_tags',
            'company' => 'trim|escape|strip_tags',
            'dob' => 'trim|lowercase'
        ];
    }
}
