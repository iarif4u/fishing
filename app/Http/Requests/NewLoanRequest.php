<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewLoanRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'customer_id' => 'required|exists:customers,id',
            'taka' => 'required',
            'date' => 'required|date|date_format:Y-m-d|before_or_equal:today',
            'note' => 'nullable'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'customer_id.required' => 'Customer is required',
            'customer_id.exists' => 'Customer is invalid',
            'taka.required' => 'Taka is required',
            'note.required' => "Note is required",
            'date.required' => 'Date is required',
            'date.date' => 'Date is invalid',
            'date.date_format' => 'Date has invalid format',
            'date.before_or_equal' => 'Date must be before today',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'customer_id' => 'trim|escape|strip_tags',
            'taka' => 'trim|escape|strip_tags',
            'note' => 'trim|escape|strip_tags',
            'date' => 'trim|escape|strip_tags'
        ];
    }
}
