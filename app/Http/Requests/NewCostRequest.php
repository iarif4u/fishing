<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewCostRequest extends BaseFormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date|date_format:Y-m-d|before_or_equal:today',
            'details' => 'required'
        ];
    }

    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'details.required' => "Details is required",
            'date.required' => 'Date is required',
            'date.date' => 'Date is invalid',
            'date.date_format' => 'Date has invalid format',
            'date.before_or_equal' => 'Date must be before today',
        ];
    }

    /**
     *  Filters to be applied to the input.
     *
     * @return array
     */
    public function filters()
    {
        return [
            'basket' => 'trim|escape|strip_tags',
            'commission' => 'trim|escape|strip_tags',
            'details' => 'trim|escape|strip_tags',
            'date' => 'trim|escape|strip_tags'
        ];
    }
}
