<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesTable extends Migration
{
    //add columns ALTER TABLE `sales` ADD `ghat` VARCHAR(64) NULL AFTER `tea_spend`, ADD `tax` VARCHAR(64) NULL AFTER `ghat`, ADD `association` VARCHAR(64) NULL AFTER `tax`, ADD `mosque` VARCHAR(64) NULL AFTER `association`;
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->id();
            $table->string('sale_uid');
            $table->integer('customer_id');
            $table->date('date');
            $table->string('sell');
            $table->string('commission');
            $table->string('tea_spend');
            $table->string('ghat');
            $table->string('tax');
            $table->string('association');
            $table->string('mosque');
            $table->string('other_spend');
            $table->string('note')->nullable();
            $table->integer('added_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
