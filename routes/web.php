<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('home');
});
Auth::routes(['register' => false]);
Route::group(['middleware' => 'auth'], function () {
    Route::get('/print', 'HomeController@print_invoice')->name('print');
    Route::get('/profile', 'HomeController@profile')->name('profile');
    Route::post('/profile', 'HomeController@update_profile')->name('profile');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::post('/cost', 'HomeController@make_new_cost')->name('cost');
    Route::get('/cost', 'HomeController@cost')->name('cost');
    Route::get('/cost/{cost_id}/edit', 'HomeController@cost_update_view')->name('cost_edit');
    Route::post('/cost/{cost_id}/edit', 'HomeController@update_cost')->name('cost_edit');
    Route::get('/cost/{cost_id}/delete', 'HomeController@delete_cost')->name('cost_delete');
    Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
        Route::get('/fourth', 'CustomersController@get_customer_fourth')->name('fourth');
        Route::get('/add', 'CustomersController@new_customer_view')->name('add');
        Route::post('/add', 'CustomersController@make_new_customer');
        Route::get('/search', 'CustomersController@search_customer')->name('search');
        Route::get('/', 'CustomersController@get_user_list')->name('list');
        Route::get('/{user_id}', 'CustomersController@get_user_profile')->name('profile');
        Route::get('/edit/{user_id}', 'CustomersController@get_user_update')->name('edit');
        Route::post('/edit/{user_id}', 'CustomersController@update_user')->name('edit');
        Route::get('/delete/{user_id}', 'CustomersController@delete_user')->name('delete');
    });
    Route::group(['prefix' => 'loan', 'as' => 'loan.'], function () {
        Route::get('/add', 'LoanController@new_loan_view')->name('add');
        Route::post('/add', 'LoanController@make_new_loan');
        Route::get('/', 'LoanController@get_loan_list')->name('list');
        Route::get('/{user_id}', 'LoanController@get_user_profile')->name('profile');
        Route::get('/edit/{loan_id}', 'LoanController@get_loan_update_view')->name('edit');
        Route::post('/edit/{loan_id}', 'LoanController@update_loan')->name('edit');
        Route::get('/delete/{loan_id}', 'LoanController@delete_loan')->name('delete');
        Route::get('/print/{loan_id}', 'LoanController@print_loan')->name('print');

    });
    Route::group(['prefix' => 'installment', 'as' => 'installment.'], function () {
        Route::get('/add', 'InstallmentController@new_installment_view')->name('add');
        Route::post('/add', 'InstallmentController@make_new_installment');
        Route::get('/', 'InstallmentController@get_installment_list')->name('list');
        Route::get('/{user_id}', 'InstallmentController@get_user_profile')->name('profile');
        Route::get('/edit/{installment_id}', 'InstallmentController@get_installment_update_view')->name('edit');
        Route::post('/edit/{installment_id}', 'InstallmentController@update_installment')->name('edit');
        Route::get('/delete/{installment_id}', 'InstallmentController@delete_installment')->name('delete');
        Route::get('/print/{installment_id}', 'InstallmentController@print_installment')->name('print');
    });
    Route::group(['prefix' => 'sale', 'as' => 'sale.'], function () {
        Route::get('/add', 'SaleController@new_sell_view')->name('add');
        Route::post('/add', 'SaleController@make_new_sell');
        Route::get('/', 'SaleController@get_sale_list')->name('list');
        Route::get('/{user_id}', 'SaleController@get_user_profile')->name('profile');
        Route::get('/edit/{installment_id}', 'SaleController@get_sale_update_view')->name('edit');
        Route::get('/print/{sell_id}', 'SaleController@get_sale_print_view')->name('print');
        Route::post('/edit/{installment_id}', 'SaleController@update_sale')->name('edit');
        Route::get('/delete/{installment_id}', 'SaleController@delete_sale')->name('delete');
    });
    Route::group(['prefix' => 'deposit', 'as' => 'deposit.'], function () {
        Route::get('/add', 'DepositController@new_deposit_view')->name('add');
        Route::post('/add', 'DepositController@make_new_deposit');
        Route::get('/', 'DepositController@get_deposit_list')->name('list');
        Route::get('/{user_id}', 'DepositController@get_user_profile')->name('profile');
        Route::get('/edit/{installment_id}', 'DepositController@get_deposit_update_view')->name('edit');
        Route::post('/edit/{installment_id}', 'DepositController@update_deposit')->name('edit');
        Route::get('/delete/{installment_id}', 'DepositController@delete_deposit')->name('delete');
        Route::get('/print/{installment_id}', 'DepositController@print_deposit')->name('print');
    });
    Route::group(['prefix' => 'reports', 'as' => 'reports.'], function () {
        Route::get('/others/cost', 'ReportController@get_others_cost_report_view')->name('other_costs');
        Route::post('/others/cost', 'ReportController@get_others_cost_report')->name('other_costs');
        Route::post('/others/cost/print', 'ReportController@print_others_cost_report')->name('other_costs_print');
        Route::get('/users', 'ReportController@get_all_user_report')->name('users');
        Route::post('/users', 'ReportController@print_all_user_report')->name('users');
        Route::get('/user/loan', 'ReportController@get_user_loan_report_view')->name('user_loan');
        Route::post('/user/loan', 'ReportController@get_user_loan_report')->name('user_loan');
        Route::post('/user/loan/print', 'ReportController@print_user_loan_report')->name('user_loan_print');
        Route::get('/user/sale', 'ReportController@get_user_sale_report_view')->name('user_sale');
        Route::post('/user/sale', 'ReportController@get_user_sale_report')->name('user_sale');
        Route::post('/user/sale/print', 'ReportController@print_user_sale_report')->name('user_sale_print');
        Route::get('/user/deposit', 'ReportController@get_user_deposit_report_view')->name('user_deposit');
        Route::post('/user/deposit', 'ReportController@get_user_deposit_report')->name('user_deposit');
        Route::post('/user/deposit/print', 'ReportController@print_user_deposit_report')->name('user_deposit_print');
        Route::get('/user/individual/transaction', 'ReportController@get_user_individual_transaction_report_view')->name('individual_transaction');
        Route::post('/user/individual/transaction', 'ReportController@get_user_individual_transaction_report')->name('individual_transaction');
        Route::post('/user/individual/transaction/print', 'ReportController@print_user_individual_transaction_report')->name('individual_transaction_print');
    });
});
