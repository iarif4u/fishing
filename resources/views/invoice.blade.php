<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/x-icon" href="{{asset("assets/img/favicon.png")}}"/>
    <title>Mrs Ma Moriam Fishing | WinnerDevs </title>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    {{--<link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600,700&display=swap" rel="stylesheet">--}}
    <link href="{{asset("bootstrap/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset("assets/css/plugins.css")}}" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/dropify/dropify.min.css")}}">
    <link href="{{asset("assets/css/users/account-setting.css")}}" rel="stylesheet" type="text/css"/>
    <!--  END CUSTOM STYLE FILE  -->

    <!-- END PAGE LEVEL CUSTOM STYLES -->
    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link href="{{asset("assets/css/scrollspyNav.css")}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/bootstrap-select/bootstrap-select.min.css")}}">
    <link href="{{asset("assets/solaiman-lipi/font.css")}}" rel="stylesheet" type="text/css">
    <!--  END CUSTOM STYLE FILE  -->
    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link href="{{asset("assets/css/users/user-profile.css")}}" rel="stylesheet" type="text/css"/>
    <!--  END CUSTOM STYLE FILE  -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{asset("plugins/flatpickr/flatpickr.css")}}" rel="stylesheet" type="text/css">
    <link href="{{asset("plugins/noUiSlider/nouislider.min.css")}}" rel="stylesheet" type="text/css">
    <!-- END THEME GLOBAL STYLES -->

    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link href="{{asset("plugins/flatpickr/custom-flatpickr.css")}}" rel="stylesheet" type="text/css">
    <link href="{{asset("plugins/noUiSlider/custom-nouiSlider.css")}}" rel="stylesheet" type="text/css">
    <link href="{{asset("plugins/bootstrap-range-Slider/bootstrap-slider.css")}}" rel="stylesheet" type="text/css">
    <!--  END CUSTOM STYLE FILE  -->
    @yield("style")
    <style>
        body, article, h1, h2, h3, h4, h5, h6, textarea, input, select, .topbar, .main-menu, .breadcrumb, .copyrights-area, form span.required {
            font-family: 'SolaimanLipi', Quicksand, sans-serif !important;
        }
    </style>
</head>

<body class="alt-menu sidebar-noneoverflow">

<!--  BEGIN MAIN CONTAINER  -->
<div class="main-container" id="container">
    <div id="content" class="main-content">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
                <div class="widget-content-area br-4">
                    <div class="skills layout-spacing ">

                        <div class="invoice-container">
                            <div class="invoice-inbox">
                                <div class="invoice-header-section">
                                    <h4 class="inv-number"></h4>
                                    <div class="invoice-action">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-printer action-print" data-toggle="tooltip" data-placement="top" data-original-title="Reply"><polyline points="6 9 6 2 18 2 18 9"></polyline><path d="M6 18H4a2 2 0 0 1-2-2v-5a2 2 0 0 1 2-2h16a2 2 0 0 1 2 2v5a2 2 0 0 1-2 2h-2"></path><rect x="6" y="14" width="12" height="8"></rect></svg>
                                    </div>
                                </div>

                                <div id="ct" class="">

                                    <div class="invoice-00001">
                                        <div class="content-section  animated animatedFadeInUp fadeInUp">

                                            <div class="row inv--head-section">

                                                <div class="col-sm-6 col-12">
                                                    <h3 class="in-heading">চালান</h3>
                                                </div>
                                                <div class="col-sm-6 col-12 align-self-center text-sm-right">
                                                    <div class="company-info">

                                                        <h4 class="inv-brand-name">মেসার্স মা মরিয়ম ফিশিং</h4>
                                                    </div>
                                                </div>

                                            </div>

                                            <div class="row inv--detail-section">

                                                <div class="col-sm-7 align-self-center">
                                                    <p class="inv-to"></p>
                                                </div>
                                                <div class="col-sm-5 align-self-center  text-sm-right order-sm-0 order-1">
                                                    <p class="inv-detail-title">অফিস : ৩০৫/ মা মরিয়ম স্টোর, ফিসারী ঘাট, ইকবাল রোড, কোতয়ালী চট্টগ্রাম। <br>১০৩/১০৪ নংগদি, নতুন মাছ বাজার, মেরিনার্স রোড, চাক্তাই বাকলিয়া, চট্টগ্রাম ।</p>

                                                </div>

                                                <div class="col-sm-7 align-self-center">
                                                    <p class="inv-customer-name">UserName</p>
                                                    <p class="inv-customer-name">Cumpany Name </p>
                                                    <p class="inv-street-addr">Mobile Number</p>

                                                </div>
                                                <div class="col-sm-5 align-self-center  text-sm-right order-2">
                                                    <p class="inv-list-number"><span class="inv-title">চালান নম্বর
 : </span> <span class="inv-number">[invoice number]</span></p>
                                                    <p class="inv-created-date"><span class="inv-title">চালান প্রদানের তারিখ: </span> <span class="inv-date">20 Aug 2019</span></p>

                                                </div>
                                            </div>

                                            <div class="row inv--product-table-section  mt-4">
                                                <div class="col-12">
                                                    <div class="table-responsive">
                                                        <table class="table">
                                                            <thead class="">
                                                            <tr>

                                                                <th scope="col">Items</th>
                                                                <th class="text-right" scope="col">Qty</th>
                                                                <th class="text-right" scope="col">Unit Price</th>
                                                                <th class="text-right" scope="col">Amount</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>

                                                                <td>Electric Shaver</td>
                                                                <td class="text-right">20</td>
                                                                <td class="text-right">$300</td>
                                                                <td class="text-right">$2800</td>
                                                            </tr>
                                                            <tr>

                                                                <td>Earphones</td>
                                                                <td class="text-right">49</td>
                                                                <td class="text-right">$500</td>
                                                                <td class="text-right">$7000</td>
                                                            </tr>
                                                            <tr>

                                                                <td>Wireless Router</td>
                                                                <td class="text-right">30</td>
                                                                <td class="text-right">$500</td>
                                                                <td class="text-right">$3500</td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-sm-5 col-12 order-sm-0 order-1">
                                                    <div class="inv--payment-info">

                                                    </div>
                                                </div>
                                                <div class="col-sm-7 col-12 order-sm-1 order-0">
                                                    <div class="inv--total-amounts text-sm-right">
                                                        <div class="row">
                                                            <div class="col-sm-8 col-7 grand-total-title">
                                                                <h4 class="">সর্বমোট : </h4>
                                                            </div>
                                                            <div class="col-sm-4 col-5 grand-total-amount">
                                                                <h4 class="">14000 টাকা</h4>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="inv--thankYou">
                                <div class="row text-center">
                                    <br><br>
                                    <div class="col-sm-3 col-3">
                                        <h5> হিসাবরক্ষক </h5>
                                    </div>

                                    <div class="col-sm-3 col-3">
                                        <h5> ক্যাশিয়ার</h5>
                                    </div>

                                    <div class="col-sm-3 col-3">
                                        <h5> পরিচালক </h5>
                                    </div>

                                    <div class="col-sm-3 col-3">
                                        <h5> গ্রাহক </h5>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END MAIN CONTAINER -->


<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="{{asset("assets/js/libs/jquery-3.1.1.min.js")}}"></script>
<script src="{{asset("bootstrap/js/popper.min.js")}}"></script>
<script src="{{asset("bootstrap/js/bootstrap.min.js")}}"></script>
<script src="{{asset("plugins/perfect-scrollbar/perfect-scrollbar.min.js")}}"></script>
<script src="{{asset("assets/js/app.js")}}"></script>
<script>
    $(document).ready(function () {
        App.init();
    });
    function printData(divId)
    {
        var divToPrint=document.getElementById(divId);
        newWin= window.open("");
        newWin.document.write(divToPrint.outerHTML);
        newWin.print();
        newWin.close();
    }
</script>

</body>
</html>
