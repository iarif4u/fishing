@extends('layouts.master')
@section('style')
    <link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/flatpickr/flatpickr.css')}}">
    <style>
        .select2-container {
            margin-bottom: 0 !important;
        }
    </style>
@endsection
@section('heading')
    <h3>বিক্রয় হিসাব</h3>
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{route('home')}}">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                     class="feather feather-home">
                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                </svg>
            </a>
        </li>
        <li class="breadcrumb-item"><a href="javascript:void(0);"> বিক্রয় </a></li>
        <li class="breadcrumb-item active" aria-current="page"><span>নতুন বিক্রয় </span></li>
    </ol>
@endsection
@section('content')
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
            <div class="widget-content-area br-4">
                <form method="post" action="{{route('sale.edit',$sale->id)}}">
                    @csrf
                    <div class="form-row mb-4">
                        <div class="form-group col-md-6 select2-customer mb-0">
                            <label for="customer_id">ব্যক্তির নাম</label>
                            <select name="customer_id" id="customer_id" class="form-control">
                                <option selected value="{{$sale->customer->id}}">{{$sale->customer->name}} ({{$sale->customer->phone}})</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6 mb-0">
                            <label for="date">তারিখ</label>
                            <input id="date" name="date" class="form-control flatpickr flatpickr-input active" type="text" placeholder="{{date('Y-m-d')}}" value="{{$sale->date}}">
                        </div>
                    </div>
                    <div class="form-row mb-4">
                        <div class="form-group col-md-3">
                            <label for="sell">মোট বিক্রি</label>
                            <input value="{{$sale->sell}}" name="sell" type="number" class="calculate-key form-control" id="sell" placeholder="৬০,০০০">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="commission">বিক্রির কমিশন</label>
                            <input value="{{$sale->commission}}" type="text" readonly class="for-paka-chotha form-control" name="commission" id="commission">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="installment">আমানত/ কিস্তি</label>
                            <input value="{{$installment->taka}}" type="number" class="form-control for-paka-chotha calculate-key" id="installment" name="installment" placeholder="১০,০০০">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="other_spend">অন্যান্য খরচ</label>
                            <input type="number" class="form-control for-paka-chotha calculate-key" id="other_spend" name="other_spend" value="{{$sale->other_spend}}" placeholder="১০,০০০">
                        </div>
                    </div>
                    <!--New Added-->
                    <div class="form-row mb-4other_spend">

                        <div class="form-group col-md-3">
                            <label for="ghat">ঠেলা/ঘাট</label>
                            <input value="{{$sale->ghat}}" name="ghat" type="number" class="form-control for-paka-chotha calculate-key" id="ghat" placeholder="৬০,০০০">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="tax">খাজনা</label>
                            <input value="{{$sale->tax}}" type="text" class="form-control for-paka-chotha calculate-key" name="tax" id="tax" placeholder="৫০,০০০">
                        </div>

                        <div class="form-group col-md-3">
                            <label for="association">বিহিন্দি জাল সমিতি</label>
                            <input value="{{$sale->association}}" type="number" class="form-control for-paka-chotha calculate-key" id="association" name="association" placeholder="১০,০০০">
                        </div>
                        <div class="form-group col-md-3">
                            <label for="mosque">মসজিদ</label>
                            <input value="{{$sale->mosque}}" type="number" class="form-control for-paka-chotha calculate-key" id="mosque" name="mosque" placeholder="১০,০০০">
                        </div>
                    </div>
                    <!--New Added End-->
                    <div class="form-row mb-4">
                        <div class="form-group col-md-6">
                            <label for="tea_spend">চা-নাস্তা খরচ</label>
                            <input value="{{$sale->tea_spend}}" type="number" class="form-control" id="tea_spend" name="tea_spend" placeholder="১০,০০০">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="note">নোট</label>
                            <input value="{{$sale->note}}" type="text" class="form-control" id="note" name="note" placeholder="নোট">
                        </div>
                    </div>
                    <div class="form-row mb-4">
                        <div class="form-group col-md-3">
                            <label for="paka_chotha">পাকা চৌঠা</label>
                            <input type="text" readonly class="form-control" name="paka_chotha" id="paka_chotha" placeholder="৫০,০০০">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">তথ্য যুক্ত করুন</button>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script src="{{asset("plugins/flatpickr/flatpickr.js")}}"></script>
    <script>
        var f1 = flatpickr(document.getElementById('date'));
        jQuery.extend({
            percentage: function(a, b) {
                return Math.round((a / b) * 100);
            }
        });
        $("#customer_id").select2({
            placeholder: "Select Customer",
            ajax: {
                url: '{{route('users.search')}}',
                dataType: 'json',
                data: function (params) {
                    // Query parameters will be ?search=[term]&type=public
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: txt.name}
                        })
                    };
                }
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        });

        $('.calculate-key').on('keypress keyup blur change', function (e) {
            var sell = parseInt($("#sell").val());
            var sum = 0;
            $('.for-paka-chotha').each(function(){
                if (Math.ceil(this.value)>0){
                    sum += parseFloat(this.value);
                }
            });
            var paka_chotha = sell-sum;
            $("#paka_chotha").val(parseFloat(paka_chotha));
        });
        $('#sell').on('keypress keyup blur change', function (e) {
            var sell = $(this).val();
            var commission = '{{env('COMMISION')}}';
            const calculation = sell * (commission / 100);
            $("#commission").val(calculation);
        });
    </script>
@endsection
