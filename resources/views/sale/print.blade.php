@extends('layouts.invoice')

@section('customer_section')
    <p class="inv-customer-name">{{$sale->customer->name}}</p>
    <p class="inv-customer-name">{{$sale->customer->company}}</p>
    <p class="inv-street-addr">{{$sale->customer->address}}</p>
@endsection
@section("invoice_head")
    ভাউচার
@endsection
@section('invoice_no')
    {{banglaNumber($sale->id)}}{{banglaNumber($sale->customer->id)}}{{banglaNumber(date('md',strtotime($sale->created_at)))}}
@endsection

@section('invoice_date')
    {{banglaNumber($sale->date)}}
@endsection

@section('content')
    <div class="row">
        <div style="border-right: 2px solid gray;margin-bottom: 28px;" class="col-md-6">
            <h4>জমা</h4>
            <table class="table">
                <thead class="">
                <tr>

                    <th class="text-center" scope="col">বিবরণ</th>
                    <th class="text-right" scope="col">টাকার পরিমান</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="text-center">অদ্দ মাছ বিক্রির পরিমাণ </td>
                    <td class="text-right">{{en2bnNumber($sale->sell)}}</td>
                </tr>
                <tr>
                    <td class="text-center">মোট খরচ বাদ </td>
                    <td class="text-right">{{en2bnNumber($total_spend)}}</td>
                </tr>
                <tr>
                    <td class="text-center">পাকা চৌঠা</td>
                    <td class="text-right">{{en2bnNumber($sale->sell-$total_spend)}}</td>
                </tr>
                </tbody>

            </table>
        </div>
        <div class="col-md-6">
            <h4>খরচ</h4>
            <table class="table">
                <thead class="">
                <tr>
                    <th class="text-center" scope="col">বিবরণ</th>
                    <th class="text-right" scope="col">টাকার পরিমান</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td class="text-center">ঠেলা/ঘাট</td>
                    <td class="text-right">{{en2bnNumber($sale->ghat)}}</td>
                </tr>
                <tr>
                    <td class="text-center">কমিশন</td>
                    <td class="text-right">{{en2bnNumber($sale->commission)}}</td>
                </tr>
                <tr>
                    <td class="text-center">খাজনা</td>
                    <td class="text-right">{{en2bnNumber($sale->tax)}}</td>
                </tr>
                <tr>
                    <td class="text-center">বিহিন্দি জাল সমিতি</td>
                    <td class="text-right">{{en2bnNumber($sale->association)}}</td>
                </tr>
                <tr>
                    <td class="text-center">মসজিদ</td>
                    <td class="text-right">{{en2bnNumber($sale->mosque)}}</td>
                </tr>
                <tr>
                    <td class="text-center">অন্যান্য খরচ</td>
                    <td class="text-right">{{en2bnNumber($sale->other_spend)}}</td>
                </tr>
                </tbody>
                <tfoot>
                <tr>
                    <td class="text-center">মোট</td>
                    <td class="text-right">{{en2bnNumber($total_spend)}}</td>
                </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <br>
@endsection
