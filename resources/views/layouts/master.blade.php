<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/x-icon" href="{{asset("assets/img/favicon.png")}}"/>
    <title>Mrs Ma Moriam Fishing | WinnerDevs </title>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    {{--<link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600,700&display=swap" rel="stylesheet">--}}
    <link href="{{asset("bootstrap/css/bootstrap.min.css")}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset("assets/css/plugins.css")}}" rel="stylesheet" type="text/css"/>
    <!-- END GLOBAL MANDATORY STYLES -->
    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/dropify/dropify.min.css")}}">
    <link href="{{asset("assets/css/users/account-setting.css")}}" rel="stylesheet" type="text/css"/>
    <!--  END CUSTOM STYLE FILE  -->

    <!-- END PAGE LEVEL CUSTOM STYLES -->
    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link href="{{asset("assets/css/scrollspyNav.css")}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/bootstrap-select/bootstrap-select.min.css")}}">
    <link href="{{asset("assets/solaiman-lipi/font.css")}}" rel="stylesheet" type="text/css">
    <!--  END CUSTOM STYLE FILE  -->
    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link href="{{asset("assets/css/users/user-profile.css")}}" rel="stylesheet" type="text/css"/>
    <!--  END CUSTOM STYLE FILE  -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="{{asset("plugins/flatpickr/flatpickr.css")}}" rel="stylesheet" type="text/css">
    <link href="{{asset("plugins/noUiSlider/nouislider.min.css")}}" rel="stylesheet" type="text/css">
    <!-- END THEME GLOBAL STYLES -->

    <!--  BEGIN CUSTOM STYLE FILE  -->
    <link href="{{asset("plugins/flatpickr/custom-flatpickr.css")}}" rel="stylesheet" type="text/css">
    <link href="{{asset("plugins/noUiSlider/custom-nouiSlider.css")}}" rel="stylesheet" type="text/css">
    <link href="{{asset("plugins/bootstrap-range-Slider/bootstrap-slider.css")}}" rel="stylesheet" type="text/css">
    <!--  END CUSTOM STYLE FILE  -->
    @yield("style")
    <style>
        body, article, h1, h2, h3, h4, h5, h6, textarea, input, select, .topbar, .main-menu, .breadcrumb, .copyrights-area, form span.required {
            font-family: 'SolaimanLipi', Quicksand, sans-serif !important;
        }
    </style>
    <style type="text/css" media="print">
        @media print
        {
            @page {
                margin-top: 0;
                margin-bottom: 0;
            }
            body  {
                padding-top: 72px;
                padding-bottom: 72px ;
            }

        }
        @print {
            @page :footer {
                display: none
            }

            @page :header {
                display: none
            }
        }
    </style>
</head>

<body class="alt-menu sidebar-noneoverflow">
@include("layouts.include.navbar")
<!--  BEGIN MAIN CONTAINER  -->
<div class="main-container" id="container">

    <div class="overlay"></div>
    <div class="search-overlay"></div>
    <!--  BEGIN TOPBAR  -->
    <div class="topbar-nav header navbar" role="banner">
        <nav id="topbar">
            <ul class="navbar-nav theme-brand flex-row  text-center">
                <li class="nav-item theme-logo">
                    <a href="{{route("home")}}">
                        <img src="{{asset("assets/img/favicon.png")}}" class="navbar-logo" alt="logo">
                    </a>
                </li>
                <li class="nav-item theme-text">
                    <a href="{{route("home")}}" class="nav-link"> মেসার্স মা মরিয়ম ফিশিং </a>
                </li>
            </ul>
            <ul class="list-unstyled menu-categories" id="topAccordion">
                <li class="menu single-menu {{activeMenu("home")}}">
                    <a href="{{route("home")}}">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-home">
                                <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                                <polyline points="9 22 9 12 15 12 15 22"></polyline>
                            </svg>
                            <span>ড্যাশবোর্ড</span>
                        </div>
                    </a>
                </li>
                <li class="menu single-menu {{areActiveRoutes(["users.add","users.list",'users.edit',"users.profile"])}}">
                    <a href="#Person" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle autodroprown">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-box">
                                <path
                                    d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path>
                                <polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline>
                                <line x1="12" y1="22.08" x2="12" y2="12"></line>
                            </svg>
                            <span>ব্যক্তি</span>
                        </div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-chevron-down">
                            <polyline points="6 9 12 15 18 9"></polyline>
                        </svg>
                    </a>
                    <ul class="collapse submenu list-unstyled" id="Person" data-parent="#topAccordion">
                        <li>
                            <a href="{{route("users.add")}}"> নতুন ব্যক্তি তৈরি </a>
                        </li>
                        <li>
                            <a href="{{route("users.list")}}"> ব্যক্তির লিস্ট </a>
                        </li>
                    </ul>
                </li>

                <li class="menu single-menu {{areActiveRoutes(["loan.add","loan.list"])}}">
                    <a href="#Dadan" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle autodroprown">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-box">
                                <path
                                    d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path>
                                <polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline>
                                <line x1="12" y1="22.08" x2="12" y2="12"></line>
                            </svg>
                            <span>দাদন </span>
                        </div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-chevron-down">
                            <polyline points="6 9 12 15 18 9"></polyline>
                        </svg>
                    </a>
                    <ul class="collapse submenu list-unstyled" id="Dadan" data-parent="#topAccordion">
                        <li>
                            <a href="{{route("loan.add")}}">দাদন যুক্ত করুন </a>
                        </li>
                        <li>
                            <a href="{{route("loan.list")}}"> দাদনের তালিকা </a>
                        </li>

                    </ul>
                </li>

                <li class="menu single-menu {{areActiveRoutes(["installment.add","installment.edit","installment.list"])}}">
                    <a href="#Installment" data-toggle="collapse" aria-expanded="false"
                       class="dropdown-toggle autodroprown">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-box">
                                <path
                                    d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path>
                                <polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline>
                                <line x1="12" y1="22.08" x2="12" y2="12"></line>
                            </svg>
                            <span>আমানত/ কিস্তি </span>
                        </div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-chevron-down">
                            <polyline points="6 9 12 15 18 9"></polyline>
                        </svg>
                    </a>
                    <ul class="collapse submenu list-unstyled" id="Installment" data-parent="#topAccordion">
                        <li>
                            <a href="{{route("installment.add")}}">নতুন কিস্তি গ্রহণ </a>
                        </li>
                        <li>
                            <a href="{{route("installment.list")}}"> কিস্তি গ্রহণ তালিকা </a>
                        </li>
                    </ul>
                </li>

                <li class="menu single-menu {{areActiveRoutes(["sale.add","sale.edit","sale.list"])}}">
                    <a href="#Sale" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle autodroprown">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-box">
                                <path
                                    d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path>
                                <polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline>
                                <line x1="12" y1="22.08" x2="12" y2="12"></line>
                            </svg>
                            <span>বিক্রয় </span>
                        </div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-chevron-down">
                            <polyline points="6 9 12 15 18 9"></polyline>
                        </svg>
                    </a>
                    <ul class="collapse submenu list-unstyled" id="Sale" data-parent="#topAccordion">
                        <li>
                            <a href="{{route('sale.add')}}">নতুন বিক্রয় </a>
                        </li>
                        <li>
                            <a href="{{route('sale.list')}}">বিক্রয় তালিকা</a>
                        </li>
                    </ul>
                </li>

                <li class="menu single-menu {{areActiveRoutes(["deposit.add","deposit.list","deposit.edit"])}}">
                    <a href="#Deposit" data-toggle="collapse" aria-expanded="false"
                       class="dropdown-toggle autodroprown">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-box">
                                <path
                                    d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path>
                                <polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline>
                                <line x1="12" y1="22.08" x2="12" y2="12"></line>
                            </svg>
                            <span>জমার হিসাব </span>
                        </div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-chevron-down">
                            <polyline points="6 9 12 15 18 9"></polyline>
                        </svg>
                    </a>
                    <ul class="collapse submenu list-unstyled" id="Deposit" data-parent="#topAccordion">
                        <li>
                            <a href="{{route('deposit.add')}}">নতুন জমা প্রদান </a>
                        </li>
                        <li>
                            <a href="{{route('deposit.list')}}"> জমা তালিকা </a>
                        </li>
                    </ul>
                </li>
                <li class="menu single-menu {{areActiveRoutes(["cost"])}}">
                    <a href="{{route('cost')}}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-box">
                            <path
                                d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z"></path>
                            <polyline points="3.27 6.96 12 12.01 20.73 6.96"></polyline>
                            <line x1="12" y1="22.08" x2="12" y2="12"></line>
                        </svg>
                        <span style="color:#fff;padding-top: 5px; padding-left: 10px;">অন্যান্য খরচ</span>
                    </a>
                </li>
                <li class="menu single-menu {{areActiveRoutes(['reports.other_costs',"reports.users","reports.user_loan","reports.user_sale","reports.user_deposit","reports.individual_transaction"])}}">
                    <a href="#Report" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle autodroprown">
                        <div class="">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                 stroke-linejoin="round" class="feather feather-plus-circle">
                                <circle cx="12" cy="12" r="10"></circle>
                                <line x1="12" y1="8" x2="12" y2="16"></line>
                                <line x1="8" y1="12" x2="16" y2="12"></line>
                            </svg>
                            <span>রিপোর্ট </span>
                        </div>
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                             stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                             class="feather feather-chevron-down">
                            <polyline points="6 9 12 15 18 9"></polyline>
                        </svg>
                    </a>
                    <ul class="collapse submenu list-unstyled" id="Report" data-parent="#topAccordion">
                        <li>
                            <a href="{{route('reports.users')}}"> সকল ব্যক্তিদের তালিকা </a>
                        </li>
                        <li>
                            <a href="{{route('reports.user_loan')}}"> ব্যক্তির দাদন রিপোর্ট </a>
                        </li>
                        <li>
                            <a href="{{route('reports.user_sale')}}"> ব্যক্তির বিক্রয় রিপোর্ট </a>
                        </li>
                        <li>
                            <a href="{{route('reports.user_deposit')}}"> ব্যক্তি জমার রিপোর্ট </a>
                        </li>
                        <li>
                            <a href="{{route('reports.individual_transaction')}}"> ব্যক্তির লেনদেন রিপোর্ট </a>
                        </li>
                        <li>
                            <a href="{{route('reports.other_costs')}}">অন্যান্য খরচ রিপোর্ট </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </nav>
    </div>
    <!--  END TOPBAR  -->
    <div id="content" class="main-content">
        <div class="layout-px-spacing">
            <div class="page-header">
                <div class="page-title">
                    @yield("heading")
                </div>
                <nav class="breadcrumb-one" aria-label="breadcrumb">
                    @yield('breadcrumb')
                </nav>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <span class="each-error">{{ $error }} </span><br/>
                    @endforeach
                </div>
            @endif
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
            @yield("content")
        </div>
    </div>
    <div class="footer-wrapper">
        <div class="footer-section f-section-1">
            <p class="">Copyright © 2020 <a target="_blank" href="https://designreset.com">Mrs Maa moriam fishing</a>,
                All rights reserved.</p>
        </div>
        <div class="footer-section f-section-2">
            <p class="">Coded with
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                     class="feather feather-heart">
                    <path
                        d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path>
                </svg>
                WinnerDevs
            </p>
        </div>
    </div>

</div>
<!-- END MAIN CONTAINER -->


<!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
<script src="{{asset("assets/js/libs/jquery-3.1.1.min.js")}}"></script>
<script src="{{asset("bootstrap/js/popper.min.js")}}"></script>
<script src="{{asset("bootstrap/js/bootstrap.min.js")}}"></script>
<script src="{{asset("plugins/perfect-scrollbar/perfect-scrollbar.min.js")}}"></script>
<script src="{{asset("assets/js/app.js")}}"></script>
<script>
    $(document).ready(function () {
        App.init();
    });
    function printData(divId)
    {
        var curURL = window.location.href;
        var divToPrint=document.getElementById(divId);
        history.replaceState(history.state, '', '/');
        history.replaceState(history.state, '', curURL);
        newWin= window.open("");
        newWin.document.write(divToPrint.outerHTML);
        newWin.print();
        newWin.close();
    }
</script>
@yield("script")
<!-- END GLOBAL MANDATORY SCRIPTS -->
<!--  BEGIN CUSTOM SCRIPTS FILE  -->

</body>
</html>
