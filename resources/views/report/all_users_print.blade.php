@extends('layouts.report')
@section('content')
<table id="print" class="table table-bordered mb-4" style="width:100%">
    <thead>
    <tr>
        <th class="text-left">ব্যক্তির নাম</th>

        <th>প্রতিষ্ঠানের</th>
        <th>বাবার নাম</th>
        <th>ফোন</th>
        <th>বর্তমান ঠিকানা</th>
        <th>দাদন বাকি</th>
        <th>জমা বাকি</th>
    </tr>
    </thead>
    <tbody>
    @if($customers->count()>0)
        @foreach($customers as $customer)
            <tr>
                <td>{{$customer->name}}</td>
                <td>{{$customer->company}}</td>
                <td>{{$customer->father}}</td>
                <td>{{banglaNumber($customer->phone)}}</td>
                <td>{{$customer->address}}</td>
                <td>{{en2bnNumber(get_due_loan($customer->id))}}</td>
                <td>{{en2bnNumber(get_due_payment($customer->id))}}</td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>
@endsection
