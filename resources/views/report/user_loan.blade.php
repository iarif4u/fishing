@extends("layouts.master")
@section('style')
    <link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/flatpickr/flatpickr.css')}}">
    <style>
        .select2-container {
            margin-bottom: 0 !important;
        }
    </style>
@endsection
@section('heading')
    <h3>ব্যক্তির দাদন রিপোর্ট</h3>
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0);">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                     class="feather feather-home">
                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                </svg>
            </a></li>
        <li class="breadcrumb-item"><a href="#">রিপোর্ট</a></li>
        <li class="breadcrumb-item active" aria-current="page"><span>দাদন রিপোর্ট</span></li>
    </ol>
@endsection
@section('content')
    <div class="row layout-top-spacing" id="cancel-row">

        <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
            <div class="widget-content widget-content-area br-6">
                <form method="post" action="{{route('reports.user_loan')}}">
                    @csrf
                    <div class="row">
                        <div class="col-lg-3">
                            <label for="customer_id">ব্যক্তির নাম</label>
                            <select name="customer_id" id="customer_id" class="form-control">
                                @if(isset($customer))
                                    <option value="{{$customer->id}}">{{$customer->name}} ({{$customer->phone}})
                                    </option>
                                @else
                                    <option selected>ব্যক্তির নাম নির্বাচন করুন</option>
                                @endif
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <p><b>শুরু তারিখ</b></p>
                            <div class="form-group">
                                <input id="start_date"
                                       value="{{request()->input('start_date',\Carbon\Carbon::now()->subMonth()->format('Y-m-d'))}}"
                                       name="start_date"
                                       class="form-control flatpickr flatpickr-input active" type="text"
                                       placeholder="শুরু তারিখ..">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <p><b>শেষের তারিখ</b></p>
                            <input id="end_date" value="{{request()->input('end_date',date('Y-m-d'))}}" name="end_date"
                                   class="form-control flatpickr flatpickr-input active" type="text"
                                   placeholder="শেষের তারিখ..">
                        </div>
                        <div class="col-lg-3 text-center">
                            <p><b> তথ্যের জন্য নিচের বাটনে ক্লিক করুন</b></p>
                            <button type="submit" class="btn btn-success btn-lg btn-block mb-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-search">
                                    <circle cx="11" cy="11" r="8"></circle>
                                    <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                                </svg>
                                অনুসন্ধান
                            </button>
                        </div>
                    </div>
                </form>
                <div class="table-responsive mb-4 mt-4">
                    <table id="print" class="table table-bordered mb-4" style="width:100%">
                        <thead>
                        <tr>
                            <th>ব্যক্তির নাম</th>
                            <th>লেনদেনের তারিখ</th>
                            <th class="text-right">দাদন পরিমান</th>
                            <th>নোট</th>
                        </tr>
                        </thead>
                        @if(isset($loans))
                            <tbody>
                            @foreach($loans as $loan)
                                <tr>
                                    <td>{{$loan->customer->name}}</td>
                                    <td>{{banglaNumber($loan->date)}}</td>
                                    <td class="text-right">{{en2bnNumber($loan->taka)}}</td>
                                    <td>{{$loan->note}}</td>
                                </tr>
                            @endforeach

                            </tbody>
                            <tfoot>
                            <tr>
                                <th></th>
                                <th></th>
                                <th class="text-right">মোট :{{ en2bnNumber($loans->sum('taka')) }}</th>
                                <th></th>
                            </tr>
                            </tfoot>
                        @endif
                    </table>
                </div>
                <div class="row">
                    <div class="col-12">
                        @if(isset($customer))
                            <form action="{{route('reports.user_loan_print')}}" method="post">
                                @csrf
                                <input type="hidden" value="{{$customer->id}}" name="customer_id">
                                <input type="hidden" value="{{request()->input('end_date')}}" name="end_date">
                                <input type="hidden" value="{{request()->input('start_date')}}" name="start_date">
                                <button type="submit" class="btn btn-primary mb-2">তথ্যগুলো প্রিন্ট
                                    করুন
                                </button>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script src="{{asset("plugins/flatpickr/flatpickr.js")}}"></script>
    <script>
        var f1 = flatpickr(document.getElementById('start_date'));
        var f2 = flatpickr(document.getElementById('end_date'));
        $("#customer_id").select2({
            placeholder: "Select Customer",
            ajax: {
                url: '{{route('users.search')}}',
                dataType: 'json',
                data: function (params) {
                    // Query parameters will be ?search=[term]&type=public
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: txt.name}
                        })
                    };
                }
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        });
    </script>
@endsection
