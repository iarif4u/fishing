@extends("layouts.master")
@section('heading')
    <h3>সকল ব্যক্তিদের তালিকা</h3>
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0);">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                     class="feather feather-home">
                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                </svg>
            </a></li>
        <li class="breadcrumb-item"><a href="#">রিপোর্ট</a></li>
        <li class="breadcrumb-item active" aria-current="page"><span>সকল ব্যক্তিদের তালিকা</span></li>
    </ol>
@endsection
@section('content')
    <div class="row layout-top-spacing" id="cancel-row">
        <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
            <div class="widget-content widget-content-area br-6">
                <div class="table-responsive mb-4 mt-4">
                    <table id="print" class="table table-bordered mb-4" style="width:100%">
                        <thead>
                        <tr>
                            <th>ব্যক্তির নাম</th>
                            <th>প্রতিষ্ঠানের</th>
                            <th>বাবার নাম</th>
                            <th>ফোন</th>
                            <th>বর্তমান ঠিকানা</th>
                            <th>দাদন বাকি</th>
                            <th>জমা বাকি</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($customers->count()>0)
                            @foreach($customers as $customer)
                                <tr>
                                    <td>{{$customer->name}}</td>
                                    <td>{{$customer->company}}</td>
                                    <td>{{$customer->father}}</td>
                                    <td>{{banglaNumber($customer->phone)}}</td>
                                    <td>{{$customer->address}}</td>
                                    <td>{{en2bnNumber(get_due_loan($customer->id))}}</td>
                                    <td>{{en2bnNumber(get_due_payment($customer->id))}}</td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
                <div class="row">
                    <div class="col-12">
                        <form method="post" action="{{route('reports.users')}}">
                            @csrf
                            <button type="submit" class="btn btn-primary mb-2">তথ্যগুলো প্রিন্ট করুন</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
