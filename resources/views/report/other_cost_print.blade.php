@extends('layouts.report')
@section('content')
<table id="print" class="table table-bordered mb-4" style="width:100%">
    <thead>
    <tr>
        <th>লেনদেনের তারিখ</th>
        <th class="text-right">টুকরি ভাড়া টাকার পরিমান</th>
        <th class="text-right">কমিশন ফেরত</th>
    </tr>
    </thead>
    <tbody>
    @if(isset($costs))
        @if($costs->count()>0)
            @foreach($costs as $cost)
                <tr>
                    <td>{{banglaNumber($cost->date)}}</td>
                    <td class="text-right">{{en2bnNumber($cost->basket)}}</td>
                    <td class="text-right">{{en2bnNumber($cost->commission)}}</td>
                </tr>
            @endforeach
        @endif
    @endif
    </tbody>
    <tfoot>
    @if(isset($costs))
        <tr>
            <th></th>
            <th class="text-right">মোট : {{en2bnNumber($cost->sum('basket'))}}</th>
            <th class="text-right">মোট : {{en2bnNumber($cost->sum('commission'))}}</th>
        </tr>
    @endif
    </tfoot>
</table>
@endsection
