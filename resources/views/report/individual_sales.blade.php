@extends("layouts.master")
@section('style')
    <link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/flatpickr/flatpickr.css')}}">
    <style>
        .select2-container {
            margin-bottom: 0 !important;
        }
    </style>
@endsection
@section('heading')
    <h3>ব্যক্তির বিক্রয় রিপোর্ট</h3>
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0);">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                     class="feather feather-home">
                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                </svg>
            </a></li>
        <li class="breadcrumb-item"><a href="#">রিপোর্ট</a></li>
        <li class="breadcrumb-item active" aria-current="page"><span>বিক্রয় রিপোর্ট</span></li>
    </ol>
@endsection
@section('content')
    <div class="row layout-top-spacing" id="cancel-row">

        <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
            <div class="widget-content widget-content-area br-6">
                <form method="post" action="{{route('reports.user_sale')}}">
                    @csrf
                    <div class="row">
                        <div class="col-lg-3">
                            <label for="customer_id">ব্যক্তির নাম</label>
                            <select name="customer_id" id="customer_id" class="form-control">
                                @if(isset($customer))
                                    <option value="{{$customer->id}}">{{$customer->name}} ({{$customer->phone}})</option>
                                @else
                                    <option selected>ব্যক্তির নাম নির্বাচন করুন</option>
                                @endif
                            </select>
                        </div>
                        <div class="col-lg-3">
                            <p><b>শুরু তারিখ</b></p>
                            <div class="form-group">
                                <input id="start_date"
                                       @if(isset($sales))
                                        value="{{request()->input('start_date',\Carbon\Carbon::now('asia/dhaka')->subMonth()->format('Y-m-d'))}}"
                                       @else
                                        value="{{\Carbon\Carbon::now('asia/dhaka')->subMonth()->format('Y-m-d')}}"
                                       @endif
                                       name="start_date"
                                       class="form-control flatpickr flatpickr-input active" type="text"
                                       placeholder="শুরু তারিখ..">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <p><b>শেষের তারিখ</b></p>
                            <input id="end_date"
                                   @if(isset($sales))
                                   value="{{request()->input('end_date',date('Y-m-d'))}}"
                                   @else
                                   value="{{date('Y-m-d')}}"
                                   @endif

                                   name="end_date"
                                   class="form-control flatpickr flatpickr-input active" type="text"
                                   placeholder="শেষের তারিখ..">
                        </div>
                        <div class="col-lg-3 text-center">
                            <p><b> তথ্যের জন্য নিচের বাটনে ক্লিক করুন</b></p>
                            <button type="submit" class="btn btn-success btn-lg btn-block mb-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-search">
                                    <circle cx="11" cy="11" r="8"></circle>
                                    <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                                </svg>
                                অনুসন্ধান
                            </button>
                        </div>
                    </div>
                </form>
                <div class="table-responsive mb-4 mt-4">
                    <table id="print" class="table table-bordered mb-4" style="width:100%">
                        <thead>
                        <tr>
                            <th>ব্যক্তির নাম</th>
                            <th>লেনদেনের তারিখ</th>
                            <th class="text-right">বিক্রয়</th>
                            <th class="text-right">কমিশন</th>
                            <th class="text-right">ঠেলা/ঘাট</th>
                            <th class="text-right">খাজনা</th>
                            <th class="text-right">বিহিন্দি জাল সমিতি</th>
                            <th class="text-right">মসজিদ</th>
                            <th class="text-right">অন্যান্য খরচ</th>
                            <th class="text-right">চা-নাস্তা</th>
                            <th class="text-right">আমানত</th>
                            <th class="text-right">পাকা চৌঠা</th>
                        </tr>
                        </thead>
                        @if(isset($results))
                            @if(count($results)>0)
                                @php
                                    $total_paka_chotha= 0;
                                @endphp
                                <tbody>
                                @foreach($results as $result)

                                    <tr>
                                        <td>
                                            <b>{{$result['company']}}</b>
                                            <br>
                                            {{$result['customer']}}
                                        </td>
                                        <td>{{banglaNumber(date('d-m-Y',strtotime($result['date'])))}}</td>
                                        <td class="text-right">
                                            {{en2bnNumber(get_exists_value('sell',$result))}}
                                        </td>
                                        <td class="text-right">
                                            {{en2bnNumber(get_exists_value('commission',$result))}}
                                        </td>
                                        <td class="text-right">
                                            {{en2bnNumber(get_exists_value('ghat',$result))}}
                                        </td>
                                        <td class="text-right">
                                            {{en2bnNumber(get_exists_value('tax',$result))}}
                                        </td>
                                        <td class="text-right">
                                            {{en2bnNumber(get_exists_value('association',$result))}}
                                        </td>
                                        <td class="text-right">
                                            {{en2bnNumber(get_exists_value('mosque',$result))}}
                                        </td>
                                        <td class="text-right">
                                            {{en2bnNumber(get_exists_value('other_spend',$result))}}
                                        </td>
                                        <td class="text-right">
                                            {{en2bnNumber(get_exists_value('tea_spend',$result))}}
                                        </td>
                                        <td class="text-right">
                                            {{en2bnNumber(get_exists_value('taka',$result))}}
                                        </td>
                                        <td class="text-right">
                                            @php
                                                $paka_chotha = $result['sell']-sum_exists_value(['commission','taka','other_spend','mosque','association','tax','ghat'],$result);
                                                $total_paka_chotha = $total_paka_chotha+$paka_chotha;
                                            @endphp
                                            {{en2bnNumber($paka_chotha)}}
                                        </td>

                                    </tr>
                                @endforeach
                                </tbody>
                            @endif
                            <tfoot>
                             <tr>
                                 <th></th>
                                 <th></th>
                                 <th class="text-right">মোট : {{en2bnNumber($sales->sum('sell'))}}</th>
                                 <th class="text-right">মোট : {{en2bnNumber($sales->sum('commission'))}}</th>
                                 <th class="text-right">মোট : {{en2bnNumber($sales->sum('ghat'))}}</th>
                                 <th class="text-right">মোট : {{en2bnNumber($sales->sum('tax'))}}</th>
                                 <th class="text-right">মোট : {{en2bnNumber($sales->sum('association'))}}</th>
                                 <th class="text-right">মোট : {{en2bnNumber($sales->sum('mosque'))}}</th>
                                 <th class="text-right">মোট : {{en2bnNumber($sales->sum('other_spend'))}}</th>
                                 <th class="text-right">মোট : {{en2bnNumber($sales->sum('tea_spend'))}}</th>
                                 <th class="text-right">মোট : {{en2bnNumber($installments->sum('taka'))}}</th>
                                 <th class="text-right">মোট : {{en2bnNumber($total_paka_chotha)}}</th>
                             </tr>
                            </tfoot>
                        @endif
                    </table>
                </div>
                <div class="row">
                    <div class="col-12">
                        @if(isset($customer))
                            <form action="{{route('reports.user_sale_print')}}" method="post">
                                @csrf
                                <input type="hidden" value="{{$customer->id}}" name="customer_id">
                                <input type="hidden" value="{{request()->input('end_date')}}" name="end_date">
                                <input type="hidden" value="{{request()->input('start_date')}}" name="start_date">
                                <button type="submit" class="btn btn-primary mb-2">তথ্যগুলো প্রিন্ট
                                    করুন
                                </button>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script src="{{asset("plugins/flatpickr/flatpickr.js")}}"></script>
    <script>
        var f1 = flatpickr(document.getElementById('start_date'));
        var f2 = flatpickr(document.getElementById('end_date'));
        $("#customer_id").select2({
            placeholder: "Select Customer",
            ajax: {
                url: '{{route('users.search')}}',
                dataType: 'json',
                data: function (params) {
                    // Query parameters will be ?search=[term]&type=public
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: txt.name}
                        })
                    };
                }
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        });
    </script>
@endsection
