@extends("layouts.master")
@section('style')
    <link rel="stylesheet" href="{{asset('plugins/flatpickr/flatpickr.css')}}">
    <style>
        .select2-container {
            margin-bottom: 0 !important;
        }
    </style>
@endsection
@section('heading')
    <h3>অন্যান্য খরচের রিপোর্ট</h3>
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0);">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                     class="feather feather-home">
                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                </svg>
            </a></li>
        <li class="breadcrumb-item"><a href="#">রিপোর্ট</a></li>
        <li class="breadcrumb-item active" aria-current="page"><span>অন্যান্য খরচের রিপোর্ট</span></li>
    </ol>
@endsection
@section('content')
    <div class="row layout-top-spacing" id="cancel-row">

        <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
            <div class="widget-content widget-content-area br-6">
                <form method="post" action="{{route('reports.other_costs')}}">
                    @csrf
                    <div class="row">

                        <div class="col-lg-4">
                            <p><b>শুরু তারিখ</b></p>
                            <div class="form-group">
                                <input id="start_date"
                                       value="{{request()->input('start_date',\Carbon\Carbon::now()->subMonth()->format('Y-m-d'))}}"
                                       name="start_date"
                                       class="form-control flatpickr flatpickr-input active" type="text"
                                       placeholder="শুরু তারিখ..">
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <p><b>শেষের তারিখ</b></p>
                            <input id="end_date" value="{{request()->input('end_date',date('Y-m-d'))}}" name="end_date"
                                   class="form-control flatpickr flatpickr-input active" type="text"
                                   placeholder="শেষের তারিখ..">
                        </div>
                        <div class="col-lg-4 text-center">
                            <p><b> তথ্যের জন্য নিচের বাটনে ক্লিক করুন</b></p>
                            <button type="submit" class="btn btn-success btn-lg btn-block mb-2">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-search">
                                    <circle cx="11" cy="11" r="8"></circle>
                                    <line x1="21" y1="21" x2="16.65" y2="16.65"></line>
                                </svg>
                                অনুসন্ধান
                            </button>
                        </div>
                    </div>
                </form>
                <div class="table-responsive mb-4 mt-4">
                    <table class="table table-bordered mb-4" style="width:100%">
                        <thead>
                        <tr>
                            <th>লেনদেনের তারিখ</th>
                            <th class="text-right">টুকরি ভাড়া টাকার পরিমান</th>
                            <th class="text-right">কমিশন ফেরত</th>
                            {{--<th>চা-নাস্তা খরচ</th>--}}
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($costs))
                        @if($costs->count()>0)
                            @foreach($costs as $cost)
                                <tr>
                                    <td>{{banglaNumber($cost->date)}}</td>
                                    <td class="text-right">{{en2bnNumber($cost->basket)}}</td>
                                    <td class="text-right">{{en2bnNumber($cost->commission)}}</td>
                                </tr>
                            @endforeach
                        @endif
                        @endif
                        </tbody>
                        <tfoot>
                        @if(isset($costs))
                        <tr>
                            <th></th>
                            <th class="text-right">মোট : {{en2bnNumber($cost->sum('basket'))}}</th>
                            <th class="text-right">মোট : {{en2bnNumber($cost->sum('commission'))}}</th>
                        </tr>
                        @endif
                        </tfoot>
                    </table>
                </div>
                <div class="row">
                    <div class="col-12">
                        @if(isset($cost))
                            <form action="{{route('reports.other_costs_print')}}" method="post">
                                @csrf
                                <input type="hidden" value="{{request()->input('end_date')}}" name="end_date">
                                <input type="hidden" value="{{request()->input('start_date')}}" name="start_date">
                                <button type="submit" class="btn btn-primary mb-2">তথ্যগুলো প্রিন্ট
                                    করুন
                                </button>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset("plugins/flatpickr/flatpickr.js")}}"></script>
    <script>
        var f1 = flatpickr(document.getElementById('start_date'));
        var f2 = flatpickr(document.getElementById('end_date'));
    </script>
@endsection
