@extends("layouts.invoice_report")
@section("invoice_head")
    ব্যক্তির বিক্রয় রিপোর্ট
@endsection
@section('customer_section')
    <p class="inv-customer-name">{{$customer->name}}</p>
    <p class="inv-customer-name">{{$customer->company}}</p>
    <p class="inv-street-addr">{{$customer->address}}</p>
@endsection
@section('content')
    <table class="table">
        <thead>{{--
        <tr>
            <th>ব্যক্তির নাম</th>
            <th>লেনদেনের তারিখ</th>
            <th>দাদন পরিমান</th>
            <th>নোট</th>
        </tr>--}}
        <th class="text-left" scope="col">ক্রঃ নং</th>
        <th class="text-left" scope="col">তারিখ</th>
        <th class="text-right">বিক্রয়</th>
        <th class="text-right">কমিশন</th>
        <th class="text-right">ঠেলা/ঘাট</th>
        <th class="text-right">খাজনা</th>
        <th class="text-right">বিহিন্দি জাল সমিতি</th>
        <th class="text-right">মসজিদ</th>
        <th class="text-right">অন্যান্য খরচ</th>
        <th class="text-right">চা-নাস্তা</th>
        <th class="text-right">আমানত</th>
        <th class="text-right">পাকা চৌঠা</th>

        </thead>
        @if(isset($results))
            @php
                $total_paka_chotha= 0;
                 $sell =0;
                 $commission =0;
                 $other_spend =0;
                 $tea_spend =0;
                 $deposit =0;
                 $counter = 0;
            @endphp
            <tbody>
            @foreach($results as $index=>$result)
                <tr>
                    <td class="text-left">{{banglaNumber(++$counter)}}</td>
                    <td>{{banglaNumber($result['date'])}}</td>
                    <td class="text-right">
                        @if(array_key_exists('sell',$result))
                            {{en2bnNumber($result['sell'])}}
                        @endif
                    </td>
                    <td class="text-right">
                        @if(array_key_exists('commission',$result))
                            {{en2bnNumber($result['commission'])}}
                        @endif
                    </td>
                    <td class="text-right">
                        {{en2bnNumber(get_exists_value('ghat',$result))}}
                    </td>
                    <td class="text-right">
                        {{en2bnNumber(get_exists_value('tax',$result))}}
                    </td>
                    <td class="text-right">
                        {{en2bnNumber(get_exists_value('association',$result))}}
                    </td>
                    <td class="text-right">
                        {{en2bnNumber(get_exists_value('mosque',$result))}}
                    </td>
                    <td class="text-right">
                        @if(array_key_exists('other_spend',$result))
                            {{en2bnNumber($result['other_spend'])}}
                        @endif
                    </td>
                    <td class="text-right">
                        @if(array_key_exists('tea_spend',$result))
                            {{en2bnNumber($result['tea_spend'])}}
                        @endif
                    </td>
                    <td class="text-right">
                        @if(array_key_exists('taka',$result))
                            {{en2bnNumber($result['taka'])}}
                        @endif
                    </td>
                    <td class="text-right">
                        @php
                            $paka_chotha = $result['sell']-sum_exists_value(['commission','taka','other_spend','mosque','association','tax','ghat'],$result);
                            $total_paka_chotha = $total_paka_chotha+$paka_chotha;
                        @endphp
                        {{en2bnNumber($paka_chotha)}}
                    </td>
                </tr>
            @endforeach

            </tbody>
            <tfoot>
            <tr>
                <th></th>
                <th></th>
                <th class="text-right">মোট : {{en2bnNumber($sales->sum('sell'))}}</th>
                <th class="text-right">মোট : {{en2bnNumber($sales->sum('commission'))}}</th>
                <th class="text-right">মোট : {{en2bnNumber($sales->sum('ghat'))}}</th>
                <th class="text-right">মোট : {{en2bnNumber($sales->sum('tax'))}}</th>
                <th class="text-right">মোট : {{en2bnNumber($sales->sum('association'))}}</th>
                <th class="text-right">মোট : {{en2bnNumber($sales->sum('mosque'))}}</th>
                <th class="text-right">মোট : {{en2bnNumber($sales->sum('other_spend'))}}</th>
                <th class="text-right">মোট : {{en2bnNumber($sales->sum('tea_spend'))}}</th>
                <th class="text-right">মোট : {{en2bnNumber($installments->sum('taka'))}}</th>
                <th class="text-right">মোট : {{en2bnNumber($total_paka_chotha)}}</th>
            </tr>
            </tfoot>
        @endif
    </table>
@endsection
{{--@section('content_footer')
    <div class="row">
        <div class="col-sm-8 col-7 grand-total-title">
            <h5 class="">সর্বমোট দাদন: </h5>
        </div>
        <div class="col-sm-4 col-5 grand-total-amount">
            <h5 class="">{{en2bnNumber($loans->sum('taka'))}} টাকা</h5>
        </div>
    </div>
@endsection--}}
