@extends("layouts.invoice_report")
@section("invoice_head")
    ব্যক্তির বিক্রয় রিপোর্ট
@endsection
@section('customer_section')
    <p class="inv-customer-name">{{$customer->name}}</p>
    <p class="inv-customer-name">{{$customer->company}}</p>
    <p class="inv-street-addr">{{$customer->address}}</p>
@endsection
@section('content')
    <table class="table">
        <thead>{{--
        <tr>
            <th>ব্যক্তির নাম</th>
            <th>লেনদেনের তারিখ</th>
            <th>দাদন পরিমান</th>
            <th>নোট</th>
        </tr>--}}
            <th class="text-left" scope="col">ক্রঃ নং</th>
            <th class="text-left" scope="col">মোটা দাদন</th>
            <th class="text-left" scope="col">আমানত</th>
            <th class="text-left" scope="col">আমানত বাকি</th>
            <th class="text-left" scope="col">বিক্রি</th>
            <th class="text-left" scope="col">কমিশন</th>
            <th class="text-left" scope="col">পাকা চৌঠা</th>
            <th class="text-left" scope="col">জমা প্রদান</th>
            <th class="text-left" scope="col">জমা বাকি</th>
        </thead>
            <tbody>
            @if(isset($customer))
                <tr>
                    <td>{{banglaNumber(1)}}</td>
                    <td>{{en2bnNumber($loan)}}</td>
                    <td>{{en2bnNumber($installment)}}</td>
                    <td>{{en2bnNumber($loan-$installment)}}</td>
                    <td>{{en2bnNumber($sale)}}</td>
                    <td>{{en2bnNumber($commission)}}</td>
                    <td>{{en2bnNumber($fourth)}}</td>
                    <td>{{en2bnNumber($deposit)}}</td>
                    <td>{{en2bnNumber($fourth-$deposit)}}</td>
                </tr>
            @endif

            </tbody>
    </table>
@endsection
{{--@section('content_footer')
    <div class="row">
        <div class="col-sm-8 col-7 grand-total-title">
            <h5 class="">সর্বমোট দাদন: </h5>
        </div>
        <div class="col-sm-4 col-5 grand-total-amount">
            <h5 class="">{{en2bnNumber($loans->sum('taka'))}} টাকা</h5>
        </div>
    </div>
@endsection--}}
