@extends("layouts.invoice_report")
@section("invoice_head")
    ভাউচার
@endsection
@section('customer_section')
    <p class="inv-customer-name">{{$customer->name}}</p>
    <p class="inv-customer-name">{{$customer->company}}</p>
    <p class="inv-street-addr">{{$customer->address}}</p>
@endsection
@section('content')
    <table class="table">
        <thead>{{--
        <tr>
            <th>ব্যক্তির নাম</th>
            <th>লেনদেনের তারিখ</th>
            <th>দাদন পরিমান</th>
            <th>নোট</th>
        </tr>--}}
        <th class="text-left" scope="col">ক্রঃ নং</th>
        <th class="text-left" scope="col">তারিখ</th>
        <th class="text-center" scope="col">বিবরণ</th>
        <th class="text-right" scope="col">টাকার পরিমান</th>
        </thead>
        @if(isset($loans))
            <tbody>
            @foreach($loans as $index=>$loan)
                <tr>
                    <td class="text-left">{{banglaNumber(++$index)}}</td>
                    <td class="text-left">{{banglaNumber($loan->date)}}</td>
                    <td class="text-center">{{$loan->note}}</td>
                    <td class="text-right">{{en2bnNumber($loan->taka)}}</td>
                </tr>
            @endforeach

            </tbody>
        @endif
    </table>
@endsection
@section('content_footer')
    <div class="row">
        <div class="col-sm-8 col-7 grand-total-title">
            <h5 class="">সর্বমোট দাদন: </h5>
        </div>
        <div class="col-sm-4 col-5 grand-total-amount">
            <h5 class="">{{en2bnNumber($loans->sum('taka'))}} টাকা</h5>
        </div>
    </div>
@endsection
