@extends("layouts.invoice_report")
@section("invoice_head")
    ব্যক্তি জমার রিপোর্ট
@endsection
@section('customer_section')
    <p class="inv-customer-name">{{$customer->name}}</p>
    <p class="inv-customer-name">{{$customer->company}}</p>
    <p class="inv-street-addr">{{$customer->address}}</p>
@endsection
@section('content')
    <table class="table">
        <thead>{{--
        <tr>
            <th>ব্যক্তির নাম</th>
            <th>লেনদেনের তারিখ</th>
            <th>দাদন পরিমান</th>
            <th>নোট</th>
        </tr>--}}
            <th class="text-left" scope="col">ক্রঃ নং</th>
            <th class="text-left" scope="col">তারিখ</th>
            <th class="text-left">মোট পাকা চৌঠা</th>
            <th class="text-left">আগে টাকা প্রদান করা হয়েছিল</th>
            <th class="text-left">জমা অবশিষ্ট টাকা</th>
        </thead>
        <tbody>
        @if(isset($deposits))
            @foreach($deposits as $index=> $deposit)
                <tr>
                    <td class="text-left">{{banglaNumber(++$index)}}</td>
                    <td class="text-left">{{banglaNumber($deposit->date)}}</td>
                    <td class="text-left">{{en2bnNumber($fourth)}}</td>
                    <td class="text-left">{{en2bnNumber($deposit->deposit)}}</td>
                    <td class="text-left">{{en2bnNumber($fourth-$deposit->deposit)}}</td>
                </tr>
                @php
                    $fourth-=$deposit->deposit;
                @endphp
            @endforeach
        @endif
        </tbody>
    </table>
@endsection
