@extends('layouts.master')
@section("style")
    <link rel="stylesheet" href="{{asset('plugins/flatpickr/flatpickr.css')}}">
@endsection
@section('heading')
    <h3>অন্যান্য খরচের তথ্যসমূহ</h3>
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{route('home')}}">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                </svg>
            </a>
        </li>
        <li class="breadcrumb-item active" aria-current="page"><span>খরচ</span></li>
    </ol>
@endsection
@section('content')
    <div class="row layout-top-spacing">
        <div class="container layout-spacing">
            <div class="widget-content-area br-4">
                <div class="widget-one">
                    <h5 class="text-center">নতুন খরচের তথ্য</h5>
                    <form action="{{route('cost_edit',$cost->id)}}" method="post">
                        @csrf
                        <div class="form-group mb-4">
                            <label for="date">তারিখ </label>
                            <input value="{{$cost->date}}" name="date" id="date" class="form-control flatpickr flatpickr-input active" type="text" placeholder="Select Date.." readonly="readonly">
                        </div>
                        <div class="form-group mb-4">
                            <label for="basket">টুকরি ভাড়া টাকার পরিমান </label>
                            <input value="{{$cost->basket}}" id="basket"  type="number" name="basket" placeholder="২০,২৫০ টাকা " class="form-control" required="">
                        </div>

                        <div class="form-group mb-4">
                            <label for="commission">কমিশন ফেরত  </label>
                            <input value="{{$cost->commission}}" id="commission" type="number" name="commission" placeholder="২০,২৫০ টাকা " class="form-control" required="">
                        </div>
                        <div class="form-group mb-4">
                            <label for="details">বিস্তারিত </label>
                            <textarea name="details" class="form-control" id="details" rows="1">{{$cost->details}}</textarea>
                        </div>
                        <input type="submit" value="সাবমিট " name="submit" class="btn btn-primary btn-block mb-4 mr-2">
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset("plugins/flatpickr/flatpickr.js")}}"></script>
    <script>
        var f1 = flatpickr(document.getElementById('date'));
    </script>
@endsection
