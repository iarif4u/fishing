@extends('layouts.master')
@section('heading')
    <h3>এডমিন এর সকল তথ্য</h3>
@endsection
@section("style")
    <link rel="stylesheet" href="{{asset("plugins/dropify/dropify.min.js")}}">
    <link rel="stylesheet" href="{{asset("plugins/flatpickr/flatpickr.css")}}">
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0);">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                     class="feather feather-home">
                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                </svg>
            </a></li>
        <li class="breadcrumb-item active" aria-current="page"><span>ব্যক্তি তথ্য</span></li>
    </ol>
@endsection

@section('content')
    <div class="row layout-spacing">

        <!-- Content -->
        <div class="col-xl-4 col-lg-6 col-md-5 col-sm-12 layout-top-spacing">

            <div class="user-profile layout-spacing">
                <div class="widget-content widget-content-area">
                    <div class="d-flex justify-content-between">
                        <h3 class="">এডমিন এর তথ্য সমূহ</h3>
                    </div>
                    <div class="text-center user-info">
                        @if(get_media_file(auth()->user(),config('const.media.profile')))
                            <img class="img-fluid" src="{{get_media_file(auth()->user(),config('const.media.profile'))}}" alt="avatar">
                        @else
                            <img class="img-fluid" src="{{asset('assets/img/120x120.jpg')}}" alt="avatar">
                        @endif
                        <p class="">{{auth()->user()->name}}</p>
                    </div>
                    <div class="user-info-list">

                        <div class="">
                            <ul class="contacts-block list-unstyled">
                                <li class="contacts-block__item">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="feather feather-calendar">
                                        <rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect>
                                        <line x1="16" y1="2" x2="16" y2="6"></line>
                                        <line x1="8" y1="2" x2="8" y2="6"></line>
                                        <line x1="3" y1="10" x2="21" y2="10"></line>
                                    </svg> {{auth()->user()->date_of_birth}}
                                </li>
                                <li class="contacts-block__item">
                                    <a href="mailto:example@mail.com">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"
                                             viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2"
                                             stroke-linecap="round" stroke-linejoin="round"
                                             class="feather feather-mail">
                                            <path
                                                d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
                                            <polyline points="22,6 12,13 2,6"></polyline>
                                        </svg>{{auth()->user()->email}}</a>
                                </li>
                                <li class="contacts-block__item">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                         fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                         stroke-linejoin="round" class="feather feather-phone">
                                        <path
                                            d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path>
                                    </svg> {{auth()->user()->phone}}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-xl-8 col-lg-6 col-md-7 col-sm-12 layout-top-spacing">
            <div class="skills layout-spacing ">
                <div class="widget-content widget-content-area">
                    <h3 class="">আপনার ব্যক্তিগত তথ্য গুলো হাল নাগাদ করুন</h3>
                    <form enctype="multipart/form-data" method="post" action="{{route('profile')}}">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="name"> নাম</label>
                                    <input value="{{auth()->user()->name}}" type="text" name="name"
                                           class="form-control mb-4" id="name" placeholder=" নাম"/>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="date_of_birth"> জন্ম তারিখ</label>
                                    <input value="{{auth()->user()->date_of_birth}}" name="date_of_birth"
                                           id="date_of_birth" class="form-control flatpickr flatpickr-input active"
                                           type="text" placeholder="তারিখ নির্বাচন করুন .." readonly="readonly">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="email"> ইমেইল এড্রেস</label>
                                    <input value="{{auth()->user()->email}}" name="email" type="email"
                                           class="form-control mb-4" id="email" placeholder=" ইমেইল এড্রেস"/>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label for="phone"> মোবাইল নাম্বার</label>
                                    <input value="{{auth()->user()->phone}}" type="tel" class="form-control mb-4"
                                           name="phone" id="phone" placeholder="  মোবাইল নাম্বার"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p><b> পাসওয়ার্ড পরিবর্তন</b></p>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label for="password"> নতুন পাসওয়ার্ড </label>
                                    <input name="password" type="password" class="form-control mb-4" id="password"
                                           placeholder="পাসওয়ার্ড"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p><b> ছবি পরিবর্তন করুন</b></p>
                                @if(get_media_file(auth()->user(),config('const.media.profile')))
                                    <input name="photo" type="file" id="input-file-NIDCard" class="dropify"
                                           data-default-file="{{get_media_file(auth()->user(),config('const.media.profile'))}}"
                                           data-max-file-size="2M"/>
                                @else
                                    <input name="photo" type="file" id="input-file-NIDCard" class="dropify"
                                           data-default-file="{{asset("assets/img/200x200.jpg")}}"
                                           data-max-file-size="2M"/>
                                @endif
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12  mt-4 text-center">
                                <button type="submit" id="multiple-messages" class="btn btn-success">সকল তথ্য হালনাগাদ
                                    করুন
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!-- end --->
        </div>

    </div>

    </div>
@endsection
@section("script")
    <script src="{{asset("plugins/dropify/dropify.min.js")}}"></script>
    <script src="{{asset("plugins/flatpickr/flatpickr.js")}}"></script>
    <script>
        $('.dropify').dropify();
        var f1 = flatpickr(document.getElementById('date_of_birth'));
    </script>
@endsection
