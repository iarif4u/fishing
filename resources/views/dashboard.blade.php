@extends("layouts.master")
@section('heading')
    <h3>ড্যাশবোর্ড</h3>
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="javascript:void(0);"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
        <li class="breadcrumb-item active" aria-current="page"><span>ড্যাশবোর্ড</span></li>
    </ol>
@endsection
@section('content')
    <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
                    <div class="widget-content-area br-4">
                        <div class="skills layout-spacing ">
                            <div class="row">
                                <div class="col-lg-4">
                                    <button class="btn btn-primary mb-4  btn-block btn-lg">
                                        <b>খাজনা</b> </br> {{en2bnNumber($today_tax)}}/=
                                    </button>
                                </div>
                                <div class="col-lg-4">
                                    <button class="btn btn-success mb-4  btn-block btn-lg">
                                        <b>ঠেলা/ঘাট</b> </br> {{en2bnNumber($today_ghat)}}/=
                                    </button>
                                </div>
                                <div class="col-lg-4">
                                    <button class="btn btn-warning mb-4  btn-block btn-lg">
                                        <b>মসজিদ</b> </br> {{en2bnNumber($today_mosque)}}/=
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <button class="btn btn-primary mb-4  btn-block btn-lg">
                                        <b> বিক্রি </b> </br> {{en2bnNumber($today_sale)}}/=
                                    </button>
                                </div>
                                <div class="col-lg-4">
                                    <button class="btn btn-success mb-4  btn-block btn-lg">
                                        <b> কমিশন </b> </br> {{en2bnNumber($today_commission)}}/=
                                    </button>
                                </div>
                                <div class="col-lg-4">
                                    <button class="btn btn-warning mb-4  btn-block btn-lg">
                                        <b>আমানত/ কিস্তি  পরিশোধ</b> </br>{{en2bnNumber($today_installment)}}/=
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <button class="btn btn-primary mb-4  btn-block btn-lg">
                                        <b>পাকা চৌঠা</b> </br> {{en2bnNumber($today_fourth)}}/=

                                    </button>
                                </div>
                                <div class="col-lg-4">
                                    <button class="btn btn-success mb-4  btn-block btn-lg">
                                        <b>বিহিন্দি জাল সমিতি</b> </br> {{en2bnNumber($today_association)}}/=
                                    </button>
                                </div>

                                <div class="col-lg-4">
                                    <button class="btn btn-warning mb-4  btn-block btn-lg">
                                        <b> চা-খরচ </b> </br> {{en2bnNumber($tea_spend)}}/=
                                    <!-- <b>মোট আমানত/ কিস্তি  পরিশোধ</b> </br>{{en2bnNumber($installment)}}/=-->
                                    </button>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <button class="btn btn-primary mb-4  btn-block btn-lg">
                                        <b> মোট বিক্রি </b> </br> {{en2bnNumber($sale)}}/=
                                    </button>
                                </div>
                                <div class="col-lg-4">
                                    <button class="btn btn-success mb-4  btn-block btn-lg">
                                        <b> দাদন বাকি</b> </br> {{en2bnNumber($loan-$installment)}}/=
                                    </button>
                                </div>
                                <div class="col-lg-4">
                                    <button class="btn btn-warning mb-4  btn-block btn-lg">
                                        <b>মোট কমিশন </b> </br> {{en2bnNumber($commission)}}/=
                                    </button>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
@endsection
