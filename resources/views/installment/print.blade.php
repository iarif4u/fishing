@extends('layouts.invoice')

@section('customer_section')
    <p class="inv-customer-name">{{$installment->customer->name}}</p>
    <p class="inv-customer-name">{{$installment->customer->company}}</p>
    <p class="inv-street-addr">{{$installment->customer->address}}</p>
@endsection
@section("invoice_head")
    ভাউচার
@endsection
@section('invoice_no')
    {{banglaNumber($installment->id)}}{{banglaNumber($installment->customer->id)}}{{banglaNumber(date('md',strtotime($installment->created_at)))}}
@endsection

@section('invoice_date')
    {{banglaNumber($installment->date)}}
@endsection

@section('content')
    <table class="table">
        <thead class="">
        <tr>
            <th class="text-left" scope="col">ক্রঃ নং</th>
            <th class="text-center" scope="col">বিবরণ</th>
            <th class="text-right" scope="col">টাকার পরিমান</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td class="text-left">{{banglaNumber(1)}}</td>
            <td class="text-center">{{$installment->note}}</td>
            <td class="text-right">{{en2bnNumber($installment->taka)}}</td>
        </tr>
        </tbody>
    </table>
@endsection
@section('content_footer')
    <div class="row">
        <div class="col-sm-8 col-7 grand-total-title">
            <h5 class="">সর্বমোট আমানত: </h5>
        </div>
        <div class="col-sm-4 col-5 grand-total-amount">
            <h5 class="">{{en2bnNumber($installment->taka)}} টাকা</h5>
        </div>
    </div>
@endsection
