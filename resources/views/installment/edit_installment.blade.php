@extends('layouts.master')
@section('style')
    <link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/flatpickr/flatpickr.css')}}">
@endsection
@section('heading')
    <h3>নতুন আমানত/ কিস্তি যুক্ত  করুন </h3>
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{route('home')}}">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                </svg>
            </a>
        </li>
        <li class="breadcrumb-item"><a href="javascript:void(0);">আমানত/ কিস্তি </a></li>
        <li class="breadcrumb-item active" aria-current="page"><span>নতুন আমানত/ কিস্তি </span></li>
    </ol>
@endsection
@section('content')
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
            <div class="widget-content-area br-4">
                <form action="{{route('installment.edit',$installment->installment_uid)}}" method="post">
                    @csrf
                    <div class="form-row mb-4">
                        <div class="form-group col-md-6 select2-customer mb-0">
                            <label for="customer_id">ব্যক্তির নাম</label>
                            <select name="customer_id" id="customer_id" class="form-control">
                                <option selected value="{{$installment->customer->id}}">{{$installment->customer->name}} ({{$installment->customer->phone}} )</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="taka">টাকার পরিমান</label>
                            <input name="taka" type="number" class="form-control" id="taka" value="{{$installment->taka}}" placeholder="৬০,০০০">
                        </div>
                    </div>
                    <div class="form-row mb-4">
                        <div class="form-group col-md-6 mb-0">
                            <label for="date">তারিখ</label>
                            <input value="{{$installment->date}}" name="date" id="date" class="form-control flatpickr flatpickr-input active" type="text" placeholder="তারিখ নির্বাচন করুন ..">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="note">নোট</label>
                            <input value="{{$installment->note}}"  name="note" type="text" class="form-control" id="note" placeholder="নোট">
                        </div>
                    </div>

                    <button type="submit" class="btn btn-primary mt-3">তথ্য যুক্ত করুন</button>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script src="{{asset("plugins/flatpickr/flatpickr.js")}}"></script>
    <script>
        var f1 = flatpickr(document.getElementById('date'));
        $("#customer_id").select2({
            placeholder: "Select Customer",
            ajax: {
                url: '{{route('users.search')}}',
                dataType: 'json',
                data: function (params) {
                    // Query parameters will be ?search=[term]&type=public
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: txt.name}
                        })
                    };
                }
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        });
    </script>
@endsection
