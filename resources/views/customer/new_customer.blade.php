@extends("layouts.master")
@section("style")
    <link rel="stylesheet" href="{{asset("plugins/dropify/dropify.min.js")}}">
    <link rel="stylesheet" href="{{asset("plugins/flatpickr/flatpickr.css")}}">
@endsection
@section('heading')
    <h3>নতুন ব্যক্তি তৈরি</h3>
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{route('home')}}">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home">
                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                </svg>
            </a>
        </li>
        <li class="breadcrumb-item"><a href="javascript:void(0);">ব্যক্তি </a></li>
        <li class="breadcrumb-item active" aria-current="page"><span>নতুন ব্যক্তি তৈরি</span></li>
    </ol>
@endsection
@section('content')
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 layout-spacing">
            <form action="{{route("users.add")}}" method="post" id="general-info" class="section general-info" enctype="multipart/form-data">
                @csrf
                <div class="info">
                    <div class="row">
                        <div class="col-lg-11 mx-auto">
                            <div class="row">
                                <div class="col-xl-2 col-lg-12 col-md-4">
                                    <div class="upload mt-4 pr-md-4">
                                        <input name="profile" type="file" id="input-file-max-fs" class="dropify" data-default-file="{{asset("assets/img/200x200.jpg")}}" data-max-file-size="2M" />
                                        <p class="mt-2"><i class="flaticon-cloud-upload mr-1"></i> আপলোড ছবি</p>
                                    </div>
                                </div>
                                <div class="col-xl-10 col-lg-12 col-md-8 mt-md-0 mt-4">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="name">ব্যক্তি পুরো নাম</label>
                                                <input value="{{old("name")}}" name="name" type="text" class="form-control mb-4" id="name" placeholder="Full Name" />
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <label for="dob" class="dob-input">জন্ম তারিখ</label>
                                            <div class="d-sm-flex d-block">
                                                <div class="form-group mr-1">
                                                    <input type="text" value="{{old("dob")}}" class="form-control mb-4" id="dob" name="dob" placeholder="YYYY-MM-DD" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="company"> প্রতিষ্ঠানের</label>
                                        <input value="{{old("company")}}" name="company" type="text" class="form-control mb-4" id="company" placeholder="মেসার্স মা মরিয়ম ফিশিং " />
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="father">বাবার নাম</label>
                                        <input value="{{old("father")}}" name="father" type="text" class="form-control mb-4" id="father" placeholder="ব্যক্তি  বাবার নাম এখানে লিখুন"  />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="mother">মায়ের নাম</label>
                                        <input value="{{old("mother")}}" type="text" class="form-control mb-4" name="mother" id="mother" placeholder="ব্যক্তি  মায়ের নাম এখানে লিখুন" />
                                    </div>
                                </div>
                                <div class="col-md-6">

                                    <div class="form-group">
                                        <label for="email">ইমেইল</label>
                                        <input name="email" type="text" class="form-control mb-4" id="email" placeholder=" ইমেল এখানে লিখুন" value="{{old("email")}}" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phone">ফোন</label>
                                        <input name="phone" type="text" class="form-control mb-4" id="phone" placeholder="ফোন নম্বর এখানে লিখুন" value="{{old("phone")}}" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="address"> বর্তমান ঠিকানা</label>
                                        <input value="{{old("address")}}" name="address" type="text" class="form-control mb-4" id="address" placeholder="বর্তমান ঠিকানা এখানে লিখুন" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="permanent_address">স্থায়ী ঠিকানা</label>
                                        <input value="{{old("permanent_address")}}" name="permanent_address" type="text" class="form-control mb-4" id="permanent_address" placeholder="স্থায়ী ঠিকানা এখানে লিখুন" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="upload mt-4 pr-md-4">
                                        <input name="nid_card" type="file" id="input-file-NIDCard" class="dropify" data-default-file="{{asset("assets/img/200x200.jpg")}}" data-max-file-size="2M" />
                                        <p class="mt-2"><i class="flaticon-cloud-upload mr-1"></i> এন আই ডি কার্ড আপলোড </p>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="upload mt-4 pr-md-4">
                                        <input name="check" type="file" id="input-file-CheckCopy" class="dropify" data-default-file="{{asset("assets/img/200x200.jpg")}}" data-max-file-size="2M" />
                                        <p class="mt-2"><i class="flaticon-cloud-upload mr-1"></i> চেক এর স্ক্যান কপি আপলোড </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12  mt-4 text-center">
                            <button type="submit" id="multiple-messages" class="btn btn-primary">Save Changes</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
@section("script")
    <script src="{{asset("plugins/dropify/dropify.min.js")}}"></script>
    <script src="{{asset("plugins/flatpickr/flatpickr.js")}}"></script>
    <script>
        $('.dropify').dropify();
        var f1 = flatpickr(document.getElementById('dob'));
    </script>
@endsection
