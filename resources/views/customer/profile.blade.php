@extends('layouts.master')
@section('heading')
    <h3>{{$customer->name}} সম্পূর্ণ তথ্য সমূহ</h3>
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{route('home')}}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg></a></li>
        <li class="breadcrumb-item"><a href="{{route("users.add")}}">ব্যক্তি</a></li>
        <li class="breadcrumb-item"><a href="{{route('users.list')}}">ব্যক্তি লিস্ট</a></li>
        <li class="breadcrumb-item active" aria-current="page"><span>ব্যক্তি তথ্য</span></li>
    </ol>
@endsection
@section('content')
    <div class="row layout-spacing">

        <!-- Content -->
        <div class="col-xl-4 col-lg-6 col-md-5 col-sm-12 layout-top-spacing">

            <div class="user-profile layout-spacing">
                <div class="widget-content widget-content-area">
                    <div class="d-flex justify-content-between">
                        <h3 class="">ব্যক্তিগত তথ্য সমূহ</h3>
                        <a href="{{route('users.edit',['user_id'=>$customer->id])}}" class="mt-2 edit-profile"> <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-3"><path d="M12 20h9"></path><path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path></svg></a>
                    </div>
                    <div class="text-center user-info">
                        <img class="img-fluid" src="{{get_media_file($customer,config('const.media.profile'))}}" alt="avatar">
                        <p class="">{{$customer->name}}</p>
                    </div>
                    <div class="user-info-list">

                        <div class="">
                            <ul class="contacts-block list-unstyled">
                                <li class="contacts-block__item">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-coffee"><path d="M18 8h1a4 4 0 0 1 0 8h-1"></path><path d="M2 8h16v9a4 4 0 0 1-4 4H6a4 4 0 0 1-4-4V8z"></path><line x1="6" y1="1" x2="6" y2="4"></line><line x1="10" y1="1" x2="10" y2="4"></line><line x1="14" y1="1" x2="14" y2="4"></line></svg>{{$customer->company}}
                                </li>
                                <li class="contacts-block__item">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-calendar"><rect x="3" y="4" width="18" height="18" rx="2" ry="2"></rect><line x1="16" y1="2" x2="16" y2="6"></line><line x1="8" y1="2" x2="8" y2="6"></line><line x1="3" y1="10" x2="21" y2="10"></line></svg>{{$customer->date_of_birth}}
                                </li>
                                <li class="contacts-block__item">
                                    <a href="mailto:{{$customer->email}}"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>{{$customer->email}}</a>
                                </li>
                                <li class="contacts-block__item">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-phone"><path d="M22 16.92v3a2 2 0 0 1-2.18 2 19.79 19.79 0 0 1-8.63-3.07 19.5 19.5 0 0 1-6-6 19.79 19.79 0 0 1-3.07-8.67A2 2 0 0 1 4.11 2h3a2 2 0 0 1 2 1.72 12.84 12.84 0 0 0 .7 2.81 2 2 0 0 1-.45 2.11L8.09 9.91a16 16 0 0 0 6 6l1.27-1.27a2 2 0 0 1 2.11-.45 12.84 12.84 0 0 0 2.81.7A2 2 0 0 1 22 16.92z"></path></svg> {{$customer->phone}}
                                </li>
                                <li class="contacts-block__item">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>  বাবার নাম : {{$customer->father}}
                                </li>
                                <li class="contacts-block__item">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg> মায়ের নাম :  {{$customer->mother}}
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div class="education layout-spacing ">
                <div class="widget-content widget-content-area">
                    <h3 class=""> বর্তমান ঠিকানা</h3>
                    <p>ঠিকানা: {{$customer->address}}</p>
                </div>
            </div>

            <div class="education layout-spacing ">
                <div class="widget-content widget-content-area">
                    <h3 class=""> স্থায়ী ঠিকানা</h3>
                    <p>ঠিকানা: {{$customer->permanent_address}}</p>
                </div>
            </div>
        </div>
        <div class="col-xl-8 col-lg-6 col-md-7 col-sm-12 layout-top-spacing">

            <div class="skills layout-spacing ">
                <div class="widget-content widget-content-area">
                    <h3 class="">সম্পূর্ণ লেনদেন</h3>
                    <div class="row">
                        <div class="col-lg-4">
                            <button class="btn btn-primary mb-4  btn-block btn-lg">
                                <b> মোটা দাদন</b> </br> {{en2bnNumber($loan)}}
                            </button>
                        </div>
                        <div class="col-lg-4">
                            <button class="btn btn-success mb-4  btn-block btn-lg">
                                <b> আমানত/ কিস্তি  পরিশোধ</b> </br>{{en2bnNumber($installment)}}
                            </button>
                        </div>
                        <div class="col-lg-4">
                            <button class="btn btn-warning mb-4  btn-block btn-lg">
                                <b> আমানত/ কিস্তি  বাকি</b> </br> {{en2bnNumber($loan-$installment)}}
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <button class="btn btn-primary mb-4  btn-block btn-lg">
                                <b> মোট বিক্রি </b> </br> {{en2bnNumber($sale)}}
                            </button>
                        </div>
                        <div class="col-lg-4">
                            <button class="btn btn-success mb-4  btn-block btn-lg">
                                <b> কমিশন </b> </br> {{en2bnNumber($commission)}}
                            </button>
                        </div>
                        <div class="col-lg-4">
                            <button class="btn btn-warning mb-4  btn-block btn-lg">
                                <b> অন্যান্য খরচ</b> </br> {{en2bnNumber($other_spend)}}
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <button class="btn btn-primary mb-4  btn-block btn-lg">
                                <b> ঠেলা/ঘাট</b> </br> {{en2bnNumber($ghat)}}
                            </button>
                        </div>
                        <div class="col-lg-4">
                            <div class="row">
                                <div class="col-lg-6">
                            <button class="btn btn-success mb-4  btn-block btn-lg">
                                <b> খাজনা </b> </br> {{en2bnNumber($tax)}}
                            </button>
                                </div>
                                <div class="col-lg-6">
                            <button class="btn btn-success mb-4  btn-block btn-lg">
                                <b> মসজিদ </b> </br> {{en2bnNumber($mosque)}}
                            </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <button class="btn btn-warning mb-4  btn-block btn-lg">
                                <b>বিহিন্দি জাল সমিতি</b> </br> {{en2bnNumber($association)}}
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-4">
                            <button class="btn btn-primary mb-4  btn-block btn-lg">
                                <b> পাকা চৌঠা </b> </br> {{en2bnNumber($fourth)}}
                            </button>
                        </div>
                        <div class="col-lg-4">
                            <button class="btn btn-success mb-4  btn-block btn-lg">
                                <b>  জমা গ্রহণ</b> </br> {{en2bnNumber($deposit)}}
                            </button>
                        </div>
                        <div class="col-lg-4">
                            <button class="btn btn-warning mb-4  btn-block btn-lg">
                                <b> জমা বাকি আছে</b> </br> {{en2bnNumber(round($fourth-$deposit))}}
                            </button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="bio layout-spacing ">
                <div class="widget-content widget-content-area pb-4">
                    <h3 class="">লেনদেন  হিসাব</h3>

                    <ul class="nav nav-tabs  mb-3 mt-3 justify-content-center" id="justifyCenterTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="justify-center-home-tab" data-toggle="tab" href="#justify-center-home" role="tab" aria-controls="justify-center-home" aria-selected="true">দাদন গ্রহণের হিসাব-নিকাশ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="justify-center-profile-tab" data-toggle="tab" href="#justify-center-profile" role="tab" aria-controls="justify-center-profile" aria-selected="false">আমানত/ কিস্তি প্রদান হিসাব-নিকাশ</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="justify-center-contact-tab" data-toggle="tab" href="#justify-center-contact" role="tab" aria-controls="justify-center-contact" aria-selected="false">জমা উত্তোলনের হিসাব নিকাশ</a>
                        </li>
                    </ul>

                    <div class="tab-content" id="justifyCenterTabContent">
                        <!-- Start -->
                        <div class="tab-pane fade show active" id="justify-center-home" role="tabpanel" aria-labelledby="justify-center-home-tab">
                            <div class="row layout-top-spacing" id="cancel-row">
                                <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                                    <div class="table-responsive mb-4 mt-4">
                                        <table id="alter_pagination" class="table table-hover" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>তারিখ</th>
                                                <th>টাকার পরিমান</th>
                                                <th> মোট দাদন </th>
                                                <th class="text-center">আরো কিছু</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @php
                                                $amount = 0;
                                            @endphp
                                            @if($loans->count()>0)
                                                @foreach($loans as $cs_loan)
                                                    @if($cs_loan->taka>0)
                                                    @php
                                                        $amount=$amount+$cs_loan->taka;
                                                    @endphp
                                                        <tr>
                                                            <td>{{banglaNumber($cs_loan->date)}}</td>
                                                            <td>{{en2bnNumber($cs_loan->taka)}}</td>
                                                            <td>{{en2bnNumber($amount)}}</td>
                                                            <td class="text-center">
                                                                <a href="{{route('loan.print',$cs_loan->loan_uid)}}" target="_blank" class="bs-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Invoice"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clipboard"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg></a>
                                                                <a href="{{route('loan.edit',$cs_loan->loan_uid)}}" class="bs-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-3"><path d="M12 20h9"></path><path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path></svg></a>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endif
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>তারিখ</th>
                                                <th>টাকার পরিমান</th>
                                                <th> মোট দাদন  </th>
                                                <th class="text-center">আরো কিছু</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- End --->
                        <!-- Start --->
                        <div class="tab-pane fade" id="justify-center-profile" role="tabpanel" aria-labelledby="justify-center-profile-tab">
                            <div class="row layout-top-spacing" id="cancel-row">
                                <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                                    <div class="table-responsive mb-4 mt-4">
                                        <table id="alter_pagination1" class="table table-hover" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>তারিখ</th>
                                                <th>টাকার পরিমান</th>
                                                <th>আমানত/ কিস্তি  বাকি</th>
                                                <th class="text-center">আরো কিছু</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($installments->count()>0)
                                                @foreach($installments as $cs_installment)
                                                    @if($cs_installment->taka>0)
                                                        <tr>
                                                            <td>{{banglaNumber($cs_installment->date)}}</td>
                                                            <td>{{en2bnNumber($cs_installment->taka)}}</td>
                                                            <td>{{en2bnNumber($cs_installment->transaction->trans_calculation)}}</td>
                                                            <td class="text-center">
                                                                <a href="{{route('installment.print',$cs_installment->installment_uid)}}" target="_blank" class="bs-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Invoice"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clipboard"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg></a>
                                                                <a href="{{route('installment.edit',$cs_installment->installment_uid)}}" class="bs-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-3"><path d="M12 20h9"></path><path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path></svg></a>
                                                            </td>
                                                        </tr>
                                                    @endif
                                                @endforeach
                                            @endif
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>তারিখ</th>
                                                <th>টাকার পরিমান</th>
                                                <th>মোট আমানত/কিস্তি  </th>
                                                <th class="text-center">আরো কিছু</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="justify-center-contact" role="tabpanel" aria-labelledby="justify-center-contact-tab">
                            <div class="row layout-top-spacing" id="cancel-row">
                                <div class="col-xl-12 col-lg-12 col-sm-12  layout-spacing">
                                    <div class="table-responsive mb-4 mt-4">
                                        <table id="alter_pagination2" class="table table-hover" style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>তারিখ</th>
                                                <th>টাকার পরিমান</th>
                                                {{--<th> মোট জমা বাকি আছে </th>--}}
                                                <th class="text-center">আরো কিছু</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @if($deposits->count())
                                                @foreach($deposits as $cs_deposit)
                                            <tr>
                                                <td>{{banglaNumber($cs_deposit->date)}}</td>
                                                <td>{{en2bnNumber($cs_deposit->deposit)}}</td>
                                                {{--<td>১,০০,০০০</td>--}}
                                                <td class="text-center">
                                                    <a href="{{route('deposit.print',$cs_deposit->deposit_uid)}}" target="_blank" class="bs-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Invoice"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-clipboard"><path d="M16 4h2a2 2 0 0 1 2 2v14a2 2 0 0 1-2 2H6a2 2 0 0 1-2-2V6a2 2 0 0 1 2-2h2"></path><rect x="8" y="2" width="8" height="4" rx="1" ry="1"></rect></svg></a>
                                                    <a href="{{route('deposit.edit',$cs_deposit->deposit_uid)}}" class="bs-tooltip" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-edit-3"><path d="M12 20h9"></path><path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path></svg></a>
                                                </td>
                                            </tr>
                                                @endforeach
                                            @endif
                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th>তারিখ</th>
                                                <th>টাকার পরিমান</th>
                                                {{--<th> মোট জমা বাকি আছে </th>--}}
                                                <th class="text-center">আরো কিছু</th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- End --->
                    </div>
                </div>
                <!-- end --->
            </div>

            <!-- Start Infomation -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="education layout-spacing ">
                        <div class="widget-content widget-content-area">
                            <h3 class="">এন আই ডি কার্ড </h3>
                            <p><img  class="img-fluid" src="{{get_media_file($customer,config('const.media.nid_card'))}}" alt="avatar"></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="education layout-spacing ">
                        <div class="widget-content widget-content-area">
                            <h3 class=""> চেক এর স্ক্যান কপি </h3>
                            <p><img class="img-fluid" src="{{get_media_file($customer,config('const.media.check'))}}" alt="avatar"></p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End Information -->
        </div>

    </div>
@endsection
@section("style")
    <link rel="stylesheet" type="text/css" href="{{asset('plugins/table/datatable/datatables.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/custom_dt_html5.css")}}">
    <link rel="stylesheet" type="text/css" href="{{asset("plugins/table/datatable/dt-global_style.css")}}">
@endsection
@section('script')
    <!-- BEGIN PAGE LEVEL CUSTOM SCRIPTS -->
    <script src="{{asset('plugins/table/datatable/datatables.js')}}"></script>
    <!-- NOTE TO Use Copy CSV Excel PDF Print Options You Must Include These Files  -->
    <script src="{{asset('plugins/table/datatable/button-ext/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/jszip.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/buttons.html5.min.js')}}"></script>
    <script src="{{asset('plugins/table/datatable/button-ext/buttons.print.min.js')}}"></script>
    <script>
        $(document).ready(function() {
            $('#alter_pagination,#alter_pagination1,#alter_pagination2').DataTable( {
                "pagingType": "full_numbers",
                "oLanguage": {
                    "oPaginate": {
                        "sFirst": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-left"><polyline points="15 18 9 12 15 6"></polyline></svg>',
                        "sPrevious": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-left"><line x1="19" y1="12" x2="5" y2="12"></line><polyline points="12 19 5 12 12 5"></polyline></svg>',
                        "sNext": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-right"><line x1="5" y1="12" x2="19" y2="12"></line><polyline points="12 5 19 12 12 19"></polyline></svg>',
                        "sLast": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-right"><polyline points="9 18 15 12 9 6"></polyline></svg>'
                    },
                    "sInfo": "Showing page _PAGE_ of _PAGES_",
                    "sSearch": '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-search"><circle cx="11" cy="11" r="8"></circle><line x1="21" y1="21" x2="16.65" y2="16.65"></line></svg>',
                    "sSearchPlaceholder": "Search...",
                    "sLengthMenu": "Results :  _MENU_",
                },
                "stripeClasses": [],
                "lengthMenu": [5, 10, 20, 50],
                "pageLength": 5
            });
        } );
    </script>
@endsection
