<div class="btn-group">
    <a href="{{route('users.profile',["user_id"=>$customer->id])}}" type="button" class="btn btn-dark btn-sm">বিস্তারিত তথ্য</a>
    <button type="button" class="btn btn-dark btn-sm dropdown-toggle dropdown-toggle-split" id="dropdownMenuReference1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuReference1">
        <a class="dropdown-item" href="{{route('users.edit',['user_id'=>$customer->id])}}">এডিট</a>
        <a class="dropdown-item" onclick="return confirm(' you want to delete?');" href="{{route('users.delete',['user_id'=>$customer->id])}}">ডিলিট</a>
    </div>
</div>
