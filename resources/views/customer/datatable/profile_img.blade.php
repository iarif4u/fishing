<div class="d-flex">
    <div class="usr-img-frame mr-2 rounded-circle">
        <img alt="avatar" class="img-fluid rounded-circle" src="{{get_media_file($customer,config('const.media.profile'))}}">
    </div>
</div>
