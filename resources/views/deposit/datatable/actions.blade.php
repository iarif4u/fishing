<div class="btn-group">
    <a href="{{route('users.profile',$deposit->customer->id)}}" type="button" class="btn btn-dark btn-sm">বিস্তারিত তথ্য</a>
    <button type="button" class="btn btn-dark btn-sm dropdown-toggle dropdown-toggle-split" id="dropdownMenuReference1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-reference="parent">
        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
    </button>
    <div class="dropdown-menu" aria-labelledby="dropdownMenuReference1">
        <a class="dropdown-item" href="{{route('deposit.edit',$deposit->deposit_uid)}}">এডিট </a>
        <a class="dropdown-item" onclick="return confirm('You want to delete?');" href="{{route('deposit.delete',$deposit->deposit_uid)}}">ডিলিট </a>
        <a class="dropdown-item" href="#">ইনভয়েস </a>
    </div>
</div>
