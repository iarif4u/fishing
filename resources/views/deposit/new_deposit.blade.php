@extends('layouts.master')
@section('style')
    <link rel="stylesheet" href="{{asset('plugins/select2/select2.min.css')}}">
    <link rel="stylesheet" href="{{asset('plugins/flatpickr/flatpickr.css')}}">
    <style>
        .select2-container {
            margin-bottom: 0 !important;
        }
    </style>
@endsection
@section('heading')
    <h3>নতুন জমার প্রদানর হিসাব</h3>
@endsection
@section('breadcrumb')
    <ol class="breadcrumb">
        <li class="breadcrumb-item">
            <a href="{{route('home')}}">
                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"
                     stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"
                     class="feather feather-home">
                    <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path>
                    <polyline points="9 22 9 12 15 12 15 22"></polyline>
                </svg>
            </a>
        </li>
        <li class="breadcrumb-item"><a href="javascript:void(0);">জমার</a></li>
        <li class="breadcrumb-item active" aria-current="page"><span>নতুন জমার  প্রদানর হিসাব</span></li>
    </ol>
@endsection
@section('content')
    <div class="row">
        <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-spacing">
            <div class="widget-content-area br-4">
                <form method="post" action="{{route('deposit.add')}}">
                    @csrf
                    <div class="form-row mb-4">
                        <div class="form-group col-md-3">
                            <label for="customer_id">ব্যক্তির নাম</label>
                            <select name="customer_id" id="customer_id" class="form-control">
                                <option selected>ব্যক্তির নাম নির্বাচন করুন</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="fourth">মোট পাকা চৌঠা </label>
                            <input type="number" class="form-control" id="fourth" placeholder="৬০,০০০" readonly>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="previous_payment">আগে টাকা প্রদান করা হয়েছিল</label>
                            <input type="number" class="form-control" id="previous_payment" name="previous_payment"
                                   placeholder="৫০,০০০" readonly>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="due_payment">আর বাকি আছে</label>
                            <input type="number" class="form-control" id="due_payment" name="due_payment" placeholder="১০,০০০" readonly>
                        </div>
                    </div>
                    <div class="form-row mb-4">
                        <div class="form-group col-md-6 mb-0">
                            <label for="date">তারিখ</label>
                            <input value="{{date('Y-m-d')}}" name="date" id="date"
                                   class="form-control flatpickr flatpickr-input active" type="text"
                                   placeholder="তারিখ নির্বাচন করুন ..">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="deposit">নগদ জমা</label>
                            <input type="number" name="deposit" class="form-control" id="deposit" placeholder="৬০,০০০">
                        </div>

                    </div>
                    <div class="form-row mb-4">
                        <div class="form-group col-md-12">
                            <label for="note">নোট</label>
                            <input name="note" type="text" class="form-control" id="note" placeholder="নোট">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary mt-3">তথ্য যুক্ত করুন</button>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset('plugins/select2/select2.min.js')}}"></script>
    <script src="{{asset("plugins/flatpickr/flatpickr.js")}}"></script>
    <script src="{{asset("plugins/axios/dist/axios.js")}}"></script>
    <script>
        var f1 = flatpickr(document.getElementById('date'));

        $("#customer_id").select2({
            placeholder: "Select Customer",
            ajax: {
                url: '{{route('users.search')}}',
                dataType: 'json',
                data: function (params) {
                    // Query parameters will be ?search=[term]&type=public
                    return {
                        search: params.term,
                        type: "public"
                    };
                },
                processResults: function (response) {
                    return {
                        results: $.map(response.data, function (txt, val) {
                            return {id: txt.id, text: txt.name}
                        })
                    };
                }
                // Additional AJAX parameters go here; see the end of this chapter for the full code of this example
            }
        });

        $('#sell').on('keypress keyup blur change', function (e) {
            var sell = $(this).val();
            var commission = '{{env('COMMISION')}}';
            const calculation = sell * (commission / 100);
            $("#commission").val(calculation);
        });
        $('#customer_id').on('change', function (e) {
            var customer_id = $(this).val();
            axios.get(`{{route('users.fourth')}}?customer_id=${customer_id}`)
                .then(function (response) {
                    $("#fourth").val(response.data.fourth);
                    $("#previous_payment").val(response.data.previous_payment);
                    $("#due_payment").val(response.data.fourth-response.data.previous_payment);
                })
                .catch(function (error) {
                    console.log(error.response);
                });
        });
    </script>
@endsection
